(function($) {
  'use strict';

  $.fn.uninav = function(options) {
    var _this = this,
    defaults = {
      //Basic variables
      domain: document.domain,

      //Header variables
      type: '',
      uninav: true,
      uninavAnimation: true,
      uninavAnimationSpeed: 'fast',
      mobileNav: true,
      dataDomain: '',
      oldsearch: true,
      headerSticky: false,
      saveLink: true,

      // New Settings
      adBlocker: [{
        enable: false,
      }],

      cookiePolicy: [{
        enable: true,
      }],

      privacyPolicy: [{
        enable: true,
      }],

      ssoBehavioral: true,

      //SSO variables
      sso: true,
      ssoApikey: '',
      customDashBoard: [],
      domainKey: '',
      referrer: '',
      clientId: '',
      scope: store.defaultScope,
      responseType: store.defaultResponseType,
      ssoPageURL: store.kapamilyaUrl,
      redirectURI: store.defaultCallback,
      defaultLogin: true,
    },

    // SSO API URL
    ssoApiUrl = 'https://staging-account.abs-cbn.com',
    DSStorePrivacyApiUrl = ssoApiUrl+'/api/DS/DSStorePrivacy',
    DSStoreGetPrivacyApiUrl = ssoApiUrl +'/api/DS/DSStoreGetPrivacy',
    
    x = this.config = $.extend(defaults, options);

    // window.clientId = x.clientId;
    window.ssoConfig = x;

    // Settings for sso, uninav, and gigya
    var setting = {
      location: function(url) {
        if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)){
          var referLink = document.createElement('a');
          referLink.href = url;
          document.body.appendChild(referLink);
          referLink.click();
        } else {
          location.href = url;
        }
      },

      getAccountInformation: function() {
        createHeaderMarkup.iframe();
        gigya.accounts.getAccountInfo({
          include: "loginIDs, profile, data, subscriptions",
          callback: function(res) {
            if (typeof x.callback === 'function') {
              window.gigyaAccountInfo = res;
              x.callback.call(this, window.gigyaAccountInfo);
            }
            // check if user is login // FAIL = user not login
            if (res.status === "FAIL") {
              if ($('.bookmarked-saved-container').length > 0) {
                storage.checkFirstVisit(false);
                // activate save button for article.
                button.activate([], true);
              }

              // load uninav Login text.
              $('.login-icon-uninav').html(createHeaderMarkup.notLogin());
              $('body').attr('data-login', 'false');

              // Removing localstorage for sso-behvioral purpose
              localStorage.removeItem('sso_uid');
              localStorage.removeItem('sso_lastLoginTimestamp');
              localStorage.removeItem('sso_behavioral');

              // Removing localstorage for privacy policy
              localStorage.removeItem('isAcceptedPrivacyPolicy');
              localStorage.removeItem('isAPIReachedLimitPrivacyPolicy');
              localStorage.removeItem('isExistPrivacyPolicy');
              localStorage.removeItem('datePrivacyPolicy');
              localStorage.removeItem('uninavUserID');
              localStorage.removeItem('uninav-sso-id');

            } else {

              var uninav_sso_uid = localStorage.getItem('uninav-sso-id');

              if(uninav_sso_uid == null) {
                // Set uninav-sso-id
                localStorage.setItem('uninav-sso-id', res.UID);
              } else {
                if(uninav_sso_uid !== res.UID) {
                  // Removing localstorage for sso-behvioral purpose
                  localStorage.removeItem('sso_uid');
                  localStorage.removeItem('sso_lastLoginTimestamp');
                  localStorage.removeItem('sso_behavioral');

                  // Removing localstorage for privacy policy
                  localStorage.removeItem('isAcceptedPrivacyPolicy');
                  localStorage.removeItem('isAPIReachedLimitPrivacyPolicy');
                  localStorage.removeItem('isExistPrivacyPolicy');
                  localStorage.removeItem('datePrivacyPolicy');
                  localStorage.removeItem('uninavUserID');

                  // Update uninav-sso-id
                  localStorage.setItem('uninav-sso-uid', res.UID);
                }
              }
              // load uninav user Login.
              $('.login-icon-uninav').html(createHeaderMarkup.alreadyLogin(res));
              // creating markup for sub menu of login user.
              if ($('.bookmarked-saved-container').length > 0) {
                storage.bubbleFlagLoginState = true;
                storage.checkLastLogin(res);

                // getting the list of saved article.
                save.list(res.data);
              }

              createHeaderMarkup.createSSOsubNavMarkup(res);
              // function of logout from commenting. also needs to logout on kapamilya accounts.
              setting.commentingLogout();
              $('body').attr('data-login', 'true');
            }
          }
        });
      },

      commentingLogout: function() {
        var params = {
          onLogout:function (e) { 
            // createHeaderMarkup.iframe();
            window.location.reload();
            // window.location.href = x.ssoPageURL + "signout";
           }
        };

        gigya.accounts.addEventHandlers(params);
      }
    };

    var storage = {
      unloading: false,
      checkFirstVisit: function(isLogin, res) {
        if (isLogin) {
          storage.loginFirstVisit(res.loginIDs, res.data);
        } else {
          if (!localStorage.notFirstVisit) {
            if ($('.bookmarked-saved-container').is(':visible')) {
              storage.logoutFirstVisit();
              // set local storage item for first visit
              localStorage.setItem("notFirstVisit", true);
            }
          }
        }
      },

      loginFirstVisit: function(_uName, data) {
        // create variable
        var markup = ''; var savedCount = 0;
        // getting the length or the total saved article that is not yet readed.
        if (data.readList) {
          $.each(data.readList, function(i, val) {
            if (val.readed === false) {
              savedCount++;
            }
          });
        }

        if (savedCount > 0) {
          // generating notification markup for first time visit login state user.
          markup += '<div class="saved-notification">';
            markup += '<div class="arrow-container">';
              markup += '<div class="_close fa fa-times" style="font-family: FontAwesome;"></div><div class="arrow-top"></div>';
            markup += '</div>';
            markup += '<div class="container">';
              markup += '<span>';
                markup += '<strong>'+_uName.username+'</strong>';
                markup += ', you saved '+savedCount+' link/s recently. Go to ';
                markup += '<a href="'+store.kapamilyaUrl+'profile#/saved" style="color:#4286f4;">Saved</a>';
                markup += ' to view them now.';
              markup += '<span>';
            markup += '</div>';
          markup += '</div>';

          // appending the generated markup into header .uninav-header
          $(markup).fadeIn("medium").appendTo('header.uninav-header');
          var leftPos = parseInt($('.login-icon-uninav').width() - 30);

          $('.saved-notification .arrow-top').css('right', leftPos + 'px');

          save.lastLogin(new Date(), true);

          // call function for auto hiding or removing notification on first visit
          xtimer = new Timer(function() {
            // remove notification after 10 second's without interaption
            $('.saved-notification').fadeOut("medium", function() {
              $(this).remove();
            });
          }, 10000);
        }
      },

      logoutFirstVisit: function(_this) {
        var btn = (_this) ? _this : $('.bookmarked-saved-container');
        var _title = 'New! Save links';
        var _content = 'Login to your Kapamilya Account to save links for viewing later ';

        _content += '<a href="javascript:void(0);" onclick="uninav.ssoLogin();" class="saved-bookmark-login">Click here</a> to login now.';

        btn.append(save.popupMessage(_title, _content, 'logout-state'));

        save.setPopupPosition();
        timer = null;
        storage.myBubbleFlagShow = true;
        timer = new Timer(function() {
          $('.bubble-message-container.logout-state').fadeOut("medium", function() {
            $(this).remove();
            storage.bubleFlag = true;
            timer = null;
            storage.myBubbleFlagShow = false;
          });
        }, 10000);
      },

      bubbleFlagLoginState: false,
      bubleFlag: true,
      myBubbleFlagShow: false,

      loginFirstVisitButtonPopup: function(_this) {
        var btn = (_this) ? _this : $('.bookmarked-saved-container');
        // var btn = $('.bookmarked-saved-container');
        var _title = 'New! Save links';
        // var _content = '<a href="javascript:void(0);" class="saved-bookmark-links">Click here</a> to save now.';
        var _content = 'Save links to your Kapamilya Account for viewing later.';

        btn.append(save.popupMessage(_title, _content, 'login-state'));

        save.setPopupPosition();
        timer = null;
        storage.myBubbleFlagShow = true;
        timer = new Timer(function() {
          $('.bubble-message-container.login-state').fadeOut("medium", function() {
            $(this).remove();
            storage.bubleFlag = true;
            timer = null;
            storage.myBubbleFlagShow = false;
          });
        }, 10000);
      },

      checkLastLogin: function(response) {
        if (response.data.lastLogin) {
          var isShown = response.data.isUnsavedShown;
          var lastLogin = new Date(response.data.lastLogin);
          lastLogin.setDate(lastLogin.getDate() + 7); // adding sevendays
          var dateNow = new Date();

          if (lastLogin <= dateNow || !isShown) {
            storage.checkFirstVisit(true, response);
          }
        } else {
          storage.checkFirstVisit(true, response);

          // save lastLogin
          save.lastLogin(new Date(), false);
        }
      },

      flag: false,
      firstFlag: false
    };

    // save button functions
    var button = {
      activate: function(showPopup) {
        $('.bookmarked').removeAttr("disabled");
        $(document).on('click', '.bookmarked', function(e) {
          e.preventDefault(); e.stopPropagation();
          var _this = $(this); var article = _this.parents('article');
          // check if not login. then show popup
          if (showPopup) {
            var _tempData = JSON.stringify(button.generateArticle(article));
            localStorage.setItem('saveData', _tempData);

            createHeaderMarkup.goToKapamilyaLogin();
            return;
          }

          if(!_this.hasClass("article-saved")) {
            var blocker = '<div class="my-blocker"></div><div class="loader-icon"></div>';
            _this.parent().append(blocker);

            save.data.push(button.generateArticle(article));
            save.article(save.data, _this);

            storage.firstFlag = true;
          } else {
            storage.flag = true;
            if ($('.bubble-message-container.saved-unsaved-link').is(":visible")) {
              $('.bubble-message-container').remove();
              storage.flag = false;
              if (storage.firstFlag) {
                button.generateSaveLinks(_this);
                storage.firstFlag = false;
              }
              return;
            }

            button.generateSaveLinks(_this);
          }
        });
      },

      generateSaveLinks: function(_this) {
        var markup = '';
        markup += '<ul>';
          markup += '<li><a href="'+store.kapamilyaUrl+'profile#/saved" class="view-saved-link">View your saved links</li>';
          markup += '<li><a href="javascript:void(0);" class="unsave-article">Remove this link</li>';
        markup += '</ul>';
        $('.bubble-message-container').remove();

        _this.parent().append(save.popupMessage('', markup, 'saved-unsaved-link'));

        $('.bubble-message-container').css('width', '175px');
        save.setPopupPosition(false, true);
      },

      generateArticle: function(_art) {
        var temp = {
          articleId: new Date().getTime().toString(),
          saveDate: button.getDateNow(),
          readed: false,
          title: _art.find('meta[itemprop="headline"]').attr("content"),
          shortDesc: _art.find('meta[itemprop="description"]').attr("content"),
          thumbnail: _art.find('meta[itemprop="thumbnailUrl"]').attr("content"),
          urlLink: _art.find('meta[itemprop="url"]').attr("content"),
          siteSrc: $('meta[property="og:site_name"]').attr("content"),
          siteName: createHeaderMarkup.returnSiteName().toLowerCase()
        };

        return temp;
      },

      getDateNow: function() {
        var date = new Date(); var year = date.getFullYear(); var day = date.getDate(); var month = date.getMonth();

        switch (month) {
          case 0: month = "January"; break; case 1: month = "February"; break; case 2: month = "March"; break;
          case 3: month = "April"; break; case 4: month = "May"; break; case 5: month = "June"; break;
          case 6: month = "July"; break; case 7: month = "August"; break; case 8: month = "September"; break;
          case 9: month = "October"; break; case 10: month = "November"; break; case 11: month = "December"; break;
        }

        return month + " " + day + ", " + year;
      },

      scrollActivate: function(article) {
        save.artLen = $('.bookmarked').parents('article').length;
        $(window).scroll(function() {
          var newLen = $('.bookmarked').parents('article').length;
          if (save.artLen < newLen) {
            save.artLen = newLen;
            save.setSavedArticle();
          }
        });
      }
    };

    // save item
    var save = {
      // variable for storing data article
      data: [],
      // variable for storing all title of articles
      title: [],
      artLen: 0,
      dataItemFlag: false,

      list: function(data) {
        if(data.readList && !save.dataItemFlag) {
          // putting all article data into variable data
          save.data = data.readList;
          // set all saved article
          save.setTitleHolder(save.data, true);
        } else {
          storage.loginFirstVisitButtonPopup();
        }
        // check if saving article comes from logout to login then do auto save
        var tempData = JSON.parse(localStorage.getItem('saveData'));

        if(tempData) {
          var alreadySaved = false;
          save.dataItemFlag = true;

          // pushing or updating the array data that contains all article
          $.each(save.data, function(i, val) {
            if (val.title.toString() === tempData.title.toString()) {
              alreadySaved = true;
              return false;
            }
          });

          if (!alreadySaved) {
            save.data.push(tempData);
            // calling auto save function after user successfully login to kapamilya accounts
            save.auto(save.data);
            // remove local storage item for saveData
            localStorage.removeItem('saveData');
          } else {
            localStorage.removeItem('saveData');
          }
        }

        // enabling button
        button.activate(false);
      },

      setTitleHolder: function(data, firstLoad) {
        var savedCount = 0;
        var _siteName = createHeaderMarkup.returnSiteName().toLowerCase();

        $.each(data, function(i, val) {
          if (val.readed === false && val.siteName === _siteName) {
            savedCount++;
          }
          save.title.push(val.title);
        });

        if (savedCount > 0) {
          $('.saved-count').html(savedCount + ' unopened').show();
          $('.budge-counts-saved').html(savedCount).show();
        } else {
          $('.saved-count, .budge-counts-saved').html('').hide();

        }

        // set active for all those article saved.
        if (firstLoad) {
          save.setSavedArticle(savedCount);
        }
      },

      setSavedArticle: function(_savedCount) {
        var article = $('.bookmarked').parents('article');
        $.each(article, function(i, val) {
          var _this = $(val);
          var title = _this.find('meta[itemprop="headline"]').attr("content");
          var container = _this.find(".bookmarked-saved-container");

          // checking if article is already saved.
          if (save.isSaved(save.title, title)) {
            // container.html('<button class="bookmarked article-saved"><span>Saved</span></button>');
            container.find('.bookmarked').addClass('article-saved');
            container.find('.bookmarked span').text('Saved');
          } else {
            container.find('.bookmarked').removeClass('article-saved');
            container.find('.bookmarked span').text('Save');
            // container.html('<button class="bookmarked"><span>Save</span></button>');
          }
        });

        if (!localStorage.notFirstVisit && _savedCount === 0) {
          // set local storage item for first visit
          localStorage.setItem("notFirstVisit", true);
          $('.bubble-message-container').css('width', '155px');
        }

        button.scrollActivate();
      },

      isSaved: function(a, b) {
        return a.indexOf(b) > -1;
      },

      article: function(data, _this) {
        var params = {
          data: {
            readList: data
          },
          callback: function(response) {
            if (response.errorCode === 0) {
              // remove loader after save
              _this.parent().find('.my-blocker').remove();
              _this.parent().find('.loader-icon').remove();
              // updating the value of title
              save.setTitleHolder(save.data, false);
              // adding class article-saved after saving to notify the user it is already saved
              _this.parents('article').find('.bookmarked-saved-container').find('.bookmarked').addClass("article-saved");
              // changing the value of Save button into Saved.
              _this.find('span').html("Saved");
              // showing bubble popup. to notify user that this article is saved.
              $('.bubble-message-container').remove();
              save.showBubblePop(_this.parent());
            } else {
              console.log("An error has occurred!\nError details: " + response.errorMessage + "\nError code: " + response.errorCode + "\nIn method: " + response.operation);
            }
          }
        };

        console.log("saving : ", params);
        gigya.accounts.setAccountInfo(params);
      },

      bubblePopup: function(str, container, closeTime) {
        $('.bookmark-saved').remove();
        var bubblePopup = $('<div class="bookmark-saved">'+str+'</div>').fadeIn("fast").appendTo(container);

        if (!closeTime) { closeTime = closeTime; } else { closeTime = 3000; }

        var timeout = setInterval(function() {
          bubblePopup.fadeOut("slow"); bubblePopup.remove(); clearInterval(timeout);
        }, closeTime);
      },

      showBubblePop: function(_container) {
        var _title = 'Saved!';
        var _content = 'Click <a href="'+store.kapamilyaUrl+'profile#/saved">here</a> to view your saved links now.';
        var markup = save.popupMessage(_title, _content);
        _container.append(markup);

        save.setPopupPosition(true);
        timer = null;
        timer = new Timer(function() {
          $('.bubble-message-container').fadeOut("medium", function() {
            $(this).remove();
            timer = null;
          });
        }, 10000);
      },

      setPopupPosition: function(isSpecial, forLogin) {
        var btn = $('.bookmarked-saved-container');
        var container = $('.bubble-message-container');
        var leftPos = btn.position().left;
        var topPos = -container.outerHeight();

        container.css("top", parseInt(topPos-11)+"px");

        if (parseInt($(window).width()) <= 426) {
          if (parseInt(leftPos) >= 150) {
            // if (container.outerWidth() === 175) {
            //   leftPos = parseInt(leftPos - container.outerWidth()) + 35;
            // } else {
              switch(createHeaderMarkup.returnSiteName().toLowerCase()) {
                case "entertainment":
                  leftPos = 205;
                  break;
                case "starcinema":
                  leftPos = 150;
                  break;
                case "sports":
                  if (isSpecial) {
                    leftPos = 195;
                  } else {
                    if (forLogin) {
                      leftPos = 90;
                    } else {
                      leftPos = 190;
                    }
                  }
                  break;
                case "lifestyle":
                case "starstudio":
                  if (forLogin) {
                      leftPos = 90;
                    } else {
                      leftPos = 150;
                    }
                  break;
                default:
                  leftPos = 195;
                  break;
              // }
            }

            container.css("left", "-"+leftPos+"px");

            if (createHeaderMarkup.returnSiteName().toLowerCase() === "starcinema") {
              $('.arrow-bubble').css({"left": "initial", "right": "70px"});
            } else if (createHeaderMarkup.returnSiteName().toLowerCase() === "sports") {
              $('.arrow-bubble').css({"left": "initial", "right": "50px"});
            } else if (createHeaderMarkup.returnSiteName().toLowerCase() === "starstudio" || createHeaderMarkup.returnSiteName().toLowerCase() === "lifestyle") {
              if (forLogin) {
                $('.arrow-bubble').css({"left": "initial", "right": "33px"});
              } else {
                var newArrowPosLeft = parseInt($('.bookmarked').offset().left + 15);
                $('.arrow-bubble').css({"left":newArrowPosLeft+"px"});
              }
            } else {
              $('.arrow-bubble').css({"left": "initial", "right": "33px"});
            }

          } else {
            switch(createHeaderMarkup.returnSiteName().toLowerCase()) {
              case "bmpm":
                container.css({
                  "bottom": "calc(100% + 5px)",
                  "right": "calc(50% - 165px)"
                });
                $('.arrow-bubble').css({"left": "40%", "top" : "6px"});
                break;
              case "starcinema":
                container.css({
                  "bottom": "calc(100% + 7px)",
                  "right": "calc(50% - 125px)"
                });

                if (forLogin) {
                  container.css({
                    "bottom": "calc(100% + 7px)",
                    "right": "calc(50% - 80px)"
                  });
                } else {
                  container.css({
                    "bottom": "calc(100% + 7px)",
                    "right": "calc(50% - 125px)"
                  });
                }
                $('.arrow-bubble').css({"left": "55%", "top": "5px"});
                break;
              case "sports":
                if (forLogin) {
                  container.css({
                    "left": 'initial',
                    "right": "calc(50% - 80px)"
                  });
                } else {
                  container.css({
                    "left": 'initial',
                    "right": "calc(50% - 125px)"
                  });
                }
                $('.arrow-bubble').css({"left": "58%", "top": "0px"});
                break;
              default:
                container.css("left", "0px");
                $('.arrow-bubble').css("left", "23px");
                break;
            }
          }
        } else {
          if (parseInt(leftPos) <= 30) {
            container.css("left", "-5px");
            switch(createHeaderMarkup.returnSiteName().toLowerCase()) {
              case "bmpm":
                $('.arrow-bubble').css("left", "9px");
                break;
              default:
                $('.arrow-bubble').css("left", "23px");
                break;
            }
          } else {
            container.css("left", "-30px");
          }
        }
      },

      popupMessage: function(_title, _content, _state) {
        _state = _state ? _state : '';
        var markup = '';
        markup += '<div class="bubble-message-container '+_state+'">';
        // check if title is empty or not. if empty just display the content without the title container
        if(_title !== '') {
          markup += '<div class="bubble-title">';
            markup += '<span>'+_title+'</span>';
            markup += '<div class="bubble_close fa fa-times"></div>';
          markup += '</div>';
        }
          markup += '<div class="bubble-content">';
            markup += _content;
          markup += '</div>';
          markup += '<div class="arrow-container">';
            markup += '<div class="arrow-bubble"></div>';
          markup += '</div>';
        markup += '</div>';

        return markup;
      },

      unsaveArticle: function(_this) {
        var _art = _this.parents("article");
        var title =  _art.find('meta[itemprop="headline"]').attr("content");

        var blocker = '<div class="my-blocker"></div><div class="loader-icon"></div>';
        var buttonContainer = _art.find('.bookmarked-saved-container');
        // appending loader to the button container
        buttonContainer.append(blocker);
        // updating save data without the selected article.
        save.data = save.data.filter(function(data) {
          return data.title.toString() !== title.toString();
        });

        // calling unsave function or deleting the article.
        save.unsaved(save.data, buttonContainer);
      },

      unsaved: function(data, _btnContainer) {
        var params = {
          data: {
            readList: data
          },
          callback: function(response) {
            if (response.errorCode === 0) {
              // remove loader after saving/deleting article selected
              _btnContainer.find('.my-blocker').remove();
              _btnContainer.find('.loader-icon').remove();
              // update the value of save data
              save.data = data;
              // updating the value of title
              save.setTitleHolder(save.data, false);
              // removing class of article-saved for button
              // _btnContainer.find('button').removeClass("article-saved");
              _btnContainer.parents('article').find('.bookmarked-saved-container').find('.bookmarked').removeClass('article-saved');
              // updating the content of button Saved into Save
              _btnContainer.find('span').html("Save");
              // remove bubble popup
              $('.bubble-message-container').remove();
            } else {
              console.log("An error has occurred!\nError details: " + response.errorMessage + "\nError code: " + response.errorCode + "\nIn method: " + response.operation);
            }
          }
        };

        console.log("unsaving : ", params);
        gigya.accounts.setAccountInfo(params);
      },

      auto: function(data) {
        var params = {
          data: {
            readList: data
          },
          callback: function(response) {
            if (response.errorCode === 0) {
              save.setTitleHolder(save.data, true);
            } else {
              console.log("An error has occurred!\nError details: " + response.errorMessage + "\nError code: " + response.errorCode + "\nIn method: " + response.operation);
            }
          }
        };

        console.log("auto save : ", params);
        gigya.accounts.setAccountInfo(params);
      },

      lastLogin: function(_dateNow, isShown) {
        gigya.accounts.setAccountInfo({
          data: {
            lastLogin: _dateNow.toString(),
            isUnsavedShown: isShown
          }, callback: function(res) {
            if (res.errorCode !== 0) { console.log("ERROR : ", res.errorMessage); }
          }
        });
      },

      count: function(data) {
        var ctr = 0;
        var _siteName = createHeaderMarkup.returnSiteName().toLowerCase();
        if (data) {
          $.each(data, function(i, val) {
            if (val.readed === false && val.siteName === _siteName) {
              ctr++;
            }
          });
        }

        return ctr;
      }
    };

    var appendScript = {
        appendSSOBehavioral: function() {
            var behaviorTS = new Date().getTime();
            $('body').append('<script type="text/javascript" src="//assets.abs-cbn.com/sso/sso-behavior.js?timestamp='+behaviorTS+'"></script>');
        }
    };

    var timer = null, xtimer = null;

    function Timer(callback, delay) {
      var timerId, start, remaining = delay;

      this.pause = function() {
        clearTimeout(timerId);
        remaining = delay; //new Date() - start;
      };

      this.resume = function() {
        start = new Date();
        clearTimeout(timerId);
        timerId = setTimeout(callback, remaining);
      };

      this.resume();
    }

    var bubble = {
      showLoginState: function(_this) {
        $('.bubble-message-container').remove();
        var _title = 'New! Save links';
        var _content = 'Save links to your Kapamilya Account for viewing later.';

        _this.append(save.popupMessage(_title, _content, 'login-state'));

        save.setPopupPosition();
        _this.find('.bubble-message-container.login-state').show();
      },

      showLogoutState: function(_this) {
        $('.bubble-message-container').remove();
        var _title = 'New! Save links';
        var _content = 'Login to your Kapamilya Account to save links for viewing later ';

        _content += '<a href="javascript:void(0)" onclick="uninav.ssoLogin();" class="saved-bookmark-login">Click here</a> to login now.';

        _this.append(save.popupMessage(_title, _content, 'logout-state'));

        save.setPopupPosition();

        _this.find('.bubble-message-container.logout-state').show();
      }
    };

    $(document).on('click', '.uninav-ad-blocker-container button', function(){
      aBlock.AdBlockerInit(x.adBlocker[0]);
    });

    $(document).on('click', '.takeover-close', function(e){
        e.preventDefault();
        $('.site-takeover').remove();
    });

    $(document).on('mouseenter', '.bookmarked-saved-container', function() {
      if ($(".login-icon-uninav").text() !== "LOADING..") {
        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
         // some code..
         return;
        }

        if ($(this).find('.bubble-message-container').is(":visible") || $(this).find('button').hasClass("article-saved")) {
          return;
        } else {
          if (timer && !storage.myBubbleFlagShow) { timer = null; }

          if (storage.bubbleFlagLoginState) {
            bubble.showLoginState($(this));
          } else {
            bubble.showLogoutState($(this));
          }
        }
      }
    });

    $(document).on('click', '.saved-bookmark-links', function() {
      $(this).parents('.bookmarked-saved-container').find('button').click();
    });
    // events
    $(document).on('mouseenter', '.bubble-message-container', function() {
      if (timer) {
        timer.pause();
      }
    });

    $(document).on('mouseleave', '.bubble-message-container', function() {
      if (timer) {
        timer.resume();
      }
    });

    $(document).on('mouseenter', '.saved-notification', function() {
      xtimer.pause();
    });

    $(document).on('mouseleave', '.saved-notification', function() {
      xtimer.resume();
    });

    $(document).on('click', '.bubble_close', function() {
      $('.bubble-message-container').fadeOut("medium", function() {
        timer = null;
        $(this).remove();
        storage.bubleFlag = true;
      });
    });

    $(document).on('click', '.bubble-message-container', function(e) {
      e.stopPropagation();
    });

    $(document).on('click', '._close', function(e) {
      e.preventDefault();
      $('.saved-notification').fadeOut("medium", function() {
        xtime = null;
        $(this).remove();
      });
    });

    $(document).on('click', '.unsave-article', function() {
      save.unsaveArticle($(this));
    });

    $(document).on('click', 'body', function(){
      $('.uninav-main, .triangle-top, .triangle-bottom').hide();
      $('.gigya-expanded-profile').css("cssText", "display: none;");
      createHeaderMarkup.isTemp = 0;
      if (storage.flag) {
        $('.bubble-message-container').remove();
        storage.flag = false;
      }
    });

    $(document).on('click', '.uninav-main, .gigya-expanded-profile', function(e){e.stopPropagation();});

    $(document).on('click', '.network-uninav', function(e) {
      e.stopPropagation();

      main.showUninavLinks();
    });

    $(document).on('click', '.close-uninav-links', function(e) {
      e.stopPropagation();

      main.closeUninavLinks();
    });

    $(document).on('click', '.login-icon-uninav', function(e) {
      e.stopPropagation();
      if ($(this).text() !== "LOADING..") {
        if (!$(this).hasClass('active') && !$(this).hasClass('without-photo')) {
          if (x.defaultLogin) {
            var siteName = createHeaderMarkup.returnSiteName().toLowerCase();
            createHeaderMarkup.loginClick(siteName);
          }
        } else {
          $('.uninav-main').hide();
          $('.saved-notification').hide();
          var leftPosition = $(this).offset().left + 15;

          $('.triangle-top, .triangle-bottom').css({'left' : leftPosition+'px'});
          $('.triangle-top, .triangle-bottom').toggle();
          if (createHeaderMarkup.isTemp === 0) {
            // $('.gigya-expanded-profile').css("cssText", "display: block; left: initial !important; top:40px !important");
            $('.sso-profile').addClass('sso-profile-active');
            $('.my-blocker').show();
            $('html').css({ 'overflow': 'hidden', '-webkit-overflow-scrolling': 'touch' });
            $('body').addClass("sso-login-active");
            createHeaderMarkup.isTemp = 1;
          } else {
            // $('.gigya-expanded-profile').css("cssText", "display: none;");
            $('.sso-profile').addClass('sso-profile-active');
            $('.my-blocker').show();
            $('html').css({ 'overflow': 'hidden', '-webkit-overflow-scrolling': 'touch' });
            $('body').addClass("sso-login-active");
            createHeaderMarkup.isTemp = 0;
          }
        }
      }
    });

    $(document).on('click', 'header .header-bar button.header-nav-toggle', function(e) {
      e.stopPropagation();
      $(this).toggleClass('collapsed');
      $('#header-collapse').toggleClass('in');

      if ($(this).hasClass('collapsed')) {
        $(this).attr('aria-expanded', 'false');
        $('#header-collapse').attr('aria-expanded', 'false');
      } else {
        $(this).attr('aria-expanded', 'true');
        $('#header-collapse').attr('aria-expanded', 'true');
      }
    });

    if (x.oldsearch === false) {
      $(document).on('click', '.uninav-icon-search', function(e) {
        e.preventDefault();
        var _this = $(this).parent();
        _this.toggleClass('active');

        if (_this.is('.active')) {
          $('header').append(createHeaderMarkup.createSearchPopup());
          $('#mobile-search').fadeIn("fast");
          $('body, html').css({ "overflow": "hidden" });
        } else {
          $('#mobile-search').fadeOut().remove();
          $('body, html').css({ "overflow": "" });
        }
      });

      $(document).on('focus', 'input[mobile-input="search"]', function() {
        $(this).siblings('span').stop().addClass('active');
      });

      $(document).on('focusout', 'input[mobile-input="search"]', function() {
        if ($('input[mobile-input="search"]').val() === "") {
          $(this).siblings('span').stop().removeClass('active');
        } else {
          $(this).siblings('span').stop().addClass('active');
          $('input[mobile-input="search"]').val();
        }
      });

      $(document).on('click', '.search-form span', function() {
        $('input[mobile-input="search"]').focus();
      });


      $(document).on('touchstart click', '.uninav-close', function(e) {
        e.preventDefault();
        $('#mobile-search').remove();
        $('.uninav-icon-search-new').removeClass('active');
        $('body, html').css({ "overflow": "" });
        $('.uninav-icon-search').parent().removeClass('active');
      });
    } else if (x.oldsearch === true) {
      $('.search-container-lg .search-container').show();
      $('.uninav-icon-new.uninav-icon-search').parents('li').hide();
    }

    $(document).on('touchstart click', '.uninav-close', function(e) {
      e.preventDefault();
      $('#mobile-search').remove();
      $('.uninav-icon-search-new').removeClass('active');
      $('body, html').css({ "overflow": "" });
      $('.uninav-icon-search').parent().removeClass('active');
    });

    $(document).on("click", ".dashboard-logout", function() {
      // createHeaderMarkup.iframe();
      uninav.ssoLogout();
    });

    $(document).on('click', '.has-child-drop', function() {
      if ($(this).parents('.header-subnav-showpage').length > 0) {
        $(this).parents('.header-subnav-showpage').find("ul").slideToggle("fast");
      } else {
        $(this).parents('li').find("ul").slideToggle("fast");
      }
    });

    $(document).on('click', '.sso-btn-login, .sign-up', function() {
      if ($("#chkDontShowThisAgain").prop('checked')) {
        localStorage.setItem('dontShowThisAgain', true);
      }

      createHeaderMarkup.goToKapamilyaLogin();
    });

    $(document).on('click', '.abs-cbn-links-container .uninav-abs-cbn .parent-uninav', function(e) {
      e.stopPropagation();
      if ($(this).text() !== "ABS-CBN") {
        $(this).find('.toggle-uninav-links').trigger('click');
      }
    });

    $(document).on('click', '.abs-cbn-links-container .uninav-abs-cbn .parent-uninav .toggle-uninav-links', function(e) {
      e.stopPropagation();
      var isActive = ($(this).hasClass('active')) ? true : false; // checks if it is already active

      $('.abs-cbn-links-container .uninav-abs-cbn .parent-uninav .toggle-uninav-links').removeClass("active");
      $('.uninav-container-links').find('.child-uninav').slideUp("fast");

      if (!isActive) {
        $(this).addClass("active");
        $(this).parents('.uninav-container-links').find('.child-uninav').slideToggle("fast");
      }
    });

    // Markup Header Creation
    var createHeaderMarkup = {
      isTemp: 0,
      isActive: true,
      headerMarkup: function() {
        $('#uninav-toggle').remove();
        var markup = '';

        markup += '<div class="main-uninav-header">';
          if (x.sso) {
            var styles = (!x.uninav) ? 'style="border-left:none;"' : '';
            markup += '<div class="right-container login-icon-uninav" '+styles+'>';
              markup += '<span class="login-loading">LOADING..</span>';
            markup += '</div>';
          }
          if (x.uninav) {
            markup += '<div class="left-container network-uninav">';
              markup += '<img src="https://assets.abs-cbn.com/universalnav/img/uninav_explore_icon.png" class="icon-explore-advertise">';
              markup += '<span> EXPLORE ABS-CBN </span>';
            markup += '</div>';
            markup += '<div class="left-container divider"> </div>';
              markup += '<div class="left-container advertise">';
                markup += '<a href="http://advertise.abs-cbn.com/" target="_blank" class="advertise-with-us"> ADVERTISE WITH US </a> ';
              markup += '</div>';
          }
        markup += '</div>';

        $('header.uninav-header').append(markup);
        $('header.uninav-header .header-fixed').css('top', '30px');

        if (x.uninav) {
          createHeaderMarkup.loadUninavLinks();
        }

        // if (x.oldsearch === false && $(window).width() <= 768) {
        //   $('.header-bar-inner h1.logo').append(createHeaderMarkup.createSearchIcon());
        // }
      },

      createSearchIcon: function() {
        var markup = '';
        markup +='<a href="javascript:void(0);" style="position: absolute; top: 0; right: 0; width: 50px;">';
          markup += '<i class="uninav-icon-new uninav-icon-search">';
            markup += '<span class="sr-only">search</span>';
          markup += '</i>';
        markup += '</a>';

        return markup;
      },

      notLogin: function() {
        $('.login-icon-uninav').addClass('inactive');
        return '<div class="login"> LOGIN </div>';
      },

      alreadyLogin: function(acc) {
        var markup = '';

        if (acc.profile.photoURL === undefined) {
          $('.login-icon-uninav').addClass('without-photo');
        } else {
          $('.login-icon-uninav').addClass('active');
          console.log(acc.profile.photoURL);
          markup += '<i style="'+createHeaderMarkup.userLoginStyles(acc.profile.photoURL)+'" class="icon-login">';
          markup += '</i>';
        }

        var articleCount = save.count(acc.data.readList);
        var display = (articleCount === 0) ? display = "display:none;" : display = "display:block;";

        markup += '<sup class="budge-counts-saved" style="'+display+'">';
          markup += (articleCount > 99) ? "99+" : articleCount;
        markup += '</sup>';
        
        var _uname = acc.loginIDs.username ? acc.loginIDs.username : '';
        markup += '<div class="uname">' + createHeaderMarkup.setUserName(_uname) + '</div>';

        return markup;
      },

      userLoginStyles: function(photo) {
        var _photo = photo;

        if (photo.indexOf("http:") >= 0) {
          _photo = photo.split('http:')[1];
        } else if (photo.indexOf("https:") >= 0) {
          _photo = photo.split('https:')[1];
        }

        var styles = "";
        styles += "background-image:url("+_photo+");";
        // styles += "background-image:url(//assets.abs-cbn.com/universalnav/img/unknown.jpg);";
        styles += "width: 22px;";
        styles += "height: 20px;";
        styles += "display: inline-block;";
        styles += "border-radius: 50%;";
        styles += "background-size: 100% 100%;";
        styles += "position: relative;";
        styles += "margin: 0px 5px 0px 15px;";
        styles += "top: 5px;";

        return styles;
      },

      setUserName: function(uName) {
        return (uName.length > 10) ? uName.substr(0, 10) + '..' : uName;
      },

      markActive: function() {
        switch (x.domain) {
          /* Put overrides or special links here */
          case 'oneforpacman.abs-cbn.com': // TODO: override directly
            x.domain = 'sports.abs-cbn.com';
            break;
          case 'entertainment.abs-cbn.com':
          case 'entertainment2.abs-cbn.com':
            x.domain = '//entertainment.abs-cbn.com/';
            break;
          case 'news.abs-cbn.com':
            if (window.location.pathname === "/patrolph") {
              x.domain = '//news.abs-cbn.com/patrolph';
            } else if (window.location.pathname === "/bmpm") {
              x.domain = '//news.abs-cbn.com/bmpm';
            }
            break;
          case 'corporate.abs-cbn.com':
            if (window.location.pathname === 'investorrelations') {
                options.activeDomain = '//corporate.abs-cbn.com/investorrelations';
            } else if(window.location.pathname === 'stellar') {
                options.activeDomain = '//corporate.abs-cbn.com/stellar';
            } else {
                options.activeDomain = '//corporate.abs-cbn.com/investorrelations';
            }
            break;
          case 'ktx.abs-cbn.com':
            x.domain = "//ktx.abs-cbn.com/?utm_source=uni-nav&utm_medium=link&utm_content=ktx";
            break;
          default:
            x.domain = '//' + x.domain + '/';
            break;
          }

        $('.uninav-main .uninav-container a[href="' + x.domain + '"]').removeAttr('href').addClass('active');
      },

      uninavMarkup: function() {
        var markup = '';

        $.ajax({
          url: store.uninavLinks,
          success: function(result) {
            markup += '<div id="uninavMainLinks" class="uninav-main" style="top:30px;">';
              markup += result;
            markup += '</div>';

            $('header').append(markup);
            createHeaderMarkup.markActive();
          }
        });
      },

      createSSOsubNavMarkup: function(res) {
        var imgSrc = res.profile.photoURL ? res.profile.photoURL : '//assets.abs-cbn.com/universalnav/img/no-image-logo.png';
        var fName = (res.profile.firstName) ? res.profile.firstName : "";
        var lName = (res.profile.lastName) ? res.profile.lastName : "";

        //Call Privacy Policy
        if (x.privacyPolicy[0].enable) { privacyPolicy.privacyInit(res, ssoApiUrl, DSStoreGetPrivacyApiUrl, x.privacyPolicy[0]); }

        if (imgSrc.indexOf("http:") >= 0) {
          imgSrc = imgSrc.split('http:')[1];
        } else if (imgSrc.indexOf("https:") >= 0) {
          imgSrc = imgSrc.split('https:')[1];
        }

        var savedCount = save.count(res.data.readList);
        var _points = 1;
        
        // $.get(x.ssoPageURL + "/api/Profile/KapamilyaPoints?UID=" + res.UID, function (data) {
        //   if(data.errorCode === 5001) {
        //     console.log(data.message);
        //     $('body').find('.kapamilya-points').remove();
        //   } else {
        //     $('body').find('.sso-profile .kapamilya-points .points').html(Math.round(data.points) + " Points");
        //   }
        // });

        var markup = '';
        markup += '<div class="sso-profile">';
          markup += '<div class="sso-content">';
            markup += '<div class="close-container">';
              markup += '<p class="kapamilya-name">';
                markup += '<i style="background-image:url(' + imgSrc + ');width: 20px;height: 20px;display: inline-block;border-radius: 50%;background-size: 100% 100%;position: relative;margin: 0px 5px 0px 0px; top: 5px;"></i>';
                markup +=  res.loginIDs.username;
                markup += '<i class="uninav-btn-cancel fa fa-times"></i></p>';
            markup += '</div>';

            markup += '<ul class="sso-content-body">';
                markup += '<li class="name">';
                markup += '<h6 class="full-name">' + fName + ' ' + lName + '</h6>';
                markup += '</li>';

                // markup += '<li class="kapamilya-points">';
                //   markup += 'Kapamilya Thank You Points<br>';
                //   markup += '<span class="points"> 0 Points</span>';
                // markup += '</li>';

                markup += '<li class="account">';
                    markup += '<a href="' + store.kapamilyaUrl + 'profile#/basic-info">';
                    markup += 'MY KAPAMILYA ACCOUNT';
                  markup += '</a>';
                markup += '</li>';

                // markup += '<li class="profile">';
                //     markup += '<a href="' + store.kapamilyaUrl + 'profile#/basic-info">';
                //     markup += 'MY PROFILE';
                //   markup += '</a>';
                // markup += '</li>';

                if (x.saveLink) {
                  markup += '<li class="notification">';
                    markup += '<a href="' + store.kapamilyaUrl + 'profile#/saved">';
                      markup += 'VIEW SAVED LINKS';
                  if (savedCount > 0) {
                    markup += '<span class="saved-count" style="color:#b11717;font-size: 12px; padding-left: 15px;">' + savedCount + ' unopened</span>';
                    // markup += '<span class="badge" style="display:inline-block;"><strong>' + savedCount +'</strong></span>';
                  } else {
                    markup += '<span class="badge" style="display:none;"><strong></strong></span>';
                  }
                    markup += '</a>';
                  markup += '</li>';
                }

                $.each(x.customDashBoard, function (i, val) {
                  markup += '<li>';
                    markup += '<a href="' + val.url + '">' + val.title + '</a>';
                  markup += '</li>';
                });

                markup += '<li class="logout">';
                  markup += '<a  href="javascript:void(0);" class="dashboard-logout">';
                    markup += 'LOGOUT';
                  markup += '</a>';
                markup += '</li>';
            markup += '</ul>';
          markup += '</div>';
        markup += '</div>';

        $('body').append(markup);
      },



      lastScroll: 0,
      scrollAnimationHeader: function(_this) {
        if (x.headerSticky === false) {
          if (createHeaderMarkup.lastScroll >= _this.scrollTop()) {
            if ($('head-bar button').hasClass('collapsed')) {
              $('header').removeClass('header-sticky');
            } else if(_this.scrollTop() <= 0) {
              $('header').removeClass('header-sticky');
              return false;
            }

            if (_this.width() <= 768) {
              $('header .header-fixed').removeClass("header-fixed-unpinned");
            }
          } else {
            $('header').addClass('header-sticky');
            if (parseInt(_this.width()) <= 768) {
              $('header .header-fixed').addClass("header-fixed-unpinned");
            }
          }

          createHeaderMarkup.lastScroll = _this.scrollTop();
        }
      },

      addButtonCollasible: function() {
        var markup = '';
        markup += '<button type="button" class="navbar-toggle collapsed header-nav-toggle" data-toggle="collapse" data-target="#header-collapse" aria-expanded="false">';
          markup += '<span class="sr-only">Toggle navigation</span>';
          markup += '<span class="icon-bar"></span>';
          markup += '<span class="icon-bar"></span>';
          markup += '<span class="icon-bar"></span>';
        markup += '</button>';

        return markup;
      },

      createSearchPopup: function() {
        var markup = '';
        markup += '<div id="mobile-search">';
          markup += '<div class="uninav-search">';
            markup += '<form class="search-form" autocomplete="off">';
              markup += '<input class="search-field" type="search" name="search" placeholder="" mobile-input="search"/>';
              markup += '<span>Search</span>';
              markup += '<div class="uninav-close" data-icon="M"></div>';
              markup += '<input type="submit" value="" class="uninav-icon-new uninav-icon-search search-button" />';
            markup += '</form>';
          markup += '</div>';
        markup += '</div>';

        return markup;
      },

      createSplash: function() {
        var ssoLogin = "uninav.ssoLogin({display:'signup'});";
        var markup = "";
        markup += '<div class="my-blocker opac"></div>';
        markup += '<div class="sso-container">';
          markup += '<div class="first-to-show">';
            markup += '<div class="close-container"><i class="uninav-btn-cancel fa fa-times"></i></div>';
            markup += '<div class="title-logo"><a href="#"><span>KAPAMILYA</span><img src="//assets.abs-cbn.com/sso/images/sso-logos.png" alt="Logo" style="height: 25px; width:25px;" loadimg="true"><span>ACCOUNTS</span></a></div>';
            markup += '<div class="title-desc">';
              markup += '<h4>ONE LOGIN TO EVERYTHING KAPAMILYA</h4>';
              markup += '<p class="slide-content-right">';
                markup += 'With your Kapamilya Name, you now have one login to your favorite Kapamilya sites.<br>';
                markup += 'Now, managing your accounts has never<br>';
                markup += 'been this easy!';
              markup += '</p>';
            markup += '</div>';
            markup += '<div class="content-body">';
              markup += '<div class="btn-container">';
                markup += '<input type="submit" class="btn-uninav sso-btn-login" value="LOGIN">';
                markup += '<div>Not yet registered? <a href="javascript:void(0);" onclick='+ssoLogin+' class="sign-up">SIGN UP</a></div>';
              markup += '</div>';
              markup += '<div class="dont-show-this-again">';
                markup += '<input type="checkbox" name="chkDontShowThisAgain" id="chkDontShowThisAgain">';
                markup += '<label for="chkDontShowThisAgain">Don&#8217;t show this again.</label>';
              markup += '</div>';
              // markup += '<div style="margin-top: 100px;"><img src="https://assets.abs-cbn.com/universalnav/img/family-is-love.png" alt="Family Is Love" style="width: 185px;"></div>';
              markup += '<div class="hub-container">';
                markup += '<i class="hub news"></i>';
                markup += '<i class="hub entertainment"></i>';
                markup += '<i class="hub sports"></i>';
                markup += '<i class="hub lifestyle"></i>';
                markup += '<br>';
                markup += '<i class="hub kty"></i>';
                markup += '<i class="hub ktx"></i>';
                markup += '<i class="hub iwantv"></i>';
                markup += '<i class="hub myx"></i>';
                markup += '<i class="hub tfc"></i>';
                // markup += '<i class="hub onemusic"></i>';
              markup += '</div>';
            markup += '</div>';
          markup += '</div>';
        markup += '</div>';

        $('body').append(markup);
      },

      loginClick: function(webSiteSource) {
        if (localStorage.getItem('dontShowThisAgain')) {
          createHeaderMarkup.goToKapamilyaLogin();
        } else {
          main.showSSOPanel();
        }
      },

      goToKapamilyaLogin: function() {
        document.cookie="returnUrl="+window.location.href+";path=/";
        localStorage.setItem('returnUrl', window.location.href);

        window.location.href = uninav.getAuthorizationURL(x);
      },

      returnSiteName: function() {
        return x.domainKey !== "" ? x.domainKey.toUpperCase() : x.dataDomain.toUpperCase();
      },

      iframe: function() {
        var urlSrc = store.kapamilyaUrl + 'checksession';
        // var urlSrc = store.kapamilyaUrl + 'signout';
        var markup = '<iframe src="'+urlSrc+'" style="display:none;"></iframe>';

        $('body').append(markup);
      },

      loadUninavLinks: function() {
        var markup = '';

        $.getJSON("https://assets.abs-cbn.com/universalnav/uninav-links-dev.json", function(data) {
          markup += '<div class="abs-cbn-links-container">';
            markup += '<div class="sso-uninav-link-title">';
              markup += 'EXPLORE';
              markup += '<span class="fa fa-times close-uninav-links"></span>';
            markup += '</div>';
            markup += '<div class="uninav-abs-cbn">';
            $.each(data.sites, function(i, item) {
              markup += '<div class="uninav-container-links">';
              if (item.parent[0].name.toLowerCase() === "abs-cbn") {
                markup += '<div class="parent-uninav '+item.parent[0].name.toLowerCase()+'">';
                  switch(x.clientId) {
                    case 'starcinema':
                    case 'entertainment':
                    case 'push':
                    case 'corphub':
                    case 'studioexperience':
                      markup += '<i class="' + item.parent[0].icon + '" style="top:14px;"></i>';
                      break;
                    default:
                      markup += '<i class="' + item.parent[0].icon + '"></i>';
                      break;
                  }
                  markup += '<a href="//www.abs-cbn.com" target="_blank" class="uninav-abs-cbn-link">' + item.parent[0].name + '</a>';
                markup += '</div>';
              } else {
                markup += '<div class="parent-uninav '+item.parent[0].name.toLowerCase()+'">';
                  switch(x.clientId) {
                    case 'starcinema':
                    case 'entertainment':
                    case 'push':
                    case 'corphub':
                    case 'studioexperience':
                      markup += '<i class="' + item.parent[0].icon + '" style="top:14px;"></i>';
                      break;
                    default:
                      markup += '<i class="' + item.parent[0].icon + '"></i>';
                      break;
                  }
                  markup += '<a href="javascript:void(0);">' + item.parent[0].name + '</a>';
                  markup += '<i class="fa fa-chevron-down toggle-uninav-links"></i>';
                markup += '</div>';
              }
                markup += '<div class="child-uninav">';
                  $.each(item.children, function(i, item) {
                    markup += '<span><a href="' + item.link + '" target="_blank" class="'+item.clsName+'">' + item.name + '</a></span>';
                  });
                markup += '</div>';
              markup += '</div>';
            });
            markup += '</div>';
          markup += '</div>';

          $('body').append(markup);
          main.markActiveSite(window.location.origin);
        });
      }
    };

    var disableScroll = false;
    var scrollPos = 0;

    var main = {
      showUninavLinks: function() {
        $('.my-blocker').show();
        $('html').css({'overflow':'hidden', '-webkit-overflow-scrolling':'touch'});
        stopScroll();

        $('.abs-cbn-links-container').addClass("uninav-links-active");
        $('.close-uninav-links').show();
        $('body').addClass('uninav-active');
      },

      closeUninavLinks: function() {
        $('.close-uninav-links').hide();
        $('.my-blocker').hide();
        $('html').css({'overflow':'auto', '-webkit-overflow-scrolling':'touch'});
        enableScroll();

        $('.abs-cbn-links-container').removeClass("uninav-links-active");
        $('body').removeClass('uninav-active');
      },

      showSSOPanel: function() {
        console.log("foo");
        $('.my-blocker').show();
        $('.close-uninav-links').hide();
        $('html').css({'overflow':'hidden', '-webkit-overflow-scrolling':'touch'});
        stopScroll();

        $('.sso-container').addClass("sso-container-active");
        $('body').addClass("sso-login-active");
      },

      closeSSOPanel: function() {
        $('.my-blocker').hide();
        $('html').css({'overflow':'auto', '-webkit-overflow-scrolling':'touch'});
        enableScroll();
      },

      markActiveSite: function(_origin) {
        switch(_origin) {
          case "https://sports.abs-cbn.com":
            if (window.location.pathname + window.location.search === "/nba?gr=www/") {
              _origin = _origin + "/nba?gr=www";
            }
            break;
          case "http://corporate.abs-cbn.com":
            var _pathname = window.location.pathname;
            if (_pathname === "/stellar/" || _pathname === "/stellar") {
              _origin = _origin + "/stellar";
            } else if (_pathname === "/lingkodkapamiya/" || _pathname === "/lingkodkapamiya") {
              _origin = _origin + "/lingkodkapamiya";
            }
            break;
          case "http://entertainment.abs-cbn.com":
          case "https://entertainment.abs-cbn.com":
            _origin = _origin + '/Tv/Home';
            break;
          case "http://news.abs-cbn.com":
          case "https://news.abs-cbn.com":
            if (window.location.pathname === "/bmpm/" || window.location.pathname === "/bmpm") {
              _origin = _origin + '/bmpm';
            }
            break;
        }

        console.log(_origin);
        $('.child-uninav a[href="' + _origin + '/"]').removeAttr('href').addClass('active-links');
      }
    };

    function stopScroll() {
      disableScroll = true;
      scrollPos = $(window).scrollTop();
    }
    function enableScroll() {
      disableScroll = false;
    }

    $(document).on('click', '.first-to-show .uninav-btn-cancel, .my-blocker, .sso-profile .uninav-btn-cancel ', function() {
      $('.sso-profile').removeClass("sso-profile-active");
      $('.sso-container').removeClass("sso-container-active");
      $('.abs-cbn-links-container').removeClass("uninav-links-active");
      $('.my-blocker').hide();
      $('html').css({'overflow':'auto', '-webkit-overflow-scrolling':'touch'});
      $('body').removeClass('uninav-active , sso-login-active');
      enableScroll();
    });

    // is-accept-privacy-policy
    $(document).on('click', '.is-accept-privacy-policy', function(e) {
      // Remove Privacy Policy Pop up
      $('.uninav-privacy-policy').hide();
      $('.privacy-policy-blocker').hide();
      $('html, body').css({'overflow':'auto', '-webkit-overflow-scrolling':'touch'});
      enableScroll();
      document.ontouchmove = function(e){ return true; };

      privacyPolicy.IsAcceptPrivacyPolicy(localStorage.getItem('uninavUserID'), 'click', DSStoreGetPrivacyApiUrl, DSStorePrivacyApiUrl, null, null); // Call this function fo check if it is already accepted to make sure.
    });

    // I'll do this later privacy policy
    $(document).on('click', '.up-do-this-later', function(e) {
      var dateNow = new Date().toUTCString();
      $('.uninav-privacy-policy').hide();
      $('.privacy-policy-blocker').hide();
      $('html, body').css({'overflow':'auto', '-webkit-overflow-scrolling':'touch'});
      $.ajax({
        type: 'POST',
        url : DSStorePrivacyApiUrl,
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: JSON.stringify({
          uid : localStorage.getItem('uninavUserID'),
          url : window.location.href,
          isAccepted : false,
        }),
        success : function(res) {
          console.log('res->',res);
          if(res.isSuccess) {
            localStorage.setItem('isAcceptedPrivacyPolicy', 'false');
            localStorage.setItem('datePrivacyPolicy', dateNow);
            localStorage.setItem('isAPIReachedLimitPrivacyPolicy', 'false');
            localStorage.setItem('isExistPrivacyPolicy', 'true');
          }
        }
      });
    });

    $(window).bind('scroll', function(){
      if (disableScroll) {
        $(window).scrollTop(scrollPos);
      } else {
        createHeaderMarkup.scrollAnimationHeader($(this));
      }
    });
    $(window).bind('touchmove', function(){
      $(window).trigger('scroll');
    });

    // on window resize change the position of uninav or sso sub menu
    $(window).resize(function() {
      if ($(".login-icon-uninav").html()) {
        var leftPosition = $('.login-icon-uninav').offset().left + 15;

        $('.triangle-top, .triangle-bottom').css('left', leftPosition+'px');
      }
    });

    $(document).ready(function() {
      $('.bookmarked').attr("disabled", "disabled");
    });

    if (x.headerSticky === false) {
      $('#header-collapse, .uninav-main').on( 'mousewheel DOMMouseScroll', function (e) {
        var e0 = e.originalEvent;
        var delta = e0.wheelDelta || -e0.detail;

        this.scrollTop += ( delta < 0 ? 1 : -1 ) * 30;
        e.preventDefault();
      });
    }

    function initialize() {
      createHeaderMarkup.headerMarkup();
      createHeaderMarkup.createSplash();
      
      // Cookie Law
      if (x.cookiePolicy[0].enable) { cookieLaw.CookieLawInit(x.cookiePolicy[0]); }

      // AdBlocker Init
      aBlock.AdBlockerInit(x.adBlocker[0]);

      if (x.sso) {setting.getAccountInformation();}

      // SSO Behavioral
      if(x.ssoBehavioral) { appendScript.appendSSOBehavioral(); }
    }

    function differentDate(d) {
      var isoDate = new Date().toUTCString(),
          dateToday =new Date(isoDate),
          getDateToday = dateToday.getDate(),

          dateFromDS = new Date(d),
          getDateFromDS = dateFromDS.getDate(),

          getTimeDiff = (getDateToday - getDateFromDS);

      console.log("Date today "+getDateToday, ", Date from DS "+d);
      console.log('Date Diff.',getTimeDiff);

      if (getDateFromDS < 10) {
        getDateFromDS = '0' + getDateFromDS;
      }
      if(getTimeDiff >= 5) {
        return true;
      }
      else {
        return false;
      }
    }

    function cookieLawMarginAdjustment() {
      var hostname = window.location.hostname;
      var pathname = window.location.pathname;
      switch(hostname) {
        case "localhost":
        case "qa-corporate.abs-cbn.com":
        case "staging.entertainment.abs-cbn.com":
        case "staging-lifestyle.abs-cbn.com":
        case "qa-universal.abs-cbn.com":
        case "thankyou-ucdev.abs-cbn.com":
        case "test.entertainment.abs-cbn.com":
          $('.uc-sub-content').css({ 'margin-top': '0px' });
          break;
        case "test.abs-cbnnews.com":
          $('.uc-sub-content').css({'margin-top':'15px'});
          break;
        case "staging-dev2.abs-cbn.com":
          if (pathname.indexOf('advisory') !== -1 ) {
            $('.uc-sub-content').css({ 'margin-top': '-5px' }); // Set for /advisory site
          } else if (pathname.indexOf('studioexperience') !== -1 || pathname.indexOf('lightscameramagic') !== -1) {
            $('.uc-sub-content').css({ 'margin-top': '0px' }); // Set for /advisory site
          }
          break;
        case "staging-dev.abs-cbn.com":
          if (pathname.indexOf('tvconnect') !== -1 || pathname.indexOf('audition') !== -1) {
            $('.uc-sub-content').css({ 'margin-top': '0px' }); // Set for /advisory site
          }
          break;
        default:
          console.log(hostname);
      }
    }

    $('head').prepend('<link rel="stylesheet" media="screen" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">');


    initialize();
  };
}(jQuery));
