var aBlock = {};

// Initialize
var _AdBlockerTimer = false; 
aBlock.AdBlockerInit = function (uAdBlock) {
    $('.uninav-ad-blocker-container').fadeOut(function () {
        $('.uninav-ad-blocker-container').remove();
    });

    aBlock.detect();

    window.setTimeout(function () {
        if ((typeof uAdBlock.enable !== 'undefined' && uAdBlock.enable === true) && _adBlockEnabled === true) {
            if (_AdBlockerTimer !== false) { clearTimeout(_AdBlockerTimer); }
            _AdBlockerTimer = setTimeout(function () {
                aBlock.createAdBlocker(uAdBlock);
            }, 7000);
        }
    }, 200);   
};

// MarkUp
aBlock.createAdBlocker = function (uAdBlock) {
    var headerImage = uAdBlock.headerImage ; //? uAdBlock.headerImage : '//assets.abs-cbn.com/universalnav/img/ring-md.png';
    var title = uAdBlock.title; // ? uAdBlock.title : 'Seems like your AdBlocker is on, Kapamilya!';
    var logo = uAdBlock.logo; //? uAdBlock.logo : '//assets.abs-cbn.com/universalnav/img/logo-abs-cbn-black.png'
    var contentCopy = uAdBlock.contentCopy ? uAdBlock.contentCopy : '<p>This site supports ads.</p><p>To enjoy uninterrupted browsing,</p><p>simply deactivate your <b>AdBlocker</b> now.</p>';

    var _markup = '';
    _markup += '<div class="uninav-ad-blocker-container">';
    
        if (headerImage !== 'none') {
            var uheaderImage = uAdBlock.headerImage ? uAdBlock.headerImage : '//assets.abs-cbn.com/universalnav/img/ring-md.png';
            _markup += '<div class="uabc-header" style="background-image : url(' + uheaderImage + ')"></div>';
        } else {
            _markup += '<div class="uabc-header" style="background-image : url()"></div>';
        } 

        _markup += '<div class="uabc-col-cont">';
            _markup += '<div class="uabc-col">';

                if(logo !== 'none') {
                    var ulogo = uAdBlock.logo ? uAdBlock.logo : '//assets.abs-cbn.com/universalnav/img/logo-abs-cbn-black.png';
                    _markup += '<img src="' + ulogo + '" alt="Logo">';
                }

                if(title !== 'none') {
                    var utitle = uAdBlock.title ? uAdBlock.title : 'Seems like your AdBlocker is on, Kapamilya!';
                    _markup += '<h3>' + utitle + '</h3>';
                }
                
                _markup += contentCopy;
                _markup += '<div class="uabc-steps">';
                    _markup += '<h4>Here\'s how: </h4>';
                    _markup += '<p>1. Open your browser\'s Extensions page.</p>';
                    _markup += '<p>2. Look for the AdBlock extension. </p>';
                    _markup += '<p>3. Click \'Disable\'.</p>';
                _markup += '</div>';
                _markup += '<button id="got-it" type="button">Got It</button>';
            _markup += '</div>';
            _markup += '<div class="uabc-col">';
                _markup += '<img src="//assets.abs-cbn.com/universalnav/img/blocker.png" alt="ADBLOCKER">';
            _markup += '</div>';
        _markup += '</div>';
    _markup += '</div>';
    $('body').append(_markup);
    $('.uninav-ad-blocker-container').hide().fadeIn();
};

// Detect
var _adBlockEnabled = false;
aBlock.detect = function () {
    var testAd = document.createElement('div');
    testAd.innerHTML = '&nbsp;';
    testAd.className = 'adsbox';
    document.body.appendChild(testAd);

    window.setTimeout(function () {
        if (testAd.offsetHeight === 0) {
            _adBlockEnabled = true;
        }
        testAd.remove();
        // console.log('AdBlock Enabled? ', _adBlockEnabled);
        return _adBlockEnabled;
    }, 100);
};
