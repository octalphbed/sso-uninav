(function($) {
  'use strict';
  // Enable cache for Script tags
  // $.ajaxPrefilter(function( options, originalOptions, jqXHR ) {
  //   if ( options.dataType === 'script' || originalOptions.dataType === 'script' ) {
  //       options.cache = true;
  //   }
  // });

  $.fn.uninav = function(options) {
    var _this = this,
    defaults = {
      //Basic variables
      header: $('header'),
      window: $(window),
      body: $('body'),
      domain: document.domain,
      main: $('div.main'),
      document: $(document),

      //Header variables
      navbar: $('.header-nav-toggle'),
      navbarCollapse: $('#header-collapse'),
      target: _this,
      type: '',
      uninav: false,
      uninavTarget: '.uninav-main',
      uninavIconTarget: $('#uninav-toggle'),
      uninavAnimation: true,
      uninavAnimationSpeed: 'fast',
      uninavHighLight: '',
      mobileToggler: $('.has-child-drop'),
      navbarToggler: true,
      mobileUninav: $('#mobile-uninav'),
      headerSticky: true,
      mobileNav: true,
      dataDomain: '',
      oldsearch: true,

      //SSO variables
      sso: true,
      ssoLogin: true,
      ssoTarget: $('.uninav-icon-login'),
      ssoMobileTarget: $('.uninav-icon-login-new'),
      ssoStatus: true,
      ssoApikey: '',
      customDashBoard: [],
      kapamilyaImage: '',
      kapamilyaImagemobile: '',
      kapamilyaImageDashboard: '',
      sourceURL: '',
      domainKey: '',
      referrer: '',
      ssoPageURL: store.kapamilyaUrl //'http://staging-kapamilyaaccounts.abs-cbn.com/'
  },
  
    x = this.config = $.extend(defaults, options);

    var getURL = {
      getWebsiteName: function() {
        var webSiteName = "";
        if (getURL.getURLSites("news") >= 0) {
          webSiteName = "NEWS";
        } else if (getURL.getURLSites("entertainment") >= 0) {
          webSiteName = "ENTERTAINMENT";
        } else if (getURL.getURLSites("mmk") >= 0) {
          webSiteName = "MMK";
        } else if (getURL.getURLSites("bmpm") >= 0) {
          webSiteName = "BMPM";
        } else if (getURL.getURLSites("onemusic") >= 0) {
          webSiteName = "ONE MUSIC";
        } else if (getURL.getURLSites("starcinema") >= 0) {
          webSiteName = "STARCINEMA";
        } else if (getURL.getURLSites("sinehub") >= 0) {
          webSiteName = "SINEHUB";
        } else if (getURL.getURLSites("iwantv") >= 0) {
          webSiteName = "I WANT TV";
        } else if (getURL.getURLSites("sports") >= 0) {
          webSiteName = "SPORTS";
        } else if (getURL.getURLSites("lifestyle") >= 0) {
          webSiteName = "LIFESTYLE";
        } else if (getURL.getURLSites("sky") >= 0) {
          webSiteName = "SKY.";
        } else if (getURL.getURLSites("skyondemand") >= 0) {
          webSiteName = "SKY ON DEMAND";
        } else if (getURL.getURLSites("tfc") >= 0) {
          webSiteName = "TFC TV";
        } else {
          webSiteName = "KAPAMILYA";
        }
        return webSiteName;  
      },

      getIconClassByReferrrer: function() {
        referrer = getURL.getCookie("Referrer").split('?')[0];

        if (getURL.getUrlParameter('referrer')) {
          referrer = getURL.getUrlParameter("referrer");
        }

        if (referrer.indexOf("news") >= 0) {
          iconClass = "news";
        } else if (referrer.indexOf("entertainment") >= 0) {
          iconClass = "entertainment";
        } else if (referrer.indexOf("mmk") >= 0) {
          iconClass = "mmk";
        } else if (referrer.indexOf("bmpm") >= 0) {
          iconClass = "default";
        } else if (referrer.indexOf("onemusic") >= 0) {
          iconClass = "onemusic";
        } else if (referrer.indexOf("starcinema") >= 0) {
          iconClass = "default";
        } else if (referrer.indexOf("sinehub") >= 0) {
          iconClass = "default";
        } else if (referrer.indexOf("starcinema2") >= 0) {
          iconClass = "default";
        } else if (referrer.indexOf("iwantv") >= 0) {
          iconClass = "iwantv";
        } else if (referrer.indexOf("tfc") >= 0) {
          iconClass = "tfctv";
        } else {
          iconClass = "default";
        }
        return iconClass;  
      },

      getURLSites: function(sName) {
        // return "www.test.news.abs-cbn.com".indexOf(sName); /*FOR TESTING ON LOCAL*/
        if (getURL.getUrlParameter('referrer')) {
          return getURL.getUrlParameter("referrer").indexOf(sName);
        }

        return window.location.href.split('?')[0].indexOf(sName);
      },

      getApiReferrerKey: function(useForApi, data) {
        // document.cookie = "Referrer=http://test.entertainment.abs-cbn.com/tv/home";
        var referrer = getURL.getCookie("Referrer");
    
        if (getURL.getUrlParameter('referrer')) {
          referrer = getURL.getUrlParameter("referrer");
        }

        var apiKeys = "";
        console.log("referrer : ", referrer);

        if (referrer !== "") {
          referrer = referrer.split("?")[0];

          if (referrer.indexOf("news") >= 0) {
            apiKeys = useForApi ? data.news : "NEWS";
          } else if (referrer.indexOf("kapamilyaaccounts") >= 0) {
            apiKeys = useForApi ? data.entertainment : "ACCOUNTS";
          } else if (referrer.indexOf("bmpm") >= 0) {
            apiKeys = useForApi ? data.bmpm : "BMPM";
          } else if (referrer.indexOf("entertainment") >= 0) {
            apiKeys = useForApi ? data.entertainment : "ENTERTAINMENT";
          } else if (referrer.indexOf("iwantv") >= 0) {
            apiKeys = useForApi ? data.iwantv : "I WANT TV";
          } else if (referrer.indexOf("mmk") >= 0) {
            apiKeys = useForApi ? data.mmk : "MMK";
          } else if (referrer.indexOf("sinehub") >= 0) {
            apiKeys = useForApi ? data.sinehub : "SINEHUB";
          } else if (referrer.indexOf("starcinema") >= 0) {
            apiKeys = useForApi ? data.starcinema : "STAR CINEMA";
          } else if (referrer.indexOf("starcinema2") >= 0) {
            apiKeys = useForApi ? data.starcinema : "STAR CINEMA";
          } else if (referrer.indexOf("onemusic") >= 0) {
            apiKeys = useForApi ? data.onemusic : "ONE MUSIC";
          } else if (referrer.indexOf("kapamilyathankyou") >= 0) {
            apiKeys = useForApi ? data.kapamilyathankyou : "KAPAMILYA THANK YOU";
          } else if (referrer.indexOf("tfc") >= 0) {
            apiKeys = useForApi ? data.tfctv : "TFC";
          } else if (referrer.indexOf("starcreative") >= 0) {
            apiKeys = useForApi ? data.starcreative : "STAR CREATIVE";
          } else if (referrer.indexOf("sports") >= 0) {
            apiKeys = useForApi ? data.sports : "SPORTS";
          } else if (referrer.indexOf("lifestyle") >= 0) {
            apiKeys = useForApi ? data.lifestyle : "LIFESTYLE";
          } else if (referrer.indexOf("KapamilyaThankyou") >= 0 || referrer.indexOf("129.144.41") >= 0) {
            apiKeys = useForApi ? data.kapamilyathankyou : "KAPAMILYA THANK YOU";
          } else if (referrer.indexOf("cablechannels") >= 0) {
            apiKeys = useForApi ? data.cablechannels : "CABLE CHANNELS";
          } else if (referrer.indexOf("ott") >= 0) {
            apiKeys = useForApi ? data.ott : "OTT";
          } else if (referrer.indexOf("ktx") >= 0) {
            apiKeys = useForApi ? data.ktx : "KTX";
          } else if (referrer.indexOf("noink") >= 0) {
            apiKeys = useForApi ? data.noink : "NOINK";
          }
        } else {
          apiKeys = useForApi ? data.entertainment : "KAPAMILYA"; // Just For Localhost
        }

        return apiKeys;
      },

      getCookie: function(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i = 0; i < ca.length; i++) {
          var c = ca[i];
          while (c.charAt(0) === ' ') {
            c = c.substring(1);
          }
          if (c.indexOf(name) === 0) {
            return c.substring(name.length,c.length);
          }
        }
        return "";
      },
      getUrlParameter: function(sParam) {
          var sPageURL = decodeURIComponent(window.location.search.substring(1)),
              sURLVariables = sPageURL.split('&'),
              sParameterName,
              i;

          for (i = 0; i < sURLVariables.length; i++) {
              sParameterName = sURLVariables[i].split('=');

              if (sParameterName[0] === sParam) {
                  return sParameterName[1] === undefined ? true : sParameterName[1];
              }
          }
      }
    };

    function setPageLocation(url) {
      if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)){
          var referLink = document.createElement('a');
          referLink.href = url;
          document.body.appendChild(referLink);
          referLink.click();
      } else {
          location.href = url;
      }
    }

    function uninavSSO() {
      $.getJSON(store.domainKeys, function(data) {
        // body...
      }).done(function(data) {
        if(x.domainKey !== "") { 
          x.ssoApikey = data[x.domainKey];
        } else {
          x.ssoApikey = data[x.dataDomain];
          // switch (window.location.hostname) {
          //   case '1eb5e7dfe0b6437ca7449539cd5412c6.cloudapp.net':
          //   case 'devnews.abs-cbn.com':
          //   case 'testnews.abs-cbn.com':
          //   case 'stagingnews.abs-cbn.com':
          //     x.ssoApikey = data.news;
          //     break;
          //   case 'staging-dev.abs-cbn.com':
          //     if (window.location.pathname.indexOf("starcreative") === 1) {
          //       x.ssoApikey = data.starcreative;
          //     } else if (window.location.pathname.indexOf("news") === 1) {
          //       x.ssoApikey = data.news;
          //     }
          //     break;
          //   case 'staging-lifestyle.abs-cbn.com':
          //     x.ssoApikey = data.lifestyle;
          //     break;
          //   case 'dev.sports.abs-cbn.com':
          //     x.ssoApikey = data.sports;
          //     break;
          //   case 'dev.mmk.abs-cbn.com':
          //   case 'conceptmachine.net':
          //   case 'www.conceptmachine.net':
          //     x.ssoApikey = data.mmk;
          //     break;
          //   case 'dev.entertainment.abs-cbn.com':
          //   case 'test.entertainment.abs-cbn.com':
          //   case 'dev.entertainment.abs-cbn.com':
          //   case 'staging.entertainment.abs-cbn.com':
          //     x.ssoApikey = data.entertainment;
          //     break;
          //   case 'dev.bmpm.abs-cbn.com':
          //   case 'bmpm-staging.kudosweb.com':
          //     x.ssoApikey = data.bmpm;
          //     break;
          //   case 'test-starcinema.abs-cbn.com':
          //   case 'starcinema2.abs-cbn.com':
          //     x.ssoApikey = data.starcinema;
          //     break;
          //   case '10.0.7.167':
          //   case '129.144.41.26':
          //   case '129.144.41.33':
          //   case '129.144.41.20':
          //     x.ssoApikey = data.kapamilyathankyou;
          //     break;
          //   case 'tfc.tv':
          //   case 'beta.tfc.tv':
          //     x.ssoApikey = data.tfctv;
          //     break;
          //   case 'starcreative.dev':
          //   case 'dev.starcreatives.abs-cbn.com':
          //     x.ssoApikey = data.starcreative;
          //     break;

          //   case 'localhost':
          //   case 'staging-kapamilyaaccounts.abs-cbn.com':
          //     x.ssoApikey = getURL.getApiReferrerKey(true, data);   
          //     if (window.location.pathname === "/sms-login" || window.location.pathname === "/sms-sign-up" || window.location.pathname === "/sms-login-no-reset") {
          //         x.ssoApikey = data.otp;
          //         // remove this after the OTP setup for each dev child site is finished
          //         if (getURL.getUrlParameter('referrer') && getURL.getUrlParameter('referrer').indexOf('entertainment') >= 0) {
          //           x.ssoApikey = data.entertainment;
          //         } else if (getURL.getUrlParameter('referrer') && getURL.getUrlParameter('referrer').indexOf('iwantv') >= 0) {
          //           x.ssoApikey = data.iwantv;
          //         }
          //     } else {
          //       x.ssoApikey = getURL.getApiReferrerKey(true, data);
          //     }
          //     break;
          // }
        }

        console.log("API KEY : ", x.ssoApikey);

        var gigyaConf = "";
        if (window.__gigyaConf === undefined) {
          gigyaConf = '<script type="text/javascript">window.__gigyaConf = {"enableSSOToken": true}</script>';
        }

        var gigyaScript;
        var timestamp = new Date().getTime();
        if (window.location.protocol === "https:") {
          gigyaScript = '<script type="text/javascript" src="https://cdns.gigya.com/JS/gigya.js?apikey=' + x.ssoApikey + '&timestamp='+timestamp+'"></script>';
        } else {
          gigyaScript = '<script type="text/javascript" src="http://cdn.gigya.com/JS/gigya.js?apikey=' + x.ssoApikey + '&timestamp='+timestamp+'"></script>';
        }
        
        $('head').prepend(
          gigyaConf + gigyaScript + '<script type="text/javascript" src="//assets.abs-cbn.com/sso/sso-behavior.js"></script>'
        );

        ssoChecker();
      });

      function logoutFromComment() {
        gigya.accounts.addEventHandlers({
          onLogout: function() {
            setPageLocation(x.ssoPageURL + '/logout');
          }
        });
      }

      function ssoChecker() {
        $("script[src*='gigya.com/JS/gigya.js?apikey=" + x.ssoApikey + "']").load(function() {
          checkGigya();
        });
      }

      function checkGigya() {
        if (typeof gigya === undefined) {
          console.log("please wait giya function is still loading");
          ssoChecker();
        } else {
          ssoApiCall();
        }
      }
      
      function ssoApiCall() {
        gigya.accounts.getAccountInfo({
          include: "loginIDs,profile,data",
          callback: function(res) {
            console.log("Request From Gigya : ", res);

            if (res.status === "FAIL") {
              button.activate([], true);
              $('.login-icon-uninav').html(createHeaderMarkup.notLogin());
              $('body').attr('data-login', 'false');
            } else {
              save.list(res.data);
              $('.login-icon-uninav').html(createHeaderMarkup.alreadyLogin(res));
              createHeaderMarkup.createSSOsubNavMarkup(res);
              logoutFromComment();

              $('body').attr('data-login', 'true');
            }
          }
        });
      }
    }

    var button = {
      activate: function(tempData, showPopup) {
        $('.bookmarked').removeAttr("disabled");
        $(document).on('click', '.bookmarked', function(e) {
          e.preventDefault();
          // check if not login. then show popup
          if (showPopup) {
            var message = '<a href="'+x.ssoPageURL+'"><strong>Login</strong> to your Kapamilya Account to save this article and read it later.</a>';
            save.bubblePopup(message, $(this).parent(), 6000);
            return;
          }

          var _this = $(this);
          if(!_this.hasClass("article-saved")) {
            var blocker = '<div class="my-blocker"></div><div class="loader-icon"></div>';
            _this.parent().append(blocker);
            
            var article = _this.parents('article');

            var id = new Date().getTime().toString();
            var dSave = button.getDateNow();
            var title = article.find('meta[itemprop="headline"]').attr("content");
            var sDesc = article.find('meta[itemprop="description"]').attr("content");
            var thumb = article.find('meta[itemprop="thumbnailUrl"]').attr("content");
            var sUrl = article.find('meta[itemprop="url"]').attr("content");
            var sSrc = $('meta[property="og:site_name"]').attr("content");

            var dataHolder = button.createTempData(id, title, sDesc, dSave, thumb, sUrl, sSrc);
            tempData.push(dataHolder);

            save.article(tempData, _this);
          }
        });
      },

      createTempData: function(id, title, sDesc, dSave, thumb, sUrl, sSrc) {
        var temp = {
          articleId: id,
          title: title,
          shortDesc: sDesc,
          saveDate: dSave,
          thumbnail: thumb,
          urlLink: sUrl,
          siteSrc: sSrc,
          readed: false
        };

        return temp;
      },

      getDateNow: function() {
        var date = new Date();
        var year = date.getFullYear();
        var day = date.getDate();
        var month = date.getMonth();

        switch (month) {
          case 0: month = "January"; break;
          case 1: month = "Febuary"; break;
          case 2: month = "March"; break;
          case 3: month = "April"; break;
          case 4: month = "May"; break;
          case 5: month = "June"; break;
          case 6: month = "July"; break;
          case 7: month = "August"; break;
          case 8: month = "September"; break;
          case 9: month = "October"; break;
          case 10: month = "November"; break;
          case 11: month = "December"; break;
        }

        return month + " " + day + ", " + year;
      },

      scrollActivate: function(article) {
        save.artLen = $('.bookmarked').parents('article').length;
        $(window).scroll(function() {
          // if($(window).scrollTop() + $(window).height() === $(document).height()) {
          //   var str = '<article><meta itemprop="headline" content="Star Cinema Chat with Piolo Pascual | Star Cinema"><meta itemprop="description" content=""><meta itemprop="thumbnailUrl" content="https://data-starcinema.abs-cbn.com/starcinema/starcinema/media/photo-gallery-northern-lights/publicity%20photos/8-coverr.jpg?ext=.jpg"><meta itemprop="url" content="http://test-starcinema.abs-cbn.com/2017/3/17/photos/northern-lights-publicity-photos-22519"><meta property="og:site_name" content="STAR CINEMA"><div class="bookmarked-saved-container landscape"><button class="bookmarked"><span>Save</span></button></div></article>';
          //   $('.main').append(str);
          //   return;
          // }

          var newLen = $('.bookmarked').parents('article').length;
          if (save.artLen < newLen) {
            save.artLen = newLen;
            save.setSavedArticle();
          }
        });
      }
    };

    var save = {
      data: [],
      title: [],
      artLen: 0,

      list: function(data) {
        if(data.readList) {
          save.data = data.readList;
          save.setTitleHolder(data.readList);

          button.activate(save.data, false);
        }
      },

      setTitleHolder: function(data) {
        $.each(data, function(i, val) {
          save.title.push(val.title);
        });

        // set active for all those article saved.
        save.setSavedArticle(save.title);
      },

      setSavedArticle: function() {
        var article = $('.bookmarked').parents('article');
        // save.artLen = article.length;

        $.each(article, function(i, val) {
          var _this = $(val);
          var title = _this.find('meta[itemprop="headline"]').attr("content");
          var container = _this.find(".bookmarked-saved-container");

          if(save.isSaved(save.title, title)) {
            container.html('<button class="bookmarked article-saved"><span>Saved</span></button>');
          } else {
            container.html('<button class="bookmarked"><span>Save</span></button>');
          }
        });

        button.scrollActivate();
      },

      isSaved: function(a, b) {
        return a.indexOf(b) > -1;
      },

      article: function(data, _this) {
        var params = {
          data: {
            readList: data
          },
          callback: function(response) {
            if (response.errorCode === 0) { 
              _this.parent().find('.my-blocker').remove();
              _this.parent().find('.loader-icon').remove();
              _this.addClass("article-saved");
              _this.find('span').html("Saved");
            } else {
              readingList.createTempBubblePopup("An error has occurred!<br>Error details: " + response.errorMessage + "<br>Error code: " + response.errorCode + "<br>In method: " + response.operation, 5000);
            }
          }
        };

        console.log("params : ", params);
        gigya.accounts.setAccountInfo(params);
      },

      bubblePopup: function(str, container, closeTime) {
        $('.bookmark-saved').remove();
        var bubblePopup = $('<div class="bookmark-saved">'+str+'</div>').fadeIn("fast").appendTo(container);
        
        if (!closeTime) {
          closeTime = closeTime;
        } else {
          closeTime = 3000;
        }

        var timeout = setInterval(function() {
          bubblePopup.fadeOut("slow");
          bubblePopup.remove();
          clearInterval(timeout);
        }, closeTime);
      },
    };

    // Clickable Functions
    $(document).on('click', 'body', function(){
      $('.uninav-main, .triangle-top, .triangle-bottom').hide();
      $('.gigya-expanded-profile').css("cssText", "display: none;");
      createHeaderMarkup.isTemp = 0;
    });
    $(document).on('click', '.uninav-main, .gigya-expanded-profile', function(e){e.stopPropagation();});

    $(document).on('click', '.network-uninav', function(e) {
      e.stopPropagation();
      $('.gigya-expanded-profile, .triangle-top, .triangle-bottom').hide();
      $('.uninav-main').slideToggle("fast");
      createHeaderMarkup.isTemp = 0;
    });

    $(document).on('click', '.login-icon-uninav', function(e) {
      e.stopPropagation();
      if (!$(this).hasClass('active') && !$(this).hasClass('without-photo')) {
        var siteName = createHeaderMarkup.returnSiteName().toLowerCase();
        createHeaderMarkup.loginClick(siteName);
      } else {
        $('.uninav-main').hide();
        var leftPosition = $(this).offset().left + 15;

        $('.triangle-top, .triangle-bottom').css({'left' : leftPosition+'px'});
        $('.triangle-top, .triangle-bottom').toggle();
        if (createHeaderMarkup.isTemp === 0) {
          $('.gigya-expanded-profile').css("cssText", "display: block; left: initial !important; top:40px !important");
          createHeaderMarkup.isTemp = 1;
        } else {
          $('.gigya-expanded-profile').css("cssText", "display: none;");
          createHeaderMarkup.isTemp = 0;
        }
      }
    });

    $(document).on('click', 'header .header-bar button.header-nav-toggle', function(e) {
      e.stopPropagation();
      $(this).toggleClass('collapsed');
      $('#header-collapse').toggleClass('in');
      
      if ($(this).hasClass('collapsed')) {
        $(this).attr('aria-expanded', 'false');
        $('#header-collapse').attr('aria-expanded', 'false');
      } else {
        $(this).attr('aria-expanded', 'true');
        $('#header-collapse').attr('aria-expanded', 'true');
      }
    });

    if (x.oldsearch === false) {
      $(document).on('click', '.uninav-icon-search', function(e) {
        e.preventDefault();
        var _this = $(this).parent();
        _this.toggleClass('active');
        
        if (_this.is('.active')) {
          $('header').append(createHeaderMarkup.createSearchPopup());
          $('#mobile-search').fadeIn("fast");
          $('body, html').css({ "overflow": "hidden" });
        } else {
          $('#mobile-search').fadeOut().remove();
          $('body, html').css({ "overflow": "" });
        }
      });

      $(document).on('focus', 'input[mobile-input="search"]', function() {
        $(this).siblings('span').stop().addClass('active');
      });
      
      $(document).on('focusout', 'input[mobile-input="search"]', function() {
        if ($('input[mobile-input="search"]').val() === "") {
          $(this).siblings('span').stop().removeClass('active');
        } else {
          $(this).siblings('span').stop().addClass('active');
          $('input[mobile-input="search"]').val();
        }
      });
      
      $(document).on('click', '.search-form span', function() {
        $('input[mobile-input="search"]').focus();
      });

      
      $(document).on('touchstart click', '.uninav-close', function(e) {
        e.preventDefault();
        $('#mobile-search').remove();
        $('.uninav-icon-search-new').removeClass('active');
        $('body, html').css({ "overflow": "" });
        $('.uninav-icon-search').parent().removeClass('active');
      });
    } else if (x.oldsearch === true) {
      $('.search-container-lg .search-container').show();
      $('.uninav-icon-new.uninav-icon-search').parents('li').hide();
    }

    $(document).on('touchstart click', '.uninav-close', function(e) {
      e.preventDefault();
      $('#mobile-search').remove();
      $('.uninav-icon-search-new').removeClass('active');
      $('body, html').css({ "overflow": "" });
      $('.uninav-icon-search').parent().removeClass('active');
    });

    $(document).on("click", ".dashboard-logout", function() {
      gigya.accounts.logout();
    });

    $(document).on('click', '.btn-cancel', function() {
      $('.lightbox').hide();
    });

    $(document).on('click', '.has-child-drop', function() {
      $(this).parent().find("ul").slideToggle("fast");
    });

    $(document).on('click', '.btn-proceed', function() {
      createHeaderMarkup.goToKapamilyaLogin();
    });

    // Markup Header Creation
    var createHeaderMarkup = {
      isTemp: 0,
      isActive: true,
      headerMarkup: function() {
        $('#uninav-toggle').remove();
        var markup = '';
        
        markup += '<div class="main-uninav-header">';
          markup += '<div class="right-container login-icon-uninav">';
            markup += '<span class="login-loading">LOADING..</span>';
          markup += '</div>';
          markup += '<div class="left-container network-uninav">';
            markup += 'NETWORK';
          markup += '</div>';
        markup += '</div>';

        $('header.uninav-header').append(markup);
        $('header.uninav-header .header-fixed').css('top', '30px');
        createHeaderMarkup.uninavMarkup();

        if (x.oldsearch === false) {
          $('.header-bar-inner h1.logo').append(createHeaderMarkup.createSearchIcon());
        }
      },

      createSearchIcon: function() {
        var markup = '<a href="javascript:void(0);" style="position: absolute; top: 0; right: 0; width: 50px;"><i class="uninav-icon-new uninav-icon-search"><span class="sr-only">search</span></i></a>';
        return markup;
      },

      userLoginMarkup: function(acc) {
        var markup = '';
        if(acc.status === 'FAIL') {
          $('.login-icon-uninav').addClass('inactive');
          markup += 'LOGIN';
        } else {
          if (acc.profile.photoURL === undefined) {
            $('.login-icon-uninav').addClass('without-photo');
          } else {
            $('.login-icon-uninav').addClass('active');
            var styles = "background-image:url("+acc.profile.photoURL+");width: 20px; height: 20px;display: inline-block; border-radius: 50%; background-size: 100% 100%; position: relative; margin: 0px 5px 0px 15px; top: 5px;";
            markup += '<i style="'+styles+'"></i>';
          }

          markup += acc.loginIDs.username;
        }
        
        return markup;
      },
      
      notLogin: function() {
        $('.login-icon-uninav').addClass('inactive');
        return 'LOGIN';
      },

      alreadyLogin: function(acc) {
        var markup = '';
        if (acc.profile.photoURL === undefined) {
          $('.login-icon-uninav').addClass('without-photo');
        } else {
          $('.login-icon-uninav').addClass('active');
          var styles = "background-image:url("+acc.profile.photoURL+");width: 20px; height: 20px;display: inline-block; border-radius: 50%; background-size: 100% 100%; position: relative; margin: 0px 5px 0px 15px; top: 5px;";
          markup += '<i style="'+styles+'"></i>';
        }

        markup += acc.loginIDs.username;

        return markup;
      },

      ssLogoutNavMarkup: function() {
        return '<span class="login-icon-uninav"><span>LOGIN</span></span>';
      },

      markActive: function() {
        switch (x.domain) {
        /* Put overrides or special links here */
        case 'oneforpacman.abs-cbn.com': // TODO: override directly
          x.domain = 'sports.abs-cbn.com';
          break;
        case 'entertainment.abs-cbn.com':
        case 'entertainment2.abs-cbn.com':
          x.domain = 'http://entertainment.abs-cbn.com/';
          break;
        default:
          x.domain = 'http://' + x.domain + '/';
          break;
        }

        $('a[href="' + x.domain + '"]').removeAttr('href').addClass('active');
      },

      uninavMarkup: function() {
        var markup ='<div class="uninav-main" style="top:30px;">' +
                      '<div class="uninav-container">' +
                        '<h2 class="uninav-main-heading">' +
                          'Welcome to your one-stop hub for everything Kapamilya!<br>Where would you like to go next?' +
                        '</h2>' +
                      '</div>' +
                      '<div class="uninav-main-divider"></div>' +
                      '<div class="uninav-container">' +
                        '<div class="uninav-main-links clearfix">' +
                          '<div class="uninav-main-links-bp-left">' +
                            '<ul class="uninav-main-links-group">' +
                              '<li><a target="_blank" href="http://entertainment.abs-cbn.com/">ABS-CBN Entertainment</a></li>' +
                              '<li><a target="_blank" href="http://news.abs-cbn.com/">ABS-CBN News</a></li>' +
                              '<li><a target="_blank" href="http://sports.abs-cbn.com/">ABS-CBN Sports</a></li>' +
                              '<li><a target="_blank" href="http://lifestyle.abs-cbn.com/">ABS-CBN Lifestyle</a></li>' +
                              '<li><a target="_blank" href="http://store.abs-cbn.com/">ABS-CBN Store</a></li>' +
                              '<li><a target="_blank" href="http://starcinema.abs-cbn.com/">Star Cinema</a></li>' +
                              '<li><a target="_blank" href="http://starmusic.abs-cbn.com/">Star Music</a></li>' +
                              '<li><a target="_blank" href="http://www.onemusic.ph/">One Music</a></li>' +
                              '<li><a target="_blank" href="http://chickenporkadobo.abs-cbn.com/">Chicken Pork Adobo</a></li>' +
                            '</ul>' +
                            '<ul class="uninav-main-links-group">' +
                              '<li><a target="_blank" href="http://www.mor1019.com/">MOR 101.9</a></li>' +
                              '<li><a target="_blank" href="http://myxph.com/">MYX PH</a></li>' +
                              '<li><a target="_blank" href="http://push.abs-cbn.com/">Push</a></li>' +
                              '<li><a target="_blank" href="http://www.choosephilippines.com/">Choose Philippines</a></li>' +
                              '<li><a target="_blank" href="http://bmpm.abs-cbnnews.com/">BMPM</a></li>' +
                              '<li><a target="_blank" href="http://dzmm.abs-cbnnews.com/">DZMM</a></li>' +
                            '</ul>' +
                          '</div>' +
                          '<div class="uninav-main-links-bp-right">' +
                            '<ul class="uninav-main-links-group">' +
                              '<li>' +
                                '<ul>' +
                                  '<li><a target="_blank" href="https://tfcmyremit.com/">TFC MyRemit</a></li>' +
                                  '<li><a target="_blank" href="http://www.tfc.tv/">TFC.TV</a></li>' +
                                '</ul>' +
                              '</li>' +
                              '<li>' +
                                '<ul>' +
                                  '<li><a target="_blank" href="http://abscbnmobile.com/">ABS-CBNmobile</a></li>' +
                                  '<li><a target="_blank" href="http://www.iwantv.com.ph/">iWant TV</a></li>' +
                                  '<li><a target="_blank" href="http://tvplus.abs-cbn.com/">ABS-CBN TVplus</a></li>' +
                                '</ul>' +
                              '</li>' +
                              '<li>' +
                                '<ul>' +
                                  '<li><a target="_blank" href="http://www.mysky.com.ph/">SKY</a></li>' +
                                  '<li><a target="_blank" href="http://www.mysky.com.ph/skyondemand">SKY On-Demand</a></li>' +
                                '</ul>' +
                              '</li>' +
                            '</ul>' +
                            '<ul class="uninav-main-links-group">' +
                              '<li><a target="_blank" href="http://corporate.abs-cbn.com/">Corporate</a></li>' +
                              '<li><a target="_blank" href="http://corporate.abs-cbn.com/investorrelations/">Investor Relations</a></li>' +
                              '<li><a target="_blank" href="http://corporate.abs-cbn.com/careers/">Careers</a></li>' +
                              '<li><a target="_blank" href="http://www.abs-cbnfoundation.com/">ABS-CBN Foundation</a></li>' +
                              '<li><a target="_blank" href="http://internationalsales.abs-cbn.com/">International Sales</a></li>' +
                              '<li><a target="_blank" href="http://marketportal.abs-cbn.com/">Market Portal</a></li>' +
                              '<li><a target="_blank" href="http://starmagicphils.abs-cbn.com/">Star Magic</a></li>' +
                            '</ul>' +
                          '</div>' +
                        '</div>' +
                      '</div>' +
                    '</div>';
        $('header').append(markup);
        createHeaderMarkup.markActive();
      },

      createSSOsubNavMarkup: function(res) {
        var imgSrc = res.profile.photoURL ? res.profile.photoURL : 'http://assets.abs-cbn.com/universalnav/img/no-image-logo.png';
        var fName = (res.profile.firstName) ? res.profile.firstName : "";
        var lName = (res.profile.lastName) ? res.profile.lastName : "";

        var markup = '';
        markup += '<div class="triangle-top"></div><div class="triangle-bottom"></div>';
        markup += '<div class="gigya-expanded-profile">';
          markup += '<div class="gigya-expanded-profile-account">';
            markup += '<img class="gigya-expanded-photo" src="' + imgSrc + '"/ >';
            markup += '<div class="kapamilya-info">';
              markup += '<p class="kapamilya-name" title="' + res.loginIDs.username + '" >';
                markup += res.loginIDs.username;
              markup += '</p>';
              markup += '<p class="full-name">';
                markup += fName + ' ' + lName;
              markup += '</p>';
            markup += '</div>';
            markup += '<div>';
              markup += '<ul>';
                markup += '<li>';
                  markup += '<a href="' + x.ssoPageURL + 'profile">My Account</a>';
                markup += '</li>';
                $.each(x.customDashBoard, function(i, val) {
                  markup += '<li>';
                    markup += '<a href="'+val.url+'">'+val.title+'</a>';
                  markup += '</li>';
                });
                markup += '<li>';
                  markup += '<a href="javascript:void(0);" class="dashboard-logout">Log out</a>';
                markup += '</li>';
              markup += '</ul>';
            markup += '</div>';
          markup += '<div>';
        markup += '</div>';

        $('header.uninav-header').append(markup);
      },

      lastScroll: 0,
      scrollAnamationHeader: function(_this) {
        if (createHeaderMarkup.lastScroll >= _this.scrollTop()) {
          if ($('head-bar button').hasClass('collapsed')) {
            $('header').removeClass('header-sticky');
          } else if(_this.scrollTop() <= 0) {
            $('header').removeClass('header-sticky');
            return false;
          }

          if (_this.width() <= 768) {
            $('header .header-fixed').removeClass("header-fixed-unpinned");
          }
        } else {
          $('header').addClass('header-sticky');
          if (parseInt(_this.width()) <= 768) {
            $('header .header-fixed').addClass("header-fixed-unpinned");
          }
        }
      
        createHeaderMarkup.lastScroll = _this.scrollTop();
      },

      addButtonCollasible: function() {
        var markup = '';
        markup += '<button type="button" class="navbar-toggle collapsed header-nav-toggle" data-toggle="collapse" data-target="#header-collapse" aria-expanded="false">';
          markup += '<span class="sr-only">Toggle navigation</span>';
          markup += '<span class="icon-bar"></span>';
          markup += '<span class="icon-bar"></span>';
          markup += '<span class="icon-bar"></span>';
        markup += '</button>';

        return markup;
      },

      createSearchPopup: function() {
        var markup = '';
        markup += '<div id="mobile-search">';
          markup += '<div class="uninav-search">';
            markup += '<form class="search-form" autocomplete="off">';
              markup += '<input class="search-field" type="search" name="search" placeholder="" mobile-input="search"/>';
              markup += '<span>Search</span>';
              markup += '<div class="uninav-close" data-icon="M"></div>';
              markup += '<input type="submit" value="" class="uninav-icon-new uninav-icon-search search-button" />';
            markup += '</form>';
          markup += '</div>';
        markup += '</div>';
        
        return markup;
      },

      createSplash: function() {
        var markup = "";
        markup += '<section class="lightbox">';
          markup += '<div class="content">';
            markup += '<div class="header">';
              markup += '<p>';
                markup += '<a href="#">';
                  markup += '<span><img src="//assets.abs-cbn.com/universalnav/img/logo-abs-cbn.png" alt="ABS-CBN Corporation" class="kapamilya-accounts-bar"></span>';
                  markup += '<span class="' + createHeaderMarkup.returnSiteName().toLowerCase() + '">' + createHeaderMarkup.returnSiteName() + '</span>';
                markup += '</a>';
              markup += '</p>';
            markup += '</div>';
            markup += '<div class="body">';
              markup += '<h1>Introducing your one access to everything Kapamilya!</h1>';
              markup += '<p class="sub">Now, you can use one login to access your favorite Kapamilya websites.</p>';
              markup += '<div><img src="//assets.abs-cbn.com/universalnav/img/kapamilya-account.png" alt="Kapamilya Account"></div>';
              markup += '<ul class="hub-container">';
                // markup += '<li><span class="hub hub-iwanttv"></span></li>';
                markup += '<li><span class="hub hub-onemusic"></span></li>';
                markup += '<li><span class="hub starcinema"></span></li>';
                markup += '<li><span class="hub hub-mmk"></span></li>';
                markup += '<br>';
                markup += '<li><span class="hub hub-news"></span></li>';
                markup += '<li><span class="hub hub-entertainment"></span></li>';
                markup += '<li><span class="hub hub-sports"></span></li>';
                  // '<li><a href="#"><span class="hub hub-lifestyle"></span></a></li>' +
                  // '<li><a href="#"><span class="hub hub-sky"></span></a></li>' +
                  // '<li><a href="#"><span class="hub hub-sky-on-demand"></span></a></li>' +
                  // '<li><a href="#"><span class="hub hub-tfc"></span></a></li>' +
              markup += '</ul>';
              markup += '<p class="text-blue">We will now bring you to kapamilyaaccounts.abs-cbn.com</p>';
              markup += '<p class="btn btn-proceed"><a href="javascript:void(0);">Proceed</a></p>';
              markup += '<p class="btn btn-cancel"><a href="javascript:void(0);" class="btn-cancel">Cancel</a></p>';
            markup += '</div>';
          markup += '</div>';
        markup += '</section>';

        $('body').append(markup);
      },

      loginClick: function(webSiteSource) {
        switch(webSiteSource) {
          case 'news':
          case 'mmk':
          case 'starcinema':
          case 'sports':
          case 'lifestyle':
          case 'entertainment':
            $('.lightbox').show(); break;
          default : createHeaderMarkup.goToKapamilyaLogin(); break;
        }
      },

      goToKapamilyaLogin: function() {
        var referrerDomain = x.dataDomain;
        if (referrerDomain === "") {
          referrerDomain = x.domainKey;
        }

        window.location.href = x.ssoPageURL+'login?referrerDomain='+referrerDomain;
      },

      returnSiteName: function() {
        return x.domainKey !== "" ? x.domainKey.toUpperCase() : x.dataDomain.toUpperCase();
      }
    };

    // Page Scroll Animation Header
    $(window).scroll(function() { createHeaderMarkup.scrollAnamationHeader($(this)); });

    // Resizing window positioning of uninav or sso sub menu
    $(window).resize(function() {
      if ($(".login-icon-uninav").html()) {
        var leftPosition = $('.login-icon-uninav').offset().left + 15;
        
        $('.triangle-top, .triangle-bottom').css('left', leftPosition+'px');
      }
    });

    $(document).ready(function() {
      $('.bookmarked').attr("disabled", "disabled");
    });

    $('#header-collapse, .uninav-main').on( 'mousewheel DOMMouseScroll', function (e) { 
      var e0 = e.originalEvent;
      var delta = e0.wheelDelta || -e0.detail;

      this.scrollTop += ( delta < 0 ? 1 : -1 ) * 30;
      e.preventDefault();  
    });

    function initialize() {
      if (x.sso) {uninavSSO();}
      createHeaderMarkup.headerMarkup();
      createHeaderMarkup.createSplash();
    }
    initialize();
  };
}(jQuery));
