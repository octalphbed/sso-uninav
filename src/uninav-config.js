var store = {
  baseUrl: window.location.protocol + '//' + window.location.host,
  kapamilyaUrl: 'https://staging-account.abs-cbn.com/',
  domainKeys: '//assets.abs-cbn.com/universalnav/sso-domain.json', // For Staging, Dev, QA
  uninavLinks: '//assets.abs-cbn.com/universalnav/uninavLinks.html',

  defaultCallback: window.location.origin + '/callback.html',
  defaultResponseType: 'id_token token',
  defaultScope: 'openid profile offline_access'
};

// Workaround for entertainment who still uses staging uninav for live site
var isLiveSite = (window.location.host === "entertainment.abs-cbn.com");
if (isLiveSite) {
  store.kapamilyaUrl = 'https://staging-account.abs-cbn.com/';
  store.domainKeys = '//assets.abs-cbn.com/universalnav/live/sso-domain.json';
}

var uninav = {};
(function () {
  uninav.ssoLogout = function() {
    gigya.accounts.logout({
      callback: function() {
        uninav.removeLocalStorage();
        window.location.reload();
        // window.location.href = store.kapamilyaUrl + "signout";
      }
    });
  };

  uninav.removeLocalStorage = function() {
    // Removing localstorage for sso-behvioral purpose
    localStorage.removeItem('sso_uid');
    localStorage.removeItem('sso_lastLoginTimestamp');
    localStorage.removeItem('sso_behavioral');

    // Removing localstorage for privacy policy
    localStorage.removeItem('isAcceptedPrivacyPolicy');
    localStorage.removeItem('isAPIReachedLimitPrivacyPolicy');
    localStorage.removeItem('isExistPrivacyPolicy');
    localStorage.removeItem('datePrivacyPolicy');
    localStorage.removeItem('uninavUserID');
  };

  uninav.ssoLogin = function (param) {
    var ssoConfig = window.ssoConfig;

    if (param) {
      if (param.redirectURI) {
        ssoConfig.redirectURI = param.redirectURI;
      }

      if (param.display) {
        ssoConfig.display = param.display;
      }

      if (param.scope) {
        ssoConfig.scope = param.scope;
      }

      if (param.responseType) {
        ssoConfig.responseType = param.responseType;
      }
      
      if (param.ssoURL) {
        ssoConfig.ssoPageURL = param.ssoURL;
      }

      if (param.ssoURL) {
        ssoConfig.ssoPageURL = param.ssoURL;
      }

      if (param.redirectPage) {
        document.cookie="returnUrl="+param.redirectPage+";path=/";
        localStorage.setItem('returnUrl', param.redirectPage);
      } else {
        document.cookie="returnUrl="+window.location.href+";path=/";
        localStorage.setItem('returnUrl', window.location.href);
      }
    } else {
      document.cookie="returnUrl="+window.location.href+";path=/";
      localStorage.setItem('returnUrl', window.location.href);
    }

    window.location.href = uninav.getAuthorizationURL(ssoConfig);
  };

  uninav.getAuthorizationURL = function (config) {
    var authorizationUrl = config.ssoPageURL + 'connect/authorize';
    var nonce = Date.now() + "" + Math.random();

    var url =
      authorizationUrl + "?" +
      "client_id=" + encodeURI(config.clientId) + "&" +
      "redirect_uri=" + encodeURI(config.redirectURI) + "&" +
      "response_type=" + encodeURI(config.responseType) + "&" +
      "scope=" + encodeURI(config.scope) + "&" +
      "nonce=" + encodeURI(nonce);

    if (config.display) {
      url += "&display=" + encodeURI(config.display);
    }

    return url;
  };

  uninav.accountInfo = function() {
    return window.gigyaAccountInfo;
  };

}(jQuery));
