(function($) {
  'use strict';

  $.fn.uninav = function(options) {
    var _this = this,
    defaults = {
      //Basic variables
      header: $('header'),
      window: $(window),
      body: $('body'),
      domain: document.domain,
      main: $('div.main'),
      document: $(document),

      //Header variables
      navbar: $('.header-nav-toggle'),
      navbarCollapse: $('#header-collapse'),
      target: _this,
      type: '',
      uninav: false,
      uninavTarget: '.uninav-main',
      uninavIconTarget: $('#uninav-toggle'),
      uninavAnimation: true,
      uninavAnimationSpeed: 'fast',
      uninavHighLight: '',
      mobileToggler: $('.has-child-drop'),
      navbarToggler: true,
      mobileUninav: $('#mobile-uninav'),
      headerSticky: true,
      mobileNav: true,
      dataDomain: '',
      oldsearch: false,

      //SSO variables
      sso: true,
      ssoTarget: $('.uninav-icon-login'),
      ssoMobileTarget: $('.uninav-icon-login-new'),
      ssoStatus: true,
      ssoApikey: '',
      customDashBoard: [],
      kapamilyaImage: '',
      kapamilyaImagemobile: '',
      kapamilyaImageDashboard: '',
      sourceURL: '',
      ssoPageURL: 'http://staging-kapamilyaaccounts.abs-cbn.com/'
    },
  
    x = this.config = $.extend(defaults, options);
    
    function createHeader() {
      function createUniNav() {
        var template = {
          main:
            '<div class="uninav-main">' +
              '<div class="uninav-container">' +
                '<h2 class="uninav-main-heading">' +
                  'Welcome to your one-stop hub for everything Kapamilya!<br>Where would you like to go next?' +
                '</h2>' +
              '</div>' +
              '<div class="uninav-main-divider"></div>' +
              '<div class="uninav-container">' +
                '<div class="uninav-main-links clearfix">' +
                  '<div class="uninav-main-links-bp-left">' +
                    '<ul class="uninav-main-links-group">' +
                      '<li><a target="_blank" href="http://entertainment.abs-cbn.com/">ABS-CBN Entertainment</a></li>' +
                      '<li><a target="_blank" href="http://www.abs-cbnnews.com/">ABS-CBN News</a></li>' +
                      '<li><a target="_blank" href="http://sports.abs-cbn.com/">ABS-CBN Sports</a></li>' +
                      '<li><a target="_blank" href="http://lifestyle.abs-cbn.com/">ABS-CBN Lifestyle</a></li>' +
                      '<li><a target="_blank" href="http://store.abs-cbn.com/">ABS-CBN Store</a></li>' +
                      '<li><a target="_blank" href="http://starcinema.abs-cbn.com/">Star Cinema</a></li>' +
                      '<li><a target="_blank" href="http://starmusic.abs-cbn.com/">Star Music</a></li>' +
                      '<li><a target="_blank" href="http://www.onemusic.ph/">One Music</a></li>' +
                      '<li><a target="_blank" href="http://chickenporkadobo.abs-cbn.com/">Chicken Pork Adobo</a></li>' +
                    '</ul>' +
                    '<ul class="uninav-main-links-group">' +
                      '<li><a target="_blank" href="http://www.mor1019.com/">MOR 101.9</a></li>' +
                      '<li><a target="_blank" href="http://myxph.com/">MYX PH</a></li>' +
                      '<li><a target="_blank" href="http://push.abs-cbn.com/">Push</a></li>' +
                      '<li><a target="_blank" href="http://www.choosephilippines.com/">Choose Philippines</a></li>' +
                      '<li><a target="_blank" href="http://bmpm.abs-cbnnews.com/">BMPM</a></li>' +
                      '<li><a target="_blank" href="http://dzmm.abs-cbnnews.com/">DZMM</a></li>' +
                    '</ul>' +
                  '</div>' +
                  '<div class="uninav-main-links-bp-right">' +
                    '<ul class="uninav-main-links-group">' +
                      '<li>' +
                        '<ul>' +
                          '<li><a target="_blank" href="https://tfcmyremit.com/">TFC MyRemit</a></li>' +
                          '<li><a target="_blank" href="http://www.tfc.tv/">TFC.TV</a></li>' +
                        '</ul>' +
                      '</li>' +
                      '<li>' +
                        '<ul>' +
                          '<li><a target="_blank" href="http://abscbnmobile.com/">ABS-CBNmobile</a></li>' +
                          '<li><a target="_blank" href="http://www.iwantv.com.ph/">iWant TV</a></li>' +
                          '<li><a target="_blank" href="http://tvplus.abs-cbn.com/">ABS-CBN TVplus</a></li>' +
                        '</ul>' +
                      '</li>' +
                      '<li>' +
                        '<ul>' +
                          '<li><a target="_blank" href="http://www.mysky.com.ph/">SKY</a></li>' +
                          '<li><a target="_blank" href="http://www.mysky.com.ph/skyondemand">SKY On-Demand</a></li>' +
                        '</ul>' +
                      '</li>' +
                    '</ul>' +
                    '<ul class="uninav-main-links-group">' +
                      '<li><a target="_blank" href="http://corporate.abs-cbn.com/">Corporate</a></li>' +
                      '<li><a target="_blank" href="http://corporate.abs-cbn.com/careers/">Careers</a></li>' +
                      '<li><a target="_blank" href="http://www.abs-cbnfoundation.com/">ABS-CBN Foundation</a></li>' +
                      '<li><a target="_blank" href="http://internationalsales.abs-cbn.com/">International Sales</a></li>' +
                      '<li><a target="_blank" href="http://marketportal.abs-cbn.com/">Market Portal</a></li>' +
                      '<li><a target="_blank" href="http://starmagicphils.abs-cbn.com/">Star Magic</a></li>' +
                    '</ul>' +
                  '</div>' +
                '</div>' +
              '</div>' +
            '</div>'
        };

        if (x.uninav === true) {
          x.header.append(template.main);
        } else {
          x.uninavIconTarget.hide();
        }
      }

      function Toggler() {
        // DESKTOP NAVBAR TOGGLER
        // No animation on mobile

        /* TOGGLER FUNCTIONS */
        var toggler = {
          areaExpandedTrue: function() {
            $('.header-nav-toggle').attr('aria-expanded', 'false');
            x.navbarCollapse.removeClass('in');
            x.navbar.addClass('collapsed');
            x.navbarCollapse.attr('aria-expanded', 'false');
          },

          areaExpandedFalse: function() {
            $('.header-nav-toggle').attr('aria-expanded', 'true');
            x.navbarCollapse.attr('aria-expanded', 'true');
            x.navbarCollapse.removeClass('collapse').addClass('collapsing');
            x.navbarCollapse.removeClass('collapsing').addClass('collapse in');
            x.navbar.removeClass('collapsed');
          },

          uninavAnimationTrue: function() {
            x.uninavIconTarget.on('click', function(e) {
              e.preventDefault();
              $(this).toggleClass('active');
              $(this).find('.uninav-icon-uninav').toggleClass('active');
              // $(x.uninavTarget).removeAttr('style');
              $(x.uninavTarget).toggleClass('active');
              $(x.uninavTarget).stop().slideToggle(x.uninavAnimationSpeed, function() {
                // callback function for slideToggle
                // do nothiing
              });
            });
          },

          uninavAnimationFalse: function() {
            x.uninavIconTarget.on('click', function() {
              $(this).toggleClass('active');
              $(x.uninavTarget).stop().toggle();
            });
          },

          oldSearchFalse: function() {
            $('.uninav-icon-search').parent().on('click', function(e) {
              e.preventDefault();
              $(this).toggleClass('active');
              
              if ($(this).is('.active')) {
                x.header.append(SearchPopUp());
                $('#mobile-search').fadeIn("fast");
                $('body, html').css({ "overflow": "hidden" });
              } else {
                $('#mobile-search').fadeOut().remove();
                $('body, html').css({ "overflow": "" });
              }
            });
          },

          oldSearchTrue: function() {
            x.window.bind('load resize', function() {
              if (x.window.width() < 945) {
                $('.header-nav .search-container').show();
              } else {
                $('.header-nav .search-container').hide();
              }
            });

            $('.search-container-lg .search-container').show();
            $('.uninav-icon-new.uninav-icon-search').parents('li').hide();
          }
        };

        x.navbar.on('click', function(e) {
          e.preventDefault();
          var y = x.navbar.attr('aria-expanded');

          if (y === "false") {
            toggler.areaExpandedFalse();
          } else {
            toggler.areaExpandedTrue();
          }
        });

        //DESKTOP UNINAV TOGGLER
        //Available animation settings "uninavAnimation" : boolean
        if (x.uninavAnimation === true) {
          toggler.uninavAnimationTrue();
        } else {
          toggler.uninavAnimationFalse();
        }

        // SEARCH TOGGLER
        if (x.oldsearch === false) {
          toggler.oldSearchFalse();
        } else {
          toggler.oldSearchTrue();
        }
        
        // SSO TOGGLER
        $(document).on('click', '.sso-active', function(e) {
          e.preventDefault();
          $(this).children().toggleClass('active');
        });

        $(document).on('click touchstart', '.uninav-icon-dashboard', function() {
          if ($(this).is('.active')) {
            $('.mobile-dashboard').show();
          } else {
            $('.mobile-dashboard').hide();
          }
        });

        $(document).on('click', '.sso-not-active, .sso-not-active-mobile', function() {
          switch(getURL.getWebsiteName().toLowerCase().split(' ').join('')) {
            case 'mmk': window.location=x.ssoPageURL; break;
            case 'entertainment': window.location=x.ssoPageURL; break;
            case 'news': window.location=x.ssoPageURL; break;
            default: $('.lightbox').fadeToggle(); break;
          }
        });
      }

      function mobileAccordion() {
        x.mobileToggler.each(function() {
          $(this).on('click', function() {
            if (!$(this).is('.active')) {
              x.mobileToggler.stop().removeClass('active');
              x.mobileToggler.stop().siblings('ul').slideUp();
              $(this).stop().addClass('active');
              $(this).stop().siblings('ul').slideDown();
            } else {
              $(this).stop().removeClass('active');
              $(this).stop().siblings('ul').slideUp();
            }
          });
        });
      }

      function headerNavResize() {
        x.window.bind('resize load', function() {
          $(".sso-uninav-login").show();
          var headerList = $('.header-nav > ul'),
          windowWidth = x.window.width();
          $.each(headerList, function() {
            var headerUL = $(this),
            navULpad = headerList.innerWidth() - headerList.width();

            $('> li:not(.not-priority)', headerUL).each(function() {
              navULpad += $(this).outerWidth();
            });
            $('.not-priority', headerUL).each(function() {
              var _this = $(this),
              notPrioTotal = navULpad + _this.width();
              
              if (notPrioTotal >= windowWidth) {
                _this.hide();
              } else {
                _this.show();
                navULpad = notPrioTotal;
              }
            });
            
            if (navULpad < windowWidth) { return; }

            var remainingLIs = $('> li:not(.not-priority)', headerUL).show().filter(':visible');
            $(remainingLIs.get().reverse()).each(function() {
              var _this = $(this);
              _this.hide();
              navULpad -= _this.width();
              if (navULpad < windowWidth) { return false; }
            });
          });
        });
      }

      function pageScroll() {
        x.window.bind('scroll load', function() {
          hideDashBoard();
          $('.mobile-dashboard').hide();
          $('.uninav-icon-dashboard').removeClass('active');
        });
        
        var lastScrollTop = 0;
        var st = window.pageYOffset || document.documentElement.scrollTop;
        
        if (st > lastScrollTop) {
          _this.addClass('header-sticky');
          _this.children('.header-fixed').removeClass('header-fixed-not-top headroom--not-bottom').addClass('header-fixed-init header-fixed-not-top headroom--bottom');
        }

        window.addEventListener("scroll", function() {
          var st = window.pageYOffset || document.documentElement.scrollTop;
          // for bouncing mobile reaction
          // just do nothing if scroll reach the top
          if (st <= 0) { return; }
          
           if (st > lastScrollTop) {
            // downscroll code
            $('#mobile-uninav').removeClass('pinned').addClass('unpinned');
            _this.children('.header-fixed').removeClass('header-fixed-pinned').addClass('header-fixed-unpinned');

            _this.addClass('header-sticky');
            _this.children('.header-fixed').removeClass('header-fixed-not-top headroom--not-bottom').addClass('header-fixed-init header-fixed-not-top headroom--bottom');

            // Show navigation if scroll reach bottom of the page
            if (st === parseInt($(document).height() - $(window).height())) {
              $('#mobile-uninav').removeClass('unpinned').addClass('pinned');
              _this.children('.header-fixed').removeClass('header-fixed-unpinned').addClass('header-fixed-pinned');
            }
          } else {
            // upscroll code
            _this.removeClass('header-sticky');

            _this.children('.header-fixed').removeClass('header-fixed-unpinned').addClass('header-fixed-pinned');
            $('#mobile-uninav').removeClass('unpinned').addClass('pinned');
          }

          lastScrollTop = st;
        }, false);
      }

      function toggleMobile() {
        $('.link-uninav').each(function() {
          $(this).on('click', function() {
            $(this).toggleClass("active", " ");
            $('.link-uninav').not($(this)).children().removeClass('active');
            $(this).children().toggleClass('active');
            $('.mobile-dashboard').hide();
            
            if ($(this).children().is('.uninav-icon-new') && $(this).children().is('.active')) {
              $(x.uninavTarget).stop().slideDown(x.uninavAnimationSpeed);
              disableScroll();
              $('body, html').css({ "overflow": "hidden" });
            } else {
              // do nothing
              $(x.uninavTarget).stop().slideUp(x.uninavAnimationSpeed);
              enableScroll();
            }
            
            if ($(this).children().is('.uninav-icon-search-new') && $(this).children().is('.active')) {
              x.header.append(SearchPopUp());
              $('#mobile-search').fadeIn("fast");
              disableScroll();
              $('body, html').css({ "overflow": "hidden" });
            } else {
              $('#mobile-search').fadeOut().remove();
              enableScroll();
            }
            
            if ($(this).children().is('.uninav-icon-new') && !$(this).children().is('.active')) {
              $('body, html').css({ "overflow": "auto" });
            }
            
            if ($(this).children().is('.uninav-icon-search-new') && !$(this).children().is('.active')) {
              $('body, html').css({ "overflow": "auto" });
            }
            
            if ($(this).children().is('.uninav-icon-login-new')) {
              $(this).children().removeClass('active');
            }
          });
        });
      }

      function createSignOut() {
        if (x.window.width() <= 940 && window.location.pathname === 'profile') {
          if (x.customDashBoard.length !== 0) {
            for (var i = x.customDashBoard.length - 1; i >= 0; i--) {
              $('.sign-out').append(
                '<a target="_blank" href="' + x.customDashBoard[i].url + '">' + 
                  x.customDashBoard[i].title +
                '</a>'
              );
            }
          }
          $('.sign-out').append('<a href="' + x.ssoPageURL + 'logout" > Sign Out </a>');

        } else {
          $('.sign-out').children().remove();
        }
      }

      function SearchPopUp() {
        var template = {
          search: 
            '<div id="mobile-search">' +
              '<div class="uninav-search">' +
                '<form class="search-form" autocomplete="off">' +
                  '<input class="search-field" type="search" name="search" placeholder="" mobile-input="search"/>' +
                  '<span>Search</span>' +
                  '<div class="uninav-close" data-icon="M"></div>' +
                  '<input type="submit" value="" class="uninav-icon-new uninav-icon-search search-button" />' +
                '</form>' +
              '</div>' +
            '</div>'
        };
        return template.search;
      }

      function searchFocus() {
        $(document).on('focus', 'input[mobile-input="search"]', function() {
          $(this).siblings('span').stop().addClass('active');
        });
        
        $(document).on('focusout', 'input[mobile-input="search"]', function() {
          if ($('input[mobile-input="search"]').val() === "") {
            $(this).siblings('span').stop().removeClass('active');
          } else {
            $(this).siblings('span').stop().addClass('active');
            $('input[mobile-input="search"]').val();
          }
        });
        
        $(document).on('click', '.search-form span', function() {
          $('input[mobile-input="search"]').focus();
        });
      }

      function searchClose() {
        $(document).on('touchstart click', '.uninav-close', function(e) {
          e.preventDefault();
          $('#mobile-search').remove();
          $('.uninav-icon-search-new').removeClass('active');
          $('body, html').css({ "overflow": "" });
          $('.uninav-icon-search').parent().removeClass('active');
          enableScroll();
        });
      }
       
      function checkMobile() {
        x.window.bind('resize load', function(e) {
          hideDashBoard();
          $(x.uninavTarget).hide();
          $(x.uninavTarget).removeClass('active');
          x.uninavIconTarget.removeClass('active');
          $('.uninav-icon-uninav').removeClass('active');
          
          if (e.type === 'load') {
            // createMobileIcons();
            filterMobileIcons();
          }
          
          if (x.window.width() <= 945) {
            // some code..
            // $('#uninav-toggle').hide();
            if (x.mobileNav === true) {
              x.mobileUninav.show();
              x.navbar.css({ "position": "absolute" });
              $(x.uninavTarget).addClass('mobile');
            } else {
              x.mobileUninav.hide();
              $('#uninav-toggle').show();
              $(x.uninavTarget).removeClass('mobile');
            }
          } else {
            x.mobileUninav.hide();
            $('#uninav-toggle').show();
            $(x.uninavTarget).removeClass('mobile');
          }
        });
      }

      function filterMobileIcons() {
        if (window.location.pathname === "/profile") {
          x.sourceURL = $('#screenset-container').attr('data-redirect-url');
          $('.site-logo').show();
          $('.site-logo').find('a').attr('href', x.sourceURL);

          var iconClass = 'default';//getURL.getIconClassByReferrrer();
          $('.site-logo #mobile-icon').attr('class', 'icon '+iconClass);
          $('.uninav-icon-login-new').parent().parent().hide();
          $('.uninav-icon-search-new').parent().parent().hide();
        } else { $('.site-logo').hide(); }

        if (window.location.pathname === "/profile") {
          $('.uninav-icon-login-new').addClass('active');
        } else {
          $('.uninav-icon-login-new').removeClass('active');
        }
        
        if (x.dataDomain === 'news') {
          $('.uninav-icon-search-new').parent().parent().hide();
        }
      }

      function markActive() {
        switch (x.domain) {
        /* Put overrides or special links here */
        case 'oneforpacman.abs-cbn.com': // TODO: override directly
          x.domain = 'sports.abs-cbn.com';
          break;
        case 'entertainment.abs-cbn.com':
        case 'entertainment2.abs-cbn.com':
          x.domain = 'http://entertainment.abs-cbn.com/';
          break;
        default:
          break;
        }
        $('a[href="' + x.domain + '"]').removeAttr('href').addClass('active');
      }

      function getSourceURL() {
        $(document).on('click', '.uninav-mobile-parent', function() {
          localStorage.setItem('sourceURL', window.location.href);
        });
      }

      function disableScroll() {
        document.ontouchmove = function(event) {
          event.preventDefault();
        };
      }

      function enableScroll() {
        document.ontouchmove = function(event) {
          return true;
        };
      }

      function hideDashBoard() {
        $('.gigya-expanded-profile').remove();
        $('.sso-uninav-login').removeClass('active');
      }

      function temporaryPopUp() {
        var template =
          '<section class="lightbox">' +
            '<div class="content">' + 
              '<div class="header">' +
                '<p>' +
                  '<a href="#">' +
                    '<span><img src="//assets.abs-cbn.com/universalnav/img/logo-abs-cbn.png" alt="ABS-CBN Corporation" class="kapamilya-accounts-bar"></span>' +
                    '<span class=' + getURL.getWebsiteName().toLowerCase().split(' ').join('') + '>' + getURL.getWebsiteName() + '</span>' +
                  '</a>' +
                '</p>' +
              '</div>' +
              '<div class="body">' +
                '<h1>Introducing your one access to everything Kapamilya!</h1>' +
                '<p class="sub">Now, you can use one login to access your favorite Kapamilya websites.</p>' +
                '<div><img src="//assets.abs-cbn.com/universalnav/img/kapamilya-account.png" alt="Kapamilya Account"></div>' +
                '<ul class="hub-container">' +
                  '<li><span class="hub hub-news"></span></li>' +
                  '<li><span class="hub hub-entertainment"></span></li>' +
                  // '<li><a href="#"><span class="hub hub-sports"></span></a></li>' +
                  // '<li><a href="#"><span class="hub hub-lifestyle"></span></a></li>' +
                  // '<li><a href="#"><span class="hub hub-iwanttv"></span></a></li>' +
                  // '<li><a href="#"><span class="hub hub-sky"></span></a></li>' +
                  // '<li><a href="#"><span class="hub hub-sky-on-demand"></span></a></li>' +
                  // '<li><a href="#"><span class="hub hub-tfc"></span></a></li>' +
                  '<li><span class="hub hub-onemusic"></span></li>' +
                '</ul>' +
                '<p class="text-blue">We will now bring you to abs-cbn.com</p>' +
                '<p class="btn btn-proceed"><a href="' + x.ssoPageURL + '">Proceed</a></p>' +
                '<p class="btn btn-cancel"><a href="#" class="btn-cancel">Cancel</a></p>' +
              '</div>'+
            '</div>' +
          '</section>';
        x.body.append(template);
      }

      function closeTemporaryPopUp() {
        $(document).on('click', '.btn-cancel', function() {
          $('.lightbox').hide();
        });
      }
      
      function logoutDashBoard() {
        $(document).on('click', '.dashboard-logout', function() {
          gigya.accounts.logout(/*{
            callback: function() {
              setPageLocation(x.ssoPageURL + '/logout');
            }
          }*/);
        });
      }

      if (x.type === "default") {
        x.navbar.attr('aria-expanded', 'false');
        x.navbarCollapse.attr('aria-expanded', 'false');
        createUniNav();
        mobileAccordion();
        headerNavResize();
        pageScroll();
        Toggler();
        checkMobile();
        searchFocus();
        toggleMobile();
        searchClose();
        markActive();
        getSourceURL();
        createSignOut();
        temporaryPopUp();
        closeTemporaryPopUp();
        logoutDashBoard();
      } else {
        // console.log('custom');
      }
    }

    var getURL = {
      getWebsiteName: function() {
        var webSiteName = "";
        if (getURL.getURLSites("news") >= 0) {
          webSiteName = "NEWS";
        } else if (getURL.getURLSites("entertainment") >= 0) {
          webSiteName = "ENTERTAINMENT";
        } else if (getURL.getURLSites("mmk") >= 0) {
          webSiteName = "MMK";
        } else if (getURL.getURLSites("bmpm") >= 0) {
          webSiteName = "BMPM";
        } else if (getURL.getURLSites("onemusic") >= 0) {
          webSiteName = "ONE MUSIC";
        } else if (getURL.getURLSites("starcinema") >= 0) {
          webSiteName = "STARCINEMA";
        } else if (getURL.getURLSites("sinehub") >= 0) {
          webSiteName = "SINEHUB";
        } else if (getURL.getURLSites("iwantv") >= 0) {
          webSiteName = "I WANT TV";
        } else if (getURL.getURLSites("sports") >= 0) {
          webSiteName = "SPORTS";
        } else if (getURL.getURLSites("lifestyle") >= 0) {
          webSiteName = "LIFESTYLE";
        } else if (getURL.getURLSites("sky") >= 0) {
          webSiteName = "SKY.";
        } else if (getURL.getURLSites("skyondemand") >= 0) {
          webSiteName = "SKY ON DEMAND";
        } else {
          webSiteName = "KAPAMILYA";
        }
        return webSiteName;  
      },

      getIconClassByReferrrer: function() {
        referrer = getURL.getCookie("Referrer").split('?')[0];

        if (getURL.getUrlParameter('referrer')) {
          referrer = getURL.getUrlParameter("referrer");
        }

        if (referrer.indexOf("news") >= 0) {
          iconClass = "news";
        } else if (referrer.indexOf("entertainment") >= 0) {
          iconClass = "entertainment";
        } else if (referrer.indexOf("mmk") >= 0) {
          iconClass = "mmk";
        } else if (referrer.indexOf("bmpm") >= 0) {
          iconClass = "default";
        } else if (referrer.indexOf("onemusic") >= 0) {
          iconClass = "onemusic";
        } else if (referrer.indexOf("starcinema") >= 0) {
          iconClass = "default";
        } else if (referrer.indexOf("sinehub") >= 0) {
          iconClass = "default";
        } else if (referrer.indexOf("iwantv") >= 0) {
          iconClass = "iwantv";
        } else {
          iconClass = "default";
        }
        return iconClass;  
      },

      getURLSites: function(sName) {
        // return "www.test.news.abs-cbn.com".indexOf(sName); /*FOR TESTING ON LOCAL*/
        if (getURL.getUrlParameter('referrer')) {
          return getURL.getUrlParameter("referrer").indexOf(sName);
        }

        return window.location.href.split('?')[0].indexOf(sName);
      },

      getApiReferrerKey: function(useForApi, data) {
        // document.cookie = "Referrer=http://test.entertainment.abs-cbn.com/tv/home";
        var referrer = getURL.getCookie("Referrer");
    
        if (getURL.getUrlParameter('referrer')) {
          referrer = getURL.getUrlParameter("referrer");
        }

        console.log(referrer);

        var apiKeys = "";

        if (referrer !== "") {
          referrer = referrer.split("?")[0];

          if (referrer.indexOf("news") >= 0) {
            apiKeys = useForApi ? data.news : "NEWS";
          } else if (referrer.indexOf("kapamilyaaccounts") >= 0) {
            apiKeys = useForApi ? data.entertainment : "ACCOUNTS";
          } else if (referrer.indexOf("bmpm") >= 0) {
            apiKeys = useForApi ? data.bmpm : "BMPM";
          } else if (referrer.indexOf("entertainment") >= 0) {
            apiKeys = useForApi ? data.entertainment : "ENTERTAINMENT";
          } else if (referrer.indexOf("iwantv") >= 0) {
            apiKeys = useForApi ? data.iwantv : "I WANT TV";
          } else if (referrer.indexOf("mmk") >= 0) {
            apiKeys = useForApi ? data.mmk : "MMK";
          } else if (referrer.indexOf("sinehub") >= 0) {
            apiKeys = useForApi ? data.sinehub : "SINEHUB";
          } else if (referrer.indexOf("starcinema") >= 0) {
            apiKeys = useForApi ? data.starcinema : "STAR CINEMA";
          } else if (referrer.indexOf("onemusic") >= 0) {
            apiKeys = useForApi ? data.onemusic : "ONE MUSIC";
          }


        } else {
          apiKeys = useForApi ? data.entertainment : "KAPAMILYA"; // Just For Localhost
        }

        return apiKeys;
      },

      getCookie: function(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i = 0; i < ca.length; i++) {
          var c = ca[i];
          while (c.charAt(0) === ' ') {
            c = c.substring(1);
          }
          if (c.indexOf(name) === 0) {
            return c.substring(name.length,c.length);
          }
        }
        return "";
      },
      getUrlParameter: function(sParam) {
          var sPageURL = decodeURIComponent(window.location.search.substring(1)),
              sURLVariables = sPageURL.split('&'),
              sParameterName,
              i;

          for (i = 0; i < sURLVariables.length; i++) {
              sParameterName = sURLVariables[i].split('=');

              if (sParameterName[0] === sParam) {
                  return sParameterName[1] === undefined ? true : sParameterName[1];
              }
          }
      }
    };

    function setPageLocation(destination) {
      window.location = destination;
    }

    function uninavSSO() {
      $.getJSON('http://assets.abs-cbn.com/universalnav/sso-domain.json', function(data) {
        // body...
      }).done(function(data) {
        switch (window.location.hostname) {
          case '1eb5e7dfe0b6437ca7449539cd5412c6.cloudapp.net':
          case 'devnews.abs-cbn.com':
          case 'testnews.abs-cbn.com':
          case 'stagingnews.abs-cbn.com':
          case 'staging-dev.abs-cbn.com':
            x.ssoApikey = data.news;
            break;
          case 'dev.mmk.abs-cbn.com':
          case 'conceptmachine.net':
          case 'www.conceptmachine.net':
            x.ssoApikey = data.mmk;
            break;
          case 'dev.entertainment.abs-cbn.com':
          case 'test.entertainment.abs-cbn.com':
          case 'dev.entertainment.abs-cbn.com':
          case 'staging.entertainment.abs-cbn.com':
            x.ssoApikey = data.entertainment;
            break;
          case 'dev.bmpm.abs-cbn.com':
          case 'bmpm-staging.kudosweb.com':
            x.ssoApikey = data.bmpm;
            break;
          case 'localhost':
          case 'staging-kapamilyaaccounts.abs-cbn.com':

            if (window.location.pathname === "/" || window.location.pathname === "/login" || window.location.pathname === "/register" || window.location.href === x.ssoPageURL) {
              // x.ssoApikey = data.kapamilyaaccounts;
              x.ssoApikey = getURL.getApiReferrerKey(true, data);
            } else {
              x.ssoApikey = data.otp;
            }
            break;

        }

        $('head').append(
          '<script type="text/javascript">window.__gigyaConf = {"enableSSOToken": true}</script>'+
          '<script type="text/javascript" src="http://cdn.gigya.com/JS/gigya.js?apikey=' + x.ssoApikey + '"></script>'
        );
        ssoChecker();
      });

      function logoutFromComment() {
        gigya.accounts.addEventHandlers({
          onLogout: function() {
            setPageLocation(x.ssoPageURL + '/logout');
          }
        });
      }

      function ssoChecker() {
        $("script[src*='http://cdn.gigya.com/JS/gigya.js?apikey=" + x.ssoApikey + "']").load(function() {
          ssoApiCall();
        });
      }
      
      function ssoApiCall() {
        gigya.accounts.getAccountInfo({
          include: "loginIDs,profile,data",
          callback: function(res) {
            if (res.status === "FAIL") {
              x.ssoTarget.parent().parent().addClass('sso-not-active');
              /*jshint scripturl:true*/
              x.ssoTarget.parent().attr('href', 'javascript:void(0);');
              $('.uninav-icon-login-new ').parent().parent().addClass('sso-not-active-mobile');
              /*jshint scripturl:true*/
              $('.uninav-icon-login-new ').parent().attr('href', 'javascript:void(0);');
              templates.mobileDashBoard("out");
              mobileLogInDashBoard();
              $(".sso-uninav-login").show();
            } else {
              x.ssoTarget.parent().parent().addClass('sso-active');
              /*jshint scripturl:true*/
              x.ssoTarget.parent().attr('href', 'javascript:void(0);');

              ssoDashBoard(res);
              ssoThumbnail(res);
              characterElipsis(res);
              if (x.customDashBoard.length === 0) {
                templates.mobileDashBoard("in");
                mobileLogOutDashBoard();
              } else {
                mobileCustomDashBoard();
              }
              $(".sso-uninav-login").show();
              logoutFromComment();
            }

            readingList.setAccountInformation(res.status);
          }
        });
        x.body.append(templates.markup);
      }

      var readingList = {
        data: [],
        dataTitle: [],
        page: 0,
        pageScroll: function () {
          readingList.displayList(false);
          readingList.displayListMobile(false);
          $(window).scroll(function() {
            if ($(window).scrollTop() === $(document).height() - $(window).height() && $("#readingList-tab").hasClass("active")) {
              if (readingList.data.length > $(".reading-list-container").data("index")) {
                readingList.nextWebPage();
              }
            }

            // FOR MOBILE SCROLLING
            if ($(window).scrollTop() === $(document).height() - $(window).height() && $(".reading-list-container-mobile").is(":visible")) {
              if (readingList.data.length > $(".reading-list-container-mobile").data("index")) {
                readingList.nextMobilePage();
              }
            }
          });
        },
        nextMobilePage: function() {
          readingList.displayListMobile(true);
          $('.menus-container').css('height', $('.sso-tab.active').height());
          $('#gigya-update-profile-screen').css('height', $('.sso-tab.active').height());
        },

        nextWebPage: function() {
          readingList.displayList(true);
          $('.menus-container').css('height', $('.sso-tab.active').height()+150);
          $('#gigya-update-profile-screen').css('height', $('.sso-tab.active').height()+150);
        },

        getDateNow: function() {
          var date = new Date();
          var year = date.getFullYear();
          var day = date.getDate();
          var month = date.getMonth();

          switch (month) {
            case 0: month = "January"; break;
            case 1: month = "Febuary"; break;
            case 2: month = "March"; break;
            case 3: month = "April"; break;
            case 4: month = "May"; break;
            case 5: month = "June"; break;
            case 6: month = "July"; break;
            case 7: month = "August"; break;
            case 8: month = "September"; break;
            case 9: month = "October"; break;
            case 10: month = "November"; break;
            case 11: month = "December"; break;
          }

          return month + " " + day + ", " + year;
        },

        createTempBubblePopup: function(str, el, closeTime) {
          var bubblePopup = $('<div class="bookmark-saved">'+str+'</div>').fadeIn("medium").appendTo('body');
          var topPosition = parseInt(el.height() + el.offset().top) + "px";
          var leftPosition = parseInt(el.offset().left - 5) + "px";
          bubblePopup.css({top:topPosition, left:leftPosition});
          
          if (closeTime) {
            closeTime = closeTime;
          } else {
            closeTime = 3000;
          }

          var timeout = setInterval(function() {
            bubblePopup.fadeOut("slow");
            bubblePopup.remove();
            clearInterval(timeout);
          }, closeTime);
        },

        setAccountInformation: function(status) {
          // get all reading list first.
          gigya.accounts.getAccountInfo({ callback: readingList.getAccountInfoResponse });
          $(".read-list").on('click', function() {
            var _this = $(this);
            // Note: check if user is login before do anything.
            if (status === "FAIL") {
              readingList.createTempBubblePopup("You need to login before you can save this article.", _this);
              return;
            }
            
            // set variable for unique article id
            var articleID = new Date().getTime();

            
            // Check if article already saved or bookmarked
            if (readingList.articleAlreadyAdd(readingList.dataTitle, $(".read-list").data("title"))) {
              readingList.createTempBubblePopup("Article already saved.", _this);
            } else {
              // prepare all data needed to set the accounts info
              var webSiteSource = "";
              switch(getURL.getWebsiteName()) {
                case 'NEWS' : webSiteSource = "ABS-CBN NEWS"; break;
                case 'ENTERTAINMENT' : webSiteSource = "ABS-CBN ENTERTAINMENT"; break;
                case 'SPORTS' : webSiteSource = "ABS-CBN SPORTS"; break;
                default : webSiteSource = "ABS-CBN NEWS"; break;
              }

              var dateSave = readingList.getDateNow();

              var temp = {
                articleId: articleID.toString(),
                title: $(this).data("title"),
                shortDesc: $(this).data("summary"),
                saveDate:  dateSave,
                thumbnail:  $(this).data("thumbnail"),
                urlLink: $(this).data("url"),
                siteSrc: webSiteSource,
                readed: false
              };

              readingList.data.push(temp);

              var params = { 
                data: {
                  readList: readingList.data
                },
                callback: function(response) {
                  if (response.errorCode === 0) { 
                    readingList.data = [];
                    gigya.accounts.getAccountInfo({ callback: readingList.getAccountInfoResponse });
                    readingList.createTempBubblePopup("Saved.", _this);
                  } else {
                    readingList.createTempBubblePopup("An error has occurred!<br>Error details: " + response.errorMessage + "<br>Error code: " + response.errorCode + "<br>In method: " + response.operation, 5000);
                  }
                }
              };
              gigya.accounts.setAccountInfo(params);
            }
          });

          $(document).on("click", ".clickable-link", function() {
            var dataValue = $(this).data("val").split("+@@@+");
            readingList.removeObjectArray(dataValue[0]);
            readingList.removeObjectArray(this);
            var temp = {
              articleId: dataValue[0],
              title: dataValue[1],
              shortDesc: dataValue[2],
              saveDate:  dataValue[3],
              thumbnail: dataValue[4],
              urlLink: dataValue[5],
              siteSrc: dataValue[6],
              readed: true
            };
            
            readingList.data.push(temp);

            var params = { 
              data: {
                readList: readingList.data
              },
              callback: function(response) {
                if (response.errorCode === 0) {
                  window.location = dataValue[5];
                } else {
                  alert("An error has occurred!" + '\n' +
                        "Error details: " + response.errorMessage + '\n' + 
                        "Error code: " + response.errorCode + '\n' +               
                        "In method: " + response.operation);
                }
              }
            };

            gigya.accounts.setAccountInfo(params);
          });

          // DELETING OBJECT INTO AN ARRAY (NOTE: USE FOR READING LIST change will apply if gigya has it's own function to delete object inside an array)
          $(document).on("click", ".delete-reading-list", function() {
            var dataValue = $(this).data("val").split("+@@@+");
            readingList.removeObjectArray(dataValue[0]);
            var _thisRow = $(this).parent().parent();
            var params = { 
              data: {
                readList: readingList.data
              },
              callback: function(response) {
                if (response.status === "OK" && response.errorCode === 0) {
                  _thisRow.fadeOut("medium", function() { 
                    $(_thisRow).remove();
                    $('.menus-container').height($('.menus-container').height() - 133);
                    $('#gigya-update-profile-screen').height($('#gigya-update-profile-screen').height() - 133);
                  });
                }
              }
            };

            gigya.accounts.setAccountInfo(params);
          });
        },

        articleAlreadyAdd: function(a, b) {
          return a.indexOf(b) >= 0;
        },

        getAccountInfoResponse: function(response) {
          if (response.data && response.data.readList) {
            if ( response.errorCode === 0 ) {           
              var readList = response.data.readList;

              $.each(readList, function(i, val) {
                readingList.data.push(val);
                readingList.dataTitle.push(val.title);
              });

              readingList.pageScroll();
            }
          }
        },

        removeObjectArray: function(val) {
          // REMOVING OBJECT INTO AN ARRAY BEFORE MAKE THE UPDATE
          var arr = readingList.data.filter(function(data) {
            return data.articleId !== val;
          });
          
          readingList.data = arr;
        },

        displayList: function(bol) {
          // FOR WEB VERSION
          var list = $(".reading-list-container");
          var index = list.data('index') % readingList.data.length || 0;
          list.data('index', index + 5);
          var ctr = 0;
          if (readingList.data.length >= list.data('index')) {
            ctr = 5;
          } else {
            ctr = readingList.data.length;
          }
          var markup = "";

          $.map(readingList.data.slice(index, index + ctr), function(data) {
            var arrayVal = data.articleId  + '+@@@+' + data.title + '+@@@+' + data.shortDesc + '+@@@+' + data.saveDate  + '+@@@+' + data.thumbnail + '+@@@+' + data.urlLink + '+@@@+' + data.siteSrc;
            markup += '<div class="row add-border-1">';
              markup += '<div class="col s5 add-height-overflow">';
                markup += '<a href="javascript:void(0);" data-val="' + arrayVal +'" class="clickable-link">';
                  markup += '<img border="0" src="' + data.thumbnail + '" />';
                markup += '</a>';
              markup += '</div>';
              markup += '<div class="col s6">';
                markup += '<div><h3 class="article-title">';
                  markup += '<a href="javascript:void(0);" data-val="' + arrayVal +'" class="clickable-link">' + data.title + '</a>';
                  markup += '</h3></div>';
                markup += '<div class="short-description">' + data.shortDesc + '</div>';
                markup += '<div>';
                  markup += '<p class="timestamp"><span class="date-posted">' + data.saveDate + '</span></p>';
                markup += '</div>';
              markup += '</div>';
              markup += '<div class="col s1">';
                markup += '<span class="delete-reading-list close rounded" data-val="' + arrayVal +'"></span>';
              markup += '</div>';
            markup += '</div>';
          });
          if (bol) {
            $(markup).appendTo(list).hide().fadeIn(1500);
          } else {
            $(markup).appendTo(list);
          }
        },

        displayListMobile: function(bol) {
          // FOR MOBILE VERSION
          var list = $(".reading-list-container-mobile");
          var index = list.data('index') % readingList.data.length || 0;
          list.data('index', index + 5);
          
          var ctr = 0;
          if (readingList.data.length >= list.data('index')) {
            ctr = 5;
          } else {
            ctr = readingList.data.length;
          }

          var markup = "";
          $.map(readingList.data.slice(index, index + ctr), function(data) {
            var arrayVal = data.articleId  + '+@@@+' + data.title + '+@@@+' + data.shortDesc + '+@@@+' + data.saveDate  + '+@@@+' + data.thumbnail + '+@@@+' + data.urlLink + '+@@@+' + data.siteSrc;
            markup += '<div class="row add-padding add-border-1">';
              markup += '<div class="thumbnail-image">';
                markup += '<a href="javascript:void(0);" data-val="' + arrayVal +'" class="clickable-link">';
                  markup += '<img border="0" src="' + data.thumbnail + '" />';
                markup += '</a>';
              markup += '</div>';
              markup += '<div class="delete-saved">';
                markup += '<span class="delete-reading-list close rounded" data-val="' + arrayVal +'"></span>';
              markup += '</div>';
              markup += '<div class="clearfix">';
                markup += '<p class="article-title">';
                  markup += '<a href="javascript:void(0);" data-val="' + arrayVal +'" class="clickable-link">' + data.title + '</a>';
                  markup += '</p>';
                markup += '<div class="short-description">' + data.shortDesc + '</div>';
                markup += '<div>';
                  markup += '<p class="timestamp"><span class="saved-date">Saved : ' + data.saveDate + '</span></p>';
                  markup += '<p class="timestamp"><span class="saved-from">Saved From : <span class="from-saved">ABS-CBN NEWS</span></span></p>';
                markup += '</div>';
              markup += '</div>';
            markup += '</div>';
          });

          if (bol) {
            $(markup).appendTo(list).hide().fadeIn(2000);
          } else {
            $(markup).appendTo(list);
          }
        }
      };

      var templates = {
        markup: "",
        mobileDashBoard: function(status) {
          if(status === "out") {
            templates.markup += '<div class="mobile-dashboard">';
              templates.markup += '<ul>';
                templates.markup += '<li>';
                  templates.markup += '<a href="' + x.ssoPageURL + 'login" onclick="gigya.accounts.login();">Sign in</a>';
                templates.markup += '</li>';
              templates.markup += '<ul>';
            templates.markup += '</div>';  
          } else {
            templates.markup += '<div class="mobile-dashboard">';
              templates.markup += '<ul>';
                templates.markup += '<li>';
                  templates.markup += '<a href="' + x.ssoPageURL + 'profile">View profile</a>';
                  templates.markup += '<a class="dashboard-logout" href="javascript:void(0);">Sign out</a>';
                templates.markup += '</li>';
              templates.markup += '<ul>';
            templates.markup += '</div>';
          }
        },
      };

      function mobileLogInDashBoard() {
        var template = {
          mobileDashBoard:
            '<div class="mobile-dashboard">' +
                '<div>' +
                  '<ul>' +
                    '<li>' +
                      '<a href="' + x.ssoPageURL + 'login" onclick="gigya.accounts.login();">' +
                        'Sign in' +
                      '</a>' +
                    '</li>' +
                  '<ul>' +
                '</div>' +
            '</div>'
        };

        x.body.append(template.mobileDashBoard);
      }

      function mobileLogOutDashBoard() {
        var template = {
          mobileDashBoard:
            '<div class="mobile-dashboard">' +
                '<div>' +
                  '<ul>' +
                    '<li>' +
                      '<a href="' + x.ssoPageURL + 'profile" >' +
                        'View profile' +
                      '</a>' +
                    '</li>' +
                    '<li>' +
                      '<a class="dashboard-logout" href="javascript:void(0);">' +
                        'Sign out' +
                      '</a>' +
                    '</li>' +
                  '<ul>' +
                '</div>' +
            '</div>'
        };

        x.body.append(template.mobileDashBoard);
      }

      function mobileCustomDashBoard() {
        mobileLogOutDashBoard();
        x.body.append(templates.markup);
        $('.mobile-dashboard ul ul ').remove();
        $('.mobile-dashboard ul li').remove();
        var template = "";

        $.each(x.customDashBoard, function(i, obj) {
          template += '<li>';
            template += '<a target="_blank" href="' + x.customDashBoard[i].url + '">';
              template += x.customDashBoard[i].title;
            template += '</a>';
          template += '</li>';
        });

        $('.mobile-dashboard ul').append(template);
        $('.mobile-dashboard ul').append('<li><a href="' + x.ssoPageURL + 'profile" > View Profile </a></li> <li><a class="dashboard-logout" href="' + x.ssoPageURL + 'logout" > Sign Out </a></li>');
      }

      function ssoTemplate(res) {
        x.kapamilyaImage = res.profile.thumbnailURL;
        x.kapamilyaImagemobile = res.profile.thumbnailURL;
        x.kapamilyaImageDashboard = res.profile.photoURL;
        
        if (res.profile.thumbnailURL === undefined) {
          x.kapamilyaImage = 'http://assets.abs-cbn.com/universalnav/img/header-icons.png';
          x.kapamilyaImagemobile = "http://assets.abs-cbn.com/universalnav/img/sprite-logo.png";
        }
        
        if (res.profile.photoURL === undefined) {
          x.kapamilyaImageDashboard = 'http://assets.abs-cbn.com/universalnav/img/no-image-logo.png';
        }

        var template = {
          image: x.kapamilyaImage,
          imagemobile: x.kapamilyaImagemobile,
          kapamilyaName: res.loginIDs.username,
          userName: 
            '<div class="sso-uninav-username">' +
              '<p>' +
                res.loginIDs.username +
              '</p>' +
            '</div>',
          defaultDashBoard: 
            '<div class="gigya-expanded-profile">' +
              '<div class="gigya-expanded-profile-account">' +
                '<img class="gigya-expanded-photo" src="' + x.kapamilyaImageDashboard + '"/ >' +
                '<div class="kapamilya-info">' +
                  '<p class="kapamilya-name" title="' + res.loginIDs.username + '" >' +
                    res.loginIDs.username +
                  '</p>' +
                  '<p class="full-name">' +
                    res.profile.firstName + ' ' + res.profile.lastName +
                  '</p>' +
                '</div>' +
                '<div>' +
                  '<ul>' +
                    '<li>' +
                      '<a href="' + x.ssoPageURL + 'profile"> View profile </a>' +
                    '</li>' +
                    '<li>' +
                      '<a href="javascript:void(0);" class="dashboard-logout"> Sign out </a>' +
                    '</li>' +
                  '</ul>' +
                '</div>' +
                '<div>' +
            '</div>'
        };

        return template;
      }

      function ssoThumbnail(res) {
        $('.uninav-icon-login-new').addClass('uninav-mobile-sso');
        $('.uninav-icon-login-new').parent().addClass('uninav-mobile-parent');
        $('.uninav-icon-login-new').children('p').html(res.loginIDs.username);

        //For Desktop
        if (ssoTemplate(res).image === "http://assets.abs-cbn.com/universalnav/img/header-icons.png") {
          x.ssoTarget.css({
            "background-image": "url(" + ssoTemplate(res).image + ")",
            "background-position": "-115px -2px"
          });
        } else {
          x.ssoTarget.css({
            "background-image": "url(" + ssoTemplate(res).image + ")",
            "background-position": "0 0"
          });
        }

        x.ssoTarget.addClass("active");
        x.ssoTarget.parent().append(ssoTemplate(res).userName);
        
        if (ssoTemplate(res).imagemobile === "http://assets.abs-cbn.com/universalnav/img/sprite-logo.png") {
          $('.uninav-icon-login-new').addClass('no-image');
        } else {
          $('.uninav-icon-login-new').css({
            "background-image": "url(" + ssoTemplate(res).image + ")"
          });
        }
      }

      function ssoDashBoard(res) {
        $('.sso-active a').on('click', function() {
          if (!$(this).is('.active')) {
            x.header.append(ssoTemplate(res).defaultDashBoard);
            dashBoardPosition();
            if (x.customDashBoard.length === 0) {
              // use default
            } else {
              customSsoLinks();
            }
          } else {
            $('.gigya-expanded-profile').remove();
          }
        });
      }

      function dashBoardPosition() {
        var top = $('.sso-active').position().top;
        var left = $('.sso-active').position().left;

        $('.gigya-expanded-profile').css({
          'top': $('.header-bar-inner').height(),
          'left': left - $('.gigya-expanded-profile').width() + $('.uninav-toggle').width() - 10
        });
      }

      function customSsoLinks() {
        $('.gigya-expanded-profile ul li').remove();
        var template = "";

        $.each(x.customDashBoard, function(i, obj) {
          template += '<li>';
            template += '<a target="_blank" href="' + x.customDashBoard[i].url + '">';
              template += x.customDashBoard[i].title;
            template += '</a>';
          template += '</li>';
        });

        $('.gigya-expanded-profile ul').append(template);
        $('.gigya-expanded-profile ul').append('<li><a href="' + x.ssoPageURL + 'profile" > View Profile </a></li> <li><a class="dashboard-logout" href="' + x.ssoPageURL + 'logout" > Sign Out </a></li>');
      
      }

      function characterElipsis(res) {
        if ($('.sso-uninav-username').length !== 0) {
          var nameLength = $('.sso-uninav-username').children().html().length,
          maxLength = 10,
          newname = '';
          
          if (nameLength > maxLength) {
            newname = $('.sso-uninav-username').children().html().substr(0, maxLength) + '...';
            $('.sso-uninav-username').children().html(newname);
            $('.sso-uninav-username').children().attr('title', res.loginIDs.username);
          }
        } else {
          // do nothing 
        }
      }
    }

    function initialize() {
      if (x.sso) {uninavSSO();}
      createHeader();
    }
    initialize();
  };
}(jQuery));
