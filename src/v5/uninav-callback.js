// store into variable the value of return url
var returnURL = localStorage.getItem('returnUrl');
// remove localstorage return url
localStorage.removeItem('returnUrl');
// redirect to the page where it should be
window.location.href = returnURL;