$(function() {
  var interval = setInterval(function() {
    if (typeof gigya != 'undefined') {
      behavior.getUID();
      clearInterval(interval);
    }
  }, 100);

  var behavior = {
    lastLoginTimestamp: '',
    gigyaUID: '',
    getUID: function() {
      gigya.accounts.getAccountInfo({
        callback: function(res) {
          if (res.errorCode === 0) {
            behavior.lastLoginTimestamp = res.lastLoginTimestamp;
            behavior.gigyaUID = res.UID;
            behavior.getData(res.UID);
          }
        }
      });
    },

    getData: function(UID) {
      var params = {
        oid: UID,
        type: "behavior",
        fields: "*",
        callback: behavior.getCallBack
      };

      gigya.ds.get(params);
    },

    getCallBack: function(response) {
      var crossSiteTraffic, sites;
      var _timeStamp = new Date().getTime();

      if (response.errorCode == 0 && response.data && response.data.crossSiteTraffic) {
        var lastSiteTrafficIndex = response.data.crossSiteTraffic[response.data.crossSiteTraffic.length-1];
        var lastSession = lastSiteTrafficIndex.sessions;
        var lastSitesIndex = response.data.sites[response.data.sites.length-1];

        if (lastSession === behavior.lastLoginTimestamp) {
          var urls = [];
          $.each(response.data.crossSiteTraffic, function(i, item) {
            if (item.sessions === behavior.lastLoginTimestamp && item.host === window.location.host) {
              urls.push(item.url);
            }
          });

          if (urls.indexOf(window.location.href) > -1)
            return;

          lastSiteTrafficIndex.nextURL = window.location.href;
          crossSiteTraffic = response.data.crossSiteTraffic;

          sites = response.data.sites;

          var id = lastSiteTrafficIndex.id + 1;
          var previousURL = lastSiteTrafficIndex.url

          crossSiteTraffic.push({
            "id": id,
            "url": window.location.href,
            "prevURL": previousURL,
            "nextURL": "",
            "host": window.location.host,
            "path": window.location.pathname,
            "date": new Date().toString(),
            "timestamp": _timeStamp,
            "sessions": behavior.lastLoginTimestamp
          });

          if (lastSitesIndex.site === window.location.host) {
            lastSitesIndex.counts = lastSitesIndex.counts+1;
            lastSitesIndex.lastVisitDate = new Date().toString();
            lastSitesIndex.lastVisitTimeStamp = _timeStamp;
          } else {
            sites.push({
              "id": sites.length,
              "site": window.location.host,
              "counts": 1,
              "lastVisitDate": new Date().toString(),
              "lastVisitTimeStamp": _timeStamp,
              "sessions": behavior.lastLoginTimestamp
            });
          }

          // behavior.deleteCrossSiteTrafficData(crossSiteTraffic, sites);
          behavior.updateUserData(crossSiteTraffic, sites);

        } else {
          crossSiteTraffic = response.data.crossSiteTraffic;
          sites = response.data.sites;

          crossSiteTraffic.push({
            "id": 0,
            "url": window.location.href,
            "prevURL": "",
            "nextURL": "",
            "host": window.location.host,
            "path": window.location.pathname,
            "date": new Date().toString(),
            "timestamp": _timeStamp,
            "sessions": behavior.lastLoginTimestamp
          });

          sites.push({
            "id": 0,
            "site": window.location.host,
            "counts": 1,
            "lastVisitDate": new Date().toString(),
            "lastVisitTimeStamp": _timeStamp,
            "sessions": behavior.lastLoginTimestamp
          });

          behavior.updateUserData(crossSiteTraffic, sites);
          // behavior.deleteCrossSiteTrafficData(crossSiteTraffic, sites);
        }

        return;
      }

      crossSiteTraffic = [{
        "id": 0,
        "url": window.location.href,
        "prevURL": "",
        "nextURL": "",
        "host": window.location.host,
        "path": window.location.pathname,
        "date": new Date().toString(),
        "timestamp": _timeStamp,
        "sessions": behavior.lastLoginTimestamp
      }];

      sites = [{
        "id": 0,
        "site": window.location.host,
        "counts": 1,
        "lastVisitDate": new Date().toString(),
        "lastVisitTimeStamp": _timeStamp,
        "sessions": behavior.lastLoginTimestamp
      }];

      behavior.setUserData(crossSiteTraffic, sites);

      return;      
    },

    setUserData: function(siteTraffic, sites) {
      var params = {
        data: {
          "crossSiteTraffic": siteTraffic,
          "sites": sites
        },
        type: "behavior",
        oid: behavior.gigyaUID,
        callback: behavior.resultsCallback
      };

      gigya.ds.store(params);
    },

    updateUserData: function(siteTraffic, sites) {
      var params = {
        data: {
          "crossSiteTraffic": siteTraffic,
          "sites": sites
        },
        type: "behavior",
        oid: behavior.gigyaUID,
        callback: behavior.resultsCallback,
        updateBehavior: "replace"
      };

      gigya.ds.store(params);
    },

    resultsCallback: function(response) {
      if (response.errorCode == 0) {
        console.log("behavior schema saved in data store");
        console.log(response);
      } else {
        console.log("Error : ", response.errorMessage);
      }
    },

    deleteCrossSiteTrafficData: function(crossSiteTraffic, sites) {
      var params = {
        oid: behavior.gigyaUID,
        type: "behavior",
        fields: "*",
        callback: function(response) {
          behavior.saveNewCrossSiteTrafficData(response, crossSiteTraffic, sites);
        }
      };

      gigya.ds.delete(params);
    },

    saveNewCrossSiteTrafficData: function(response, crossSiteTraffic, sites) {
      if (response.errorCode === 0) {
        behavior.setUserData(crossSiteTraffic, sites);
      } else {
        console.log("Error : ", response.errorMessage);
      }
    }
  };
});