### Implementing UNINAV V3

- include jquery on the html [JQuery](https://code.jquery.com/)
- Include uninav under the JQuery
- Include this on head
```html
<link rel="stylesheet" type="text/css" href="//azuretv2devewu00sca63.blob.core.windows.net/abscbnscripts/universalnav/uninav-v3.css">
```
- Include this on the bottom of the body

```html
<script type="text/javascript" src="//azuretv2devewu00sca63.blob.core.windows.net/abscbnscripts/universalnav/uninav-v3.js"></script>	
```
- Include this after uninav script
```html
    <script>
        $('header').uninav({
            <!--settings goes here-->
        });
    </script>
```
### Include mobile navigation

- include this on the page that you want to add mobile navigation

```php
    <div id="mobile-nav"> </div>
```
 
### Use new icons

```html
    <!--old-->
    <i class="uninav-icon"></i>
    <!--new-->
    <i class="uninav-icon-new"></i>
```

### Settings
```php
        type: "default",
        uninav : true,
        uninavAnimation : true,
        headerSticky : true,
        dataDomain: 'onemusic',
        mobileNav: true,
        ssoPageURL : 'http://staging-kapamilyaaccounts.abs-cbn.com/',
        uninavIconTarget: $('#uninav-toggle')
```

- uninav, uninavAnimation, headerSticky, mobileNav is all - `boolean`
- dataDomain : `String //ex. lifestyle, news, entertainment, sports, onemusic`
- uninavIconTarget : `target of the uninav button`
