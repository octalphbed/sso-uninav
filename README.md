Welcome to ABS-CBN Universal Navigation
=======================================


That 'thing' on top, also called the **Uninav**. The main goal of the script is to centralize the code, and unify the headers and Single Sign On of ABS-CBN websites. The script currently has four versions.


## Setup
The script uses **Gulp** and **BrowserSync**. There are many references on the internet regarding **Gulp + BrowserSync** setup. Simply call `gulp` on the root of the Uninav directory to compile and watch the files.

# Version 4


### HEADER MARK UP STRUCTURE 

Adding header tag into body section

```html
<header class="main uninav-header uninav-default uninav-ktx">
    <div class="header-fixed">
        <div class="header-bar">
          <button type="button" class="navbar-toggle collapsed header-nav-toggle" data-toggle="collapse" data-target="#header-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <div class="header-bar-inner">
            <h1 class="logo">
              <a href="index.html">
                <img src="//azuretv2devewu00sca63.blob.core.windows.net/abscbnscripts/universalnav/img/logo-abs-cbn.png" alt="ABS-CBN"> <span>KTX</span>
              </a>
            </h1>
            <ul class="header-social list-unstyled">
              <li class="search-container-lg">
                <div class="search-container">
                  <form class="search-form" autocomplete="off">
                    <input type="text" name="Search" placeholder="Search">
                    <input type="submit" value="Search" class="uninav-icon uninav-icon-search">
                  </form>
                </div>
              </li>
              <li><a href="javascript:void(0);"><i class="uninav-icon uninav-icon-search"><span class="sr-only">search</span></i></a></li>
              <li><a href="#"><i class="uninav-icon uninav-icon-fb"></i> <span class="sr-only">facebook</span></a></li>
              <li><a href="#"><i class="uninav-icon uninav-icon-tw"></i> <span class="sr-only">twitter</span></a></li>
              <li><a href="#"><i class="uninav-icon uninav-icon-yt"></i> <span class="sr-only">youtube</span></a></li>
              <li><a class="sso-uninav-login" href="javascript:void(0)"><i class="uninav-icon-new uninav-icon-login"></i> <span class="sr-only">login</span></a></li>
            </ul>
          </div>
        </div>
        <nav id="header-collapse" class="header-collapse collapse">
          <div class="header-nav">
            <div class="search-container">
              <form class="search-form" autocomplete="off">
                <input type="text" name="Search" placeholder="Search">
                <input type="submit" value="Search" class="uninav-icon uninav-icon-search">
              </form>
            </div>
            <ul class="list-unstyled clearfix">
              <li class="active"><a href="index.html">Home</a></li>
            </ul>
          </div>
        </nav>
    </div>
</header>
```

### INCLUDE CSS

#### For Live

```sh
<link rel="stylesheet" type="text/css" href="//assets.abs-cbn.com/universalnav/live/uninav-v4.css">
```

#### For Testing
```sh
<link rel="stylesheet" type="text/css" href="//assets.abs-cbn.com/universalnav/uninav-v4.css">
```

### INCLUDE UPDATED JQUERY LIBRARY ON HTML FOR THE UNINAV SCRIPT TO WORK

You can find Jquery Library following link below [jQuery](http://jquery.com/download).

```sh
<script type=”text/javascript” src=” https://code.jquery.com/jquery-3.2.1.min.js”></script>
```

### ADD UNINAV SCRIPT

Include this at the bottom of the body

#### For Live

```sh
<script type="text/javascript" src="http://assets.abs-cbn.com/universalnav/live/uninav-v4.js"></script>
```

#### For Testing

```sh
<script type="text/javascript" src="http://assets.abs-cbn.com/universalnav/uninav-v4.js"></script>
```

### PLUGIN CONFIGURATION

Add this script/library to fully utilize the universal navigation.

```sh
<script type="text/javascript">
$(function() {
     $('header').uninav({
        type: "default",
        uninav : true,
        sso: true,
        referrer: "ktx",
        ssoLogin: true,
        oldsearch: false,
        domainKey: "ktx"
    });
});
</script>
```

### DATA DICTIONARY

|      KEYWORD     | VALUE         | DEFINITON       | REQUIRED (Y/N) |
 ----------------- | --------------| ----------------| -------------- |
| Type             | Default We don’t have any custom value right now aside from default.  | Revert automatically to a preselected option (Uninav Network/ Log in) | Y |
| Uninav           | TRUE   | Shows navigational icon | Y |
|                  | FALSE  | No option to hide navigational icon  No False value for now | Y |
| SSO              | Website that uses Uninav is required to log in to SSO, since not all websites are integrated to SSO, we do not have any value for now. | Single Sign On SSO is a session and user authentication service that permits a user  to use one set of login credentials (e.g. name and password) to access multiple applications. | Y | 
| Referrer         | N/A | ABS-CBN Websites or the site referrence Example: Lifestyle, News, Sports | Y |
| Old Search       | TRUE | if search icon is hoverable (a text box slide from right to left) | Y |
|                  | FALSE | if search icon is clickable (a search pop up or lightbox) | Y |
| Data domain      | N/A | Data domain is a referrence on what api key should be used. Same example on referrer. | Y |

### SEARCH IMPLEMENTATION

Please take note on search function, customized based on site requirements. 
Include this before the facebook icon on header.

```html
<li><a href="javascript:void(0);"><i class="uninav-icon uninav-icon-search"><span class="sr-only">search</span></i></a></li>
```

See options below for choice of use:


#### Option 1: Old implementation without animation

![Search OLD](https://bitbucket.org/frontenddevelopment/universal-nav/downloads/Picture1.png) 

To configure, **set old search to true**;

```sh
$(function()
    $('header').uninav({
        oldsearch: false,
    });
});
```

#### Option 2: Search in lightbox implementation

![New Search](https://bitbucket.org/frontenddevelopment/universal-nav/downloads/Picture2.png) 

To configure, **set old search to false**;


```sh
$(function()
     $('header').uninav({
        oldsearch: false,
    });
});
```

### USER LOG OUT

When a user logs out from one of the member sites, Gigya logs the user out of all other sites in the relevant segment, and terminates that user's active Gigya sessions in sites that belong to the segment.

Make sure to complete the following steps to enable a successful SSO logout:


1.  Every member site should have a logout URL. This is a page that contains the Gigya script and issues a call to accounts.logout  /  socialize.logout  accordingly, effectively logging out the active user when the page is loaded.
2.  Send a complete list of logout URLs to your Implementation Consultant. Make sure to have a logout URL for every site.
3.  Make sure to implement user log out with a call to accounts.logout / socialize.logout to notify Gigya.


Once all is set up, Gigya will recognize when a call to accounts.logout or socialize.logout comes from a site that belongs to an SSO segment. In such cases, Gigya will call the logout URL of each member site of the segment, thus logging the user out of all SSO group sites.
Keep in mind that this process only terminates Gigya sessions. If you manage site sessions independently, you will need to terminate those sessions explicitly, as Gigya cannot terminate your site session for you.


#### HTML EXAMPLE

All websites integrated with SSO will have to create a logout page in this format: [http://www.website.com/logout/ssoLogout.html](http://www.website.com/logout/ssoLogout.html)
This page is required in Gigya configuration for single logout function to work properly. 

You can just create an html file such as logout.html using the sample below. In addition, it allows the proper    clearing of login sessions across websites that are integrated with SSO.

Remember to replace the SITE-API-KEY with the actual apikey provided:
Email or secure API key to the resource person.

```html
<!DOCTYPE html>
<html>
                <head>
                                <script type="text/javascript" lang="javascript" src="http://cdn.gigya.com/JS/gigya.js?apikey=SITE-API-KEY ">
                                </script>
                                <script type="text/javascript">
                                         function pageLoadHandler(){ gigya.accounts.logout();}
                                </script>
                </head>
                <body onload="javascript:pageLoadHandler();"></body>
</html>

```

### SAVE LATER BUTTON IMPLEMENTATION (New function in uninav)

**Meta tags** 

Adding meta tags inside article tag, please make sure to add the below tag inside the article content type such as article, video and photo page. 

```sh
<meta itemprop="headline" content="Title of the article">
<meta itemprop="description" content="a short description">
<meta itemprop="thumbnailUrl" content="the image or thumbnail souce url">
<meta itemprop="url" content="the url of the article">
<meta property="og:site_name" content="Site name or source were it came from">


```
**Button**

You can place it anywhere inside the **article, photos, and video content page**.

**Option 1** For Square button

![Box type](https://bitbucket.org/frontenddevelopment/universal-nav/downloads/box.png) 

```html
<div class="bookmarked-saved-container">
    <button class="bookmarked"><span>Save</span></button>
</div>
```

**Option 2** For Landscape button

![landscape](https://bitbucket.org/frontenddevelopment/universal-nav/downloads/landscape.png) 

```html
<div class="bookmarked-saved-container landscape">
    <button class="bookmarked"><span>Save</span></button>
</div>
```


# Version 3

### Implementing UNINAV V3

### step 1
- Same implementation from uninav v2
- include jquery on the html [JQuery](https://code.jquery.com/)
- Include uninav under the JQuery
- Include the css below under head

```html
<link rel="stylesheet" type="text/css" href="http://assets.abs-cbn.com/universalnav/uninav-v3.css">
```
- Include this on the bottom of the body

```html
<script type="text/javascript" src="http://assets.abs-cbn.com/universalnav/uninav-v3.js"></script>    
```
### step 2

- include on you master js or if you have pages that not using sso set sso: `false`
```html
    <script>
        $('header').uninav({
            <!--settings goes here-->
        });
    </script>
```
### step 3
### Include mobile navigation

- include this on header change the news to your own data domain

```php
<div id="mobile-uninav">
    <section id="mobile-icons"> 
            <div class="pull-right mobile-menu"> 
                    <a href="javascript:void(0);" class="link-uninav"> 
                            <span id="mobile-icon" class="icon uninav-icon-dashboard"></span> 
                    </a> 
            </div> 
            <div class="pull-right"> 
                    <a href="javascript:void(0);" class="link-uninav"> 
                            <span id="mobile-icon" class="icon uninav-icon-new"></span> 
                    </a> 
            </div> 
            <div class="pull-left"> 
                    <a href="http://staging-kapamilyaaccounts.abs-cbn.com/profile" class="link-uninav"> 
                            <span id="mobile-icon" class="icon uninav-icon-login-new"><p></p></span> 
                    </a> 
            </div> 
            <div class="pull-right"> 
                    <a href="javascript:void(0);" class="link-uninav"> 
                            <span id="mobile-icon" class="icon uninav-icon-search-new"></span> 
                    </a> 
            </div> 
            <div class="pull-left site-logo"> 
                    <a href="javascript:void(0);" class="link-uninav"> 
                            <span id="mobile-icon" class="icon news"></span> 
                    </a> 
            </div> 
    </section>
</div>
```
 
### Use new icons

```html
    <!--old-->
    <i class="uninav-icon"></i>
    <!--new-->
    <i class="uninav-icon-new"></i>
```
### Use new search 
- include this before the facebook icon on header
```html
<li><a href="javascript:void(0);"><i class="uninav-icon-new uninav-icon-search"><span class="sr-only">search</span></i></a></li>
    
```

### Settings
```php
        type: "default", //required 
        uninav : true,  //required
        dataDomain: 'onemusic', //required
        mobileNav: true, //required
        oldsearch: true //for news
        sso: true //can use for special cases like login page , profile page 
```

- uninav, uninavAnimation, headerSticky, mobileNav is all - `boolean`
- dataDomain : `String` available data domain `onemusic, news, lifestyle, entertainment, sports, iwanttv, tfc, mysky, skyondemand`
- oldsearch : `boolean`
- uninavIconTarget : `target of the uninav button`
- customDashBoard : [{`title:` `string`, `url:` `string`}, {`title:` `string`, `url:` `string`} ... ]

# Version 2


Used for sites that implement the whole universal header (folding header).

**CSS:**
```html
<link rel="stylesheet" href="https://assets.abs-cbn.com/universalnav/uninav-v2.css">
```

**JS:**
```html
<script src="https://assets.abs-cbn.com/universalnav/uninav-v2.js"></script>
```

This version relies on the markup being already present on the site of implementation. The script and the stylesheet then applies the proper styling and functionality.

### Current sites of implementation based on skins stylesheet: *(as of Jul 11, 2016)*

- Corporate
- Investor
- Entertainment
- Sports
- Lifestyle
- Television
- News
- Careers
- Licensing
- Chicken Pork Adobo
- TVplus
- ABS-CBN mobile
- StarCreatives
- Starmagic Workshop

### Base markup
```html
<header class="main uninav-header uninav-[layout] uninav-[skin-name]">
    <div class="header-fixed">
    </div>
</header>
```
* `[layout]` - The desired layout. Current layouts available:
    * `default`
    * `layout-2`
* `[skin-name]` - The site's skin name. Skins are located at `_header-skins.scss`
    * `corporate`
    * `investor`
    * `entertainment`
    * `sports`
    * `lifestyle`
    * `television`
    * `news`
    * `careers`
    * `licensing`
    * `cpa`
    * `tvplus`
    * `abscbnmobile`
    * `starcreatives`
    * `starmagic-workshop`

##### Sample:
```html
<header class="main uninav-header uninav-default uninav-corporate"></header>
```

#### Layout `default` markup

```html
<div class="header-bar">
    <button type="button" class="navbar-toggle collapsed header-nav-toggle" data-toggle="collapse" data-target="#header-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <button id="uninav-toggle" type="button" class="navbar-toggle collapsed uninav-toggle pull-right">
        <span class="sr-only">Toggle Universal Navigation</span>
        <span class="uninav-icon uninav-icon-uninav"></span>
    </button>
    <div class="header-bar-inner">
        <h1 class="logo">
            <a href="index.html">
                <img src="img/logo-abs-cbn.png" alt="ABS-CBN"> <span>Site Name</span>
            </a>
        </h1>
        <ul class="header-social list-unstyled">
            <li class="search-container-lg"></li>
            <li><a href="#"><i class="uninav-icon uninav-icon-fb"></i> <span class="sr-only">facebook</span></a></li>
            <li><a href="#"><i class="uninav-icon uninav-icon-tw"></i> <span class="sr-only">twitter</span></a></li>
            <li><a href="#"><i class="uninav-icon uninav-icon-yt"></i> <span class="sr-only">youtube</span></a></li>
        </ul>
    </div>
</div>
<nav id="header-collapse" class="header-collapse collapse">
    <div class="header-nav">
        <div class="search-container">
            <form class="search-form" autocomplete="off">
                <input type="search" name="search" placeholder="Search">
                <input type="submit" value="Search" class="uninav-icon uninav-icon-search">
            </form>
        </div>
        <ul class="list-unstyled clearfix">
            <li class="active"><a href="#">Link 1</a></li>
            <li><a href="#">Link 2</a></li>
            <li>
                <a href="#">Link 3 with sublinks <span class="has-child-drop"></span></a>
                <ul>
                    <li><a href="#">Sublink 1</a></li>
                    <li><a href="#">Sublink 2</a></li>
                    <li><a href="#">Sublink 3</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <div class="header-login">
        <ul class="list-unstyled">
            <li><a href="#"><i class="uninav-icon uninav-icon-login"></i> Log in</a></li>
            <li>
                <a href="#" class="clearfix header-profile">
                    <img src="http://placehold.it/20x20" alt=""> John Doe
                    <span class="pull-right text-underline">View Profile</span>
                </a>
            </li>
            <li><a href="#">Log Out</a></li>
        </ul>
    </div>
</nav>
```


#### Layout `layout-2` markup

```html
<div class="header-bar">
    <button type="button" class="navbar-toggle collapsed header-nav-toggle" data-toggle="collapse" data-target="#header-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <button id="uninav-toggle" type="button" class="navbar-toggle collapsed uninav-toggle pull-right">
        <span class="sr-only">Toggle Universal Navigation</span>
        <span class="uninav-icon uninav-icon-uninav"></span>
    </button>
    <div class="header-bar-inner">
        <h1 class="logo">
            <a href="index.html">
                <img src="img/logo-abs-cbn.png" alt="ABS-CBN"> <span>[site-name]</span>
            </a>
        </h1>
        <nav id="header-collapse" class="header-collapse collapse">
            <div class="header-nav">
                <div class="search-container">
                    <form class="search-form" autocomplete="off">
                        <input type="search" name="search" placeholder="Search">
                        <input type="submit" value="Search" class="uninav-icon uninav-icon-search">
                    </form>
                </div>
                <ul class="list-unstyled clearfix">
                    <li class="active"><a href="#">Link 1</a></li>
                    <li><a href="#">Link 2</a></li>
                    <li>
                        <a href="#">Link 3 with sublinks <span class="has-child-drop"></span></a>
                        <ul>
                            <li><a href="#">Sublink 1</a></li>
                            <li><a href="#">Sublink 2</a></li>
                            <li><a href="#">Sublink 3</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="header-login">
                <ul class="list-unstyled">
                    <li><a href="#"><i class="uninav-icon uninav-icon-login"></i> Log in</a></li>
                    <li>
                        <a href="#" class="clearfix header-profile">
                            <img src="http://placehold.it/20x20" alt=""> John Doe
                            <span class="pull-right text-underline">View Profile</span>
                        </a>
                    </li>
                    <li><a href="#">Log Out</a></li>
                </ul>
            </div>
        </nav>
        <ul class="header-social list-unstyled">
            <li class="search-container-lg"></li>
            <li><a href="#"><i class="uninav-icon uninav-icon-fb"></i> <span class="sr-only">facebook</span></a></li>
            <li><a href="#"><i class="uninav-icon uninav-icon-tw"></i> <span class="sr-only">twitter</span></a></li>
            <li><a href="#"><i class="uninav-icon uninav-icon-yt"></i> <span class="sr-only">youtube</span></a></li>
        </ul>
    </div>
</div>
```


### Base Skin styling (based on Corporate skin)
```scss
header.uninav-[skin-name] {
    $bg-color: #252525;
    $text-color: #898989;
    $nav-color: #1e1e1e;
    $border-color: #898989;
    $inverse-color: #fff;
    &, .header-bar { background-color: $bg-color; }
    .header-bar {
        background-image: url(img/ring-black-md.png);
        @media (min-width: $screen-lg) {
            background-image: url(img/ring-black-lg.png);
        }
    }
    .uninav-toggle { background: darken($nav-color, 3%); }
    .header-nav-toggle[aria-expanded="true"] { background-color: $nav-color; }
    .icon-bar { background-color: $inverse-color; }
    .logo a { color: $inverse-color; }
    .header-collapse {
        background-color: $nav-color;
        a { color: $text-color; }
        a:hover, a:active { background-color: $bg-color; }
    }
    .header-nav {
        li.active > a { color: $inverse-color; }
        > ul {
            @media (min-width: $screen-lg) {
                > li:hover > a { background-color: $bg-color; }
            }
            ul {
                background-color: $bg-color;
                a:hover, a:active { background-color: $nav-color; }
            }
        }
        .has-child-drop {
            &:hover, &:active, &.active { background-color: $bg-color; }
            &:after { border-top-color: $border-color; }
        }
    }
    .search-container { input[type=text], input[type=search] { border-color: $inverse-color; color: $inverse-color; } }
    .search-container-lg .search-container:hover { input[type=text], input[type=search] { border-color: $inverse-color; color: $inverse-color } }
    .header-login a { border-top-color: $border-color; }
}
```

### Implementation and new skin creation

#### Step 1:
Start assembling the header markup. Choose your desired layout and paste inside the base markup. In this example, we're using the `default` layout with a skin of `newskin`. Customize based on requirements:
```html
<header class="main uninav-header uninav-default uninav-newskin">
    <div class="header-fixed">
        <div class="header-bar">
            <!-- more code... -->
        </div>
        <nav id="header-collapse" class="header-collapse collapse">
            <!-- more code... -->
        </nav>
    </div>
</header>
```

#### Step 2:
Create a new skin at `_header-skins.scss`. Use the template found on the **Base Skin styling** section. Customize based on requirements.

# Version 1


Used for sites that only need to implement the **Uninav Button**, which shows the links of ABS-CBN websites.

**CSS:**
```html
<link rel="stylesheet" href="https://assets.abs-cbn.com/universalnav/uninav.css">
```

**JS:**
```html
<script src="https://assets.abs-cbn.com/universalnav/uninav.js"></script>
```

The main logic is to override websites based on their **domain name**. A couple of reasons for this approach:
- **We can target websites without heavily changing the markup**. All clients need to do is to put the Uninav CSS and JS on their templates.
- **The script was to be implemented on older sites** - sites which needed complicated approval on code changes both the frontend and the backend
- **Sites have different stylesheets**
- **Sites have different JS libraries**, specifically jQuery (including non-"$.fn.on" versions)
- If ever clients back out from its implementation, **they only have to remove two lines of code**
- Debugging is done only on the Uninav side

### Current sites of implementation as listed on the script: *(as of Jul 11, 2016)*

- http://www.abs-cbn.com
- http://www.abs-cbnnews.com
- http://abscbnmobile.com
- http://accounts.abs-cbn.com
- http://www.choosephilippines.com
- http://corporate.abs-cbn.com
- http://www.iwantv.com.ph
- http://dzmm.abs-cbnnews.com
- http://bmpm.abs-cbnnews.com
- http://internationalsales.abs-cbn.com
- http://marketportal.abs-cbn.com
- http://mmk.abs-cbn.com
- http://www.mor1019.com
- http://myxph.com
- http://push.abs-cbn.com
- http://starmagic.abs-cbn.com
- http://starcinema.abs-cbn.com
- http://starcinemamobile.abs-cbn.com
- http://starmusic.abs-cbn.com
- http://store.abs-cbn.com
- http://tvplus.abs-cbn.com

### Implementation

#### Step 1:
Create a new block on `domains` variable with the following base:
```javascript
'www.example.com': {
    togglerCreate: function() {
    },
    targetHeaderHeight: function() {
    },
    uninavClass: 'uninav-example'
}
```
* Let's assume the site is at `www.example.com`.
* `togglerCreate` is used to create the button/toggler. The function needs to return the toggler element.
* `targetHeaderHeight` is used to compute the height where the link will start sliding.
* `uninavClass` is a class appended to the body and used to customize the look of the elements through CSS.
* If the site is responsive, add `uninav-responsive` class (e.g. `uninavClass: 'uninav-example uninav-responsive'`)

#### Step 2:
Create a way to insert the universal nav toggler/button:
```javascript
togglerCreate: function() {
    // Using common toggler.template as the toggler's template (see src)
    $(toggler.template).appendTo('#someElement');
    // Always return .uninav-toggler!
    return $('.uninav-toggler');
}
```

#### Step 3:
Compute height from where the nav will start sliding:
```javascript
targetHeaderHeight: function() {
    return $('#siteHeader').outerHeight();
}
```

#### Step 4:
Create a new CSS block on `_main.scss` (in our case, this is `.uninav-example`):
```css
// www.example.com
.uninav-example {
    #someElement {
        padding: 20px;
    }
    .uninav-toggler {
        color: yellow;
    }
}
```

#### Happy Coding