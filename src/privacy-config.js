var privacyPolicy = {};

var uninavUserID = null;

privacyPolicy.privacyInit = function (uRes, ssoApiUrl, DSStoreGetPrivacyApiUrl , uPrivacy) {
    uninavUserID = uRes.UID;
    localStorage.setItem('uninavUserID', uninavUserID);

    var isAcceptedPrivacyPolicy = localStorage.getItem('isAcceptedPrivacyPolicy'),
        isExistPrivacyPolicy = localStorage.getItem('isExistPrivacyPolicy'),
        datePrivacyPolicy = localStorage.getItem('datePrivacyPolicy'),
        isAPIReachedLimit = localStorage.getItem('isAPIReachedLimitPrivacyPolicy');

    if (isAcceptedPrivacyPolicy !== null && isAPIReachedLimit !== null && isExistPrivacyPolicy !== null) {
        if (isAcceptedPrivacyPolicy === 'false' && isExistPrivacyPolicy === 'true') {
            privacyPolicy.createPrivacyPolicy(ssoApiUrl, uPrivacy); // Call the Pop up for the accept privacy policy
            
        } else if (isAcceptedPrivacyPolicy === 'false' && isExistPrivacyPolicy === 'false') {
            privacyPolicy.createPrivacyPolicy(ssoApiUrl,uPrivacy); // Call the Pop up for the accept privacy policy
        }
    } else {
        privacyPolicy.IsAcceptPrivacyPolicy(uninavUserID, 'pop-up', DSStoreGetPrivacyApiUrl, DSStorePrivacyApiUrl = null, ssoApiUrl, uPrivacy); // Call the privacy policy API if the user is accepted the policy
    }  
};

privacyPolicy.createPrivacyPolicy = function (ssoApiUrl, uPrivacy) {
    
    var headerImage = uPrivacy.headerImage; //? uPrivacy.headerImage : '//assets.abs-cbn.com/universalnav/img/abs-cbn-ring-logo-desktop.png';
    var title = uPrivacy.title; // ? uPrivacy.title : 'Welcome, Kapamilya!';
    var logo = uPrivacy.logo;  //? uPrivacy.logo : '//assets.abs-cbn.com/universalnav/img/login_abscbn_logo_retina_version.png';
    var contentCopy = uPrivacy.contentCopy ? uPrivacy.contentCopy : 'We&apos;ve updated our Privacy Notice<br class="mobile-view"/> to keep your data<br class="desk-view"> safe and secure, <br class="mobile-view"/>Kapamilya!<br>Continuing to use this site means <br class="mobile-view"> you agree <br class="desk-view"> to these updates.';

    $('body').append('<div class="privacy-policy-blocker opac"></div>');
    $('.privacy-policy-blocker').show();
    $('html').css({'overflow':'hidden', '-webkit-overflow-scrolling':'touch'});
    privacyPolicy.stopScroll();

    var markup = '';
    if (headerImage !== 'none') {
        var uheaderImage = uPrivacy.headerImage ? uPrivacy.headerImage : '//assets.abs-cbn.com/universalnav/img/abs-cbn-ring-logo-desktop.png';
        markup += '<div class="uninav-privacy-policy" style="background-image : url(' + uheaderImage + ')">';
    } else {
        markup += '<div class="uninav-privacy-policy" style="background-image : url()">';
    }

            markup += '<div class="upp-content">';
                // markup += '<image src="https://assets.abs-cbn.com/universalnav/img/logo_70x61.png" class="up-logo">';
            if (logo !== 'none') {
                var ulogo = uPrivacy.logo ? uPrivacy.logo : '//assets.abs-cbn.com/universalnav/img/login_abscbn_logo_retina_version.png';
                markup += '<img src="' + ulogo + '" class="up-logo" width="70">';
            }
                
                markup += '<br />';
                markup += '<div class="up-inner-content">';
                markup += '<h4 class="up-head-title">' + contentCopy + ' </h4>';
                markup += '<div class="up-sub-btn">';
                    markup += '<a href="' + ssoApiUrl + '/privacy-policy" class="up-see-updates" target="_blank">View Privacy Notice</a>';
                    markup += '<br /><br />';
                    markup += '<button class="up-btn-accept is-accept-privacy-policy">I AGREE!</button>';
                    // markup += '<br /><br />';
                    // markup += '<a href="#" class="up-see-updates up-do-this-later">I\'ll do this later</a>';
                markup += '</div>';
                markup += '</div>';
            markup += '</div>';
            markup += '</div>';
        markup += '</div>';
    $('body').append(markup);
};

privacyPolicy.stopScroll = function () {
    disableScroll = true;
    scrollPos = $(window).scrollTop();
};

privacyPolicy.enableScroll =  function () {
    disableScroll = false;
};

privacyPolicy.IsAcceptPrivacyPolicy = function (uid, mode, DSStoreGetPrivacyApiUrl, DSStorePrivacyApiUrl, ssoApiUrl, uPrivacy) {
    $.ajax({
        type: 'POST',
        url: DSStoreGetPrivacyApiUrl,
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: JSON.stringify({
            uid: uid,
            url: "" // no=use
        }),
        success: function (res) {
            // The pop up will not showup While API is returning isAPIReachedLimit=false
            console.log(mode);
            console.log(res);
            if (!res.isAccepted) {
                localStorage.setItem('isAcceptedPrivacyPolicy', res.isAccepted);
                localStorage.setItem('isExistPrivacyPolicy', res.isExist);

                if (res.isAccepted === false && mode === 'pop-up') { // Call to pop-up the privacy
                    privacyPolicy.createPrivacyPolicy(ssoApiUrl,uPrivacy);
                }
                else if (res.isAccepted === false && mode === 'click') { // Click the I Agree buttton
                    privacyPolicy.acceptPrivacyPolicy(DSStorePrivacyApiUrl);
                }
            } else {
                localStorage.removeItem('uninavUserID');
                localStorage.setItem('isAcceptedPrivacyPolicy', res.isAccepted);
                localStorage.setItem('datePrivacyPolicy', res.date);
                localStorage.setItem('isExistPrivacyPolicy', res.isExist);
                localStorage.setItem('isAPIReachedLimitPrivacyPolicy', res.isAPIReachedLimit);
            }
        }
    });
};


privacyPolicy.acceptPrivacyPolicy =  function (DSStorePrivacyApiUrl) {
    $.ajax({
        type: 'POST',
        url: DSStorePrivacyApiUrl,
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: JSON.stringify({
            uid: localStorage.getItem('uninavUserID'),
            url: window.location.href,
            isAccepted: true,
        }),
        success: function (res) {
            console.log(res);
            if (res.isSuccess) {
                localStorage.removeItem('uninavUserID');
                localStorage.setItem('isAcceptedPrivacyPolicy', 'true');
                localStorage.setItem('isAPIReachedLimitPrivacyPolicy', 'false');
                localStorage.setItem('isExistPrivacyPolicy', 'true');
                // Remove Cookie Law
                localStorage.setItem('uninavIsAgreeCookieLaw', 'true');
                $('.uninav-cookie-law-container').remove();
            }
        }
    });
};
