(function($) {
  'use strict';
  var isMakulitTestSite = (window.location.host === "testnews.abs-cbn.com" || window.location.host === "test.entertainment.abs-cbn.com");
  
  $.fn.uninav = function(options) {
    var _this = this,
    defaults = {
      //Basic variables
      header: $('header'),
      window: $(window),
      body: $('body'),
      domain: document.domain,
      main: $('div.main'),
      document: $(document),

      //Header variables
      navbar: $('.header-nav-toggle'),
      navbarCollapse: $('#header-collapse'),
      target: _this,
      type: '',
      uninav: false,
      uninavTarget: '.uninav-main',
      uninavIconTarget: $('#uninav-toggle'),
      uninavAnimation: true,
      uninavAnimationSpeed: 'fast',
      uninavHighLight: '',
      mobileToggler: $('.has-child-drop'),
      navbarToggler: true,
      mobileUninav: $('#mobile-uninav'),
      headerSticky: true,
      mobileNav: true,
      dataDomain: '',
      oldsearch: false,

      //SSO variables
      sso: true,
      ssoTarget: $('.uninav-icon-login'),
      ssoMobileTarget: $('.uninav-icon-login-new'),
      ssoStatus: true,
      ssoApikey: '',
      customDashBoard: [],
      kapamilyaImage: '',
      kapamilyaImagemobile: '',
      kapamilyaImageDashboard: '',
      sourceURL: '',
      ssoPageURL: store.kapamilyaUrl
  },
  
    x = this.config = $.extend(defaults, options);
    
    function createHeader() {
      function createUniNav() {
        var template = {
          main: '<div id="uninavMainLinks" class="uninav-main"></div>'
        };

        if (x.uninav === true) {
          x.header.append(template.main);
          $('#uninavMainLinks').load(store.uninavLinks);
        } else {
          x.uninavIconTarget.hide();
        }
      }

      function Toggler() {
        // DESKTOP NAVBAR TOGGLER
        // No animation on mobile

        /* TOGGLER FUNCTIONS */
        var toggler = {
          areaExpandedTrue: function() {
            $('.header-nav-toggle').attr('aria-expanded', 'false');
            x.navbarCollapse.removeClass('in');
            x.navbar.addClass('collapsed');
            x.navbarCollapse.attr('aria-expanded', 'false');
          },

          areaExpandedFalse: function() {
            $('.header-nav-toggle').attr('aria-expanded', 'true');
            x.navbarCollapse.attr('aria-expanded', 'true');
            x.navbarCollapse.removeClass('collapse').addClass('collapsing');
            x.navbarCollapse.removeClass('collapsing').addClass('collapse in');
            x.navbar.removeClass('collapsed');
          },

          uninavAnimationTrue: function() {
            x.uninavIconTarget.on('click', function(e) {
              e.preventDefault();
              $(this).toggleClass('active');
              $(this).find('.uninav-icon-uninav').toggleClass('active');
              // $(x.uninavTarget).removeAttr('style');
              $(x.uninavTarget).toggleClass('active');
              $(x.uninavTarget).stop().slideToggle(x.uninavAnimationSpeed, function() {
                // callback function for slideToggle
                // do nothiing
              });
            });
          },

          uninavAnimationFalse: function() {
            x.uninavIconTarget.on('click', function() {
              $(this).toggleClass('active');
              $(x.uninavTarget).stop().toggle();
            });
          },

          oldSearchFalse: function() {
            $('.uninav-icon-search').parent().on('click', function(e) {
              e.preventDefault();
              $(this).toggleClass('active');
              
              if ($(this).is('.active')) {
                x.header.append(SearchPopUp());
                $('#mobile-search').fadeIn("fast");
                $('body, html').css({ "overflow": "hidden" });
              } else {
                $('#mobile-search').fadeOut().remove();
                $('body, html').css({ "overflow": "" });
              }
            });
          },

          oldSearchTrue: function() {
            x.window.bind('load resize', function() {
              if (x.window.width() < 945) {
                $('.header-nav .search-container').show();
              } else {
                $('.header-nav .search-container').hide();
              }
            });

            $('.search-container-lg .search-container').show();
            $('.uninav-icon-new.uninav-icon-search').parents('li').hide();
          }
        };

        x.navbar.on('click', function(e) {
          e.preventDefault();
          var y = x.navbar.attr('aria-expanded');

          if (y === "false") {
            toggler.areaExpandedFalse();
          } else {
            toggler.areaExpandedTrue();
          }
        });

        //DESKTOP UNINAV TOGGLER
        //Available animation settings "uninavAnimation" : boolean
        if (x.uninavAnimation === true) {
          toggler.uninavAnimationTrue();
        } else {
          toggler.uninavAnimationFalse();
        }

        // SEARCH TOGGLER
        if (x.oldsearch === false) {
          toggler.oldSearchFalse();
        } else {
          toggler.oldSearchTrue();
        }
        
        // SSO TOGGLER
        $(document).on('click', '.sso-active', function(e) {
          e.preventDefault();
          $(this).children().toggleClass('active');
        });

        $(document).on('click touchstart', '.uninav-icon-dashboard', function() {
          if ($(this).is('.active')) {
            $('.mobile-dashboard').show();
          } else {
            $('.mobile-dashboard').hide();
          }
        });

        $(document).on('click', '.sso-not-active, .sso-not-active-mobile', function() {
          switch(getURL.getWebsiteName().toLowerCase().split(' ').join('')) {
            case 'mmk': 
            case 'news':
              $('.lightbox').show();
              break;
            default: setPageLocation(x.ssoPageURL); break;
          }
        });
      }

      function mobileAccordion() {
        x.mobileToggler.each(function() {
          $(this).on('click', function() {
            if (!$(this).is('.active')) {
              x.mobileToggler.stop().removeClass('active');
              x.mobileToggler.stop().siblings('ul').slideUp();
              $(this).stop().addClass('active');
              $(this).stop().siblings('ul').slideDown();
            } else {
              $(this).stop().removeClass('active');
              $(this).stop().siblings('ul').slideUp();
            }
          });
        });
      }

      function headerNavResize() {
        x.window.bind('resize load', function() {
          $(".sso-uninav-login").show();
          var headerList = $('.header-nav > ul'),
          windowWidth = x.window.width();
          $.each(headerList, function() {
            var headerUL = $(this),
            navULpad = headerList.innerWidth() - headerList.width();

            $('> li:not(.not-priority)', headerUL).each(function() {
              navULpad += $(this).outerWidth();
            });
            $('.not-priority', headerUL).each(function() {
              var _this = $(this),
              notPrioTotal = navULpad + _this.width();
              
              if (notPrioTotal >= windowWidth) {
                _this.hide();
              } else {
                _this.show();
                navULpad = notPrioTotal;
              }
            });
            
            if (navULpad < windowWidth) { return; }

            var remainingLIs = $('> li:not(.not-priority)', headerUL).show().filter(':visible');
            $(remainingLIs.get().reverse()).each(function() {
              var _this = $(this);
              _this.hide();
              navULpad -= _this.width();
              if (navULpad < windowWidth) { return false; }
            });
          });
        });
      }

      function pageScroll() {
        var lastScroll = 0, didScroll = false;
        x.window.bind('scroll load touchmove', function() {
          didScroll = true;
          hideDashBoard();
          $('.mobile-dashboard').hide();
          $('.uninav-icon-dashboard').removeClass('active');
        });
        
        setInterval(function() {
          if (didScroll) {
            didScroll = false;
            if (x.window.width() <= 945) {
            // some code..
              if (x.headerSticky === true) {
                var scrollTop = $(this).scrollTop();
                  if (scrollTop > lastScroll) {
                    _this.addClass('header-sticky');
                    _this.children('.header-fixed').removeClass('header-fixed-pinned').addClass('header-fixed-unpinned');
                    $('#mobile-uninav').removeClass('pinned').addClass('unpinned'); 
                  }
                  if (scrollTop < lastScroll) {
                    _this.removeClass('header-sticky');
                    _this.children('.header-fixed').removeClass('header-fixed-unpinned').addClass('header-fixed-pinned');
                    $('#mobile-uninav').removeClass('unpinned').addClass('pinned');
                  }
                  if (x.window.scrollTop() <= 0) {
                    _this.removeClass('header-sticky');
                    _this.children('.header-fixed').removeClass('header-fixed-unpinned').addClass('header-fixed-pinned');
                    $('#mobile-uninav').removeClass('unpinned').addClass('pinned');
                  }
                  if ($(window).scrollTop() + $(window).height() === $(document).height()) {
                    _this.removeClass('header-sticky');
                    _this.children('.header-fixed').removeClass('header-fixed-unpinned').addClass('header-fixed-pinned');
                    $('#mobile-uninav').removeClass('pinned').addClass('unpinned');
                  }
                  lastScroll = scrollTop;
              } else { /* Do nothing */ }
            } else {
              if (x.headerSticky === true) {
                if (x.window.scrollTop() + x.window.height() === $(document).height()) {
                  _this.children('.header-fixed').removeClass('header-fixed-not-top headroom--not-bottom').addClass('header-fixed-init header-fixed-not-top headroom--bottom');
                }
                if (x.window.scrollTop() >= 0 && x.window.scrollTop() + x.window.height() !== $(document).height()) {
                  _this.addClass('header-sticky');
                  _this.children('.header-fixed').removeClass('header-fixed-top headroom--bottom').addClass('header-fixed-init headroom--not-bottom header-fixed-not-top');
                }
                if (x.window.scrollTop() === 0) {
                  _this.removeClass('header-sticky');
                  _this.children('.header-fixed').removeClass('header-fixed-not-top headroom--bottom headroom--not-bottom').addClass('header-fixed-init headroom--not-bottom header-fixed-top');
                }
              }
            }
          }
        }, 10);
      }

      function toggleMobile() {
        $('.link-uninav').each(function() {
          $(this).on('click', function() {
            $(this).toggleClass("active", " ");
            $('.link-uninav').not($(this)).children().removeClass('active');
            $(this).children().toggleClass('active');
            $('.mobile-dashboard').hide();
            
            if ($(this).children().is('.uninav-icon-new') && $(this).children().is('.active')) {
              $(x.uninavTarget).stop().slideDown(x.uninavAnimationSpeed);
              disableScroll();
              $('body, html').css({ "overflow": "hidden" });
            } else {
              $(x.uninavTarget).stop().slideUp(x.uninavAnimationSpeed);
              enableScroll();
            }
            
            if ($(this).children().is('.uninav-icon-search-new') && $(this).children().is('.active')) {
              x.header.append(SearchPopUp());
              $('#mobile-search').fadeIn("fast");
              disableScroll();
              $('body, html').css({ "overflow": "hidden" });
            } else {
              $('#mobile-search').fadeOut().remove();
              enableScroll();
            }
            
            if ($(this).children().is('.uninav-icon-new') && !$(this).children().is('.active')) {
              $('body, html').css({ "overflow": "auto" });
            }
            
            if ($(this).children().is('.uninav-icon-search-new') && !$(this).children().is('.active')) {
              $('body, html').css({ "overflow": "auto" });
            }
            
            if ($(this).children().is('.uninav-icon-login-new')) {
              $(this).children().removeClass('active');
            }
          });
        });
      }

      function createSignOut() {
        if (x.window.width() <= 940 && window.location.pathname === 'profile') {
          if (x.customDashBoard.length !== 0) {
            for (var i = x.customDashBoard.length - 1; i >= 0; i--) {
              $('.sign-out').append(
                '<a target="_blank" href="' + x.customDashBoard[i].url + '">' + 
                  x.customDashBoard[i].title +
                '</a>'
              );
            }
          }
          $('.sign-out').append('<a href="' + x.ssoPageURL + 'logout" > Log out </a>');

        } else {
          $('.sign-out').children().remove();
        }
      }

      function SearchPopUp() {
        var template = {
          search: 
            '<div id="mobile-search">' +
              '<div class="uninav-search">' +
                '<form class="search-form" autocomplete="off">' +
                  '<input class="search-field" type="search" name="search" placeholder="" mobile-input="search"/>' +
                  '<span>Search</span>' +
                  '<div class="uninav-close" data-icon="M"></div>' +
                  '<input type="submit" value="" class="uninav-icon-new uninav-icon-search search-button" />' +
                '</form>' +
              '</div>' +
            '</div>'
        };
        return template.search;
      }

      function searchFocus() {
        $(document).on('focus', 'input[mobile-input="search"]', function() {
          $(this).siblings('span').stop().addClass('active');
        });
        
        $(document).on('focusout', 'input[mobile-input="search"]', function() {
          if ($('input[mobile-input="search"]').val() === "") {
            $(this).siblings('span').stop().removeClass('active');
          } else {
            $(this).siblings('span').stop().addClass('active');
            $('input[mobile-input="search"]').val();
          }
        });
        
        $(document).on('click', '.search-form span', function() {
          $('input[mobile-input="search"]').focus();
        });
      }

      function searchClose() {
        $(document).on('touchstart click', '.uninav-close', function(e) {
          e.preventDefault();
          $('#mobile-search').remove();
          $('.uninav-icon-search-new').removeClass('active');
          $('body, html').css({ "overflow": "" });
          $('.uninav-icon-search').parent().removeClass('active');
          enableScroll();
        });
      }
       
      function checkMobile() {
        x.window.bind('resize load', function(e) {
          hideDashBoard();
          $(x.uninavTarget).hide();
          $(x.uninavTarget).removeClass('active');
          x.uninavIconTarget.removeClass('active');
          $('.uninav-icon-uninav').removeClass('active');
          
          if (e.type === 'load') {
            filterMobileIcons();
          }
          
          if (x.window.width() <= 945) {
            if (x.mobileNav === true) {
              x.mobileUninav.show();
              x.navbar.css({ "position": "absolute" });
              $(x.uninavTarget).addClass('mobile');
            } else {
              x.mobileUninav.hide();
              $('#uninav-toggle').show();
              $(x.uninavTarget).removeClass('mobile');
            }
          } else {
            x.mobileUninav.hide();
            $('#uninav-toggle').show();
            $(x.uninavTarget).removeClass('mobile');
          }
        });
      }

      function filterMobileIcons() {
        switch(window.location.pathname) {
          case '/profile':
          case '/accounts':
          case '/contact':
          case '/saved':
            hidingMenusMobile();
            break;
          default: $('.site-logo').hide();
            break;
        }

        if (window.location.pathname === "/profile" || window.location.pathname === "/accounts" || window.location.pathname === "/contact" || window.location.pathname === "/saved") {
          $('.uninav-icon-login-new').addClass('active');
        } else {
          $('.uninav-icon-login-new').removeClass('active');
        }
        
        if (x.dataDomain === 'news') {
          $('.uninav-icon-search-new').parent().parent().hide();
        }
      }

      function hidingMenusMobile() {
        x.sourceURL = $('#screenset-container').attr('data-redirect-url');
        $('.site-logo').show();
        $('.site-logo').find('a').attr('href', x.sourceURL);

        var iconClass = 'default';
        $('.site-logo #mobile-icon').attr('class', 'icon '+iconClass);
        $('.uninav-icon-login-new').parent().parent().hide();
        $('.uninav-icon-search-new').parent().parent().hide();
      }

      function markActive() {
        switch (x.domain) {
          /* Put overrides or special links here */
          case 'oneforpacman.abs-cbn.com': // TODO: override directly
            x.domain = 'sports.abs-cbn.com';
            break;
          case 'entertainment.abs-cbn.com':
          case 'entertainment2.abs-cbn.com':
            x.domain = '//entertainment.abs-cbn.com/';
            break;
          case 'news.abs-cbn.com':
            if (window.location.pathname === "/patrolph") {
              x.domain = '//news.abs-cbn.com/patrolph';
            }
            break;
          case 'corporate.abs-cbn.com':
            if (window.location.pathname === 'investorrelations') {
                options.activeDomain = '//corporate.abs-cbn.com/investorrelations';
            } else if(window.location.pathname === 'stellar') {
                options.activeDomain = '//corporate.abs-cbn.com/stellar';
            } else {
                options.activeDomain = '//corporate.abs-cbn.com/investorrelations';
            }
            break;
          case 'ktx.abs-cbn.com':
            x.domain = "//ktx.abs-cbn.com/?utm_source=uni-nav&utm_medium=link&utm_content=ktx";
            break;
          default:
            x.domain = '//' + x.domain + '/';
            break;
          }

        // $('a[href="' + x.domain + '"]').removeAttr('href').addClass('active');
      }

      function getSourceURL() {
        $(document).on('click', '.uninav-mobile-parent', function() {
          localStorage.setItem('sourceURL', window.location.href);
        });
      }

      function disableScroll() {
        document.ontouchmove = function(event) {
          event.preventDefault();
        };
      }

      function enableScroll() {
        document.ontouchmove = function(event) {
          return true;
        };
      }

      function hideDashBoard() {
        $('.gigya-expanded-profile').remove();
        $('.sso-uninav-login').removeClass('active');
      }

      function temporaryPopUp() {
        var template =
          '<section class="lightbox">' +
            '<div class="content">' + 
              '<div class="header">' +
                '<p>' +
                  '<a href="#">' +
                    '<span><img src="//assets.abs-cbn.com/universalnav/img/logo-abs-cbn.png" alt="ABS-CBN Corporation" class="kapamilya-accounts-bar"></span>' +
                    '<span class=' + getURL.getWebsiteName().toLowerCase().split(' ').join('') + '>' + getURL.getWebsiteName() + '</span>' +
                  '</a>' +
                '</p>' +
              '</div>' +
              '<div class="body">' +
                '<h1>Introducing your one access to everything Kapamilya!</h1>' +
                '<p class="sub">Now, you can use one login to access your favorite Kapamilya websites.</p>' +
                '<div><img src="//assets.abs-cbn.com/universalnav/img/kapamilya-account.png" alt="Kapamilya Account"></div>' +
                '<ul class="hub-container">' +
                  '<li><span class="hub hub-onemusic"></span></li>' +
                  '<li><span class="hub starcinema"></span></li>' +
                  '<li><span class="hub starcreatives"></span></li>' +
                  '<li><span class="hub hub-mmk"></span></li>' +
                  '<br>' +
                  '<li><span class="hub hub-entertainment"></span></li>' +
                  '<li><span class="hub hub-news"></span></li>' +
                  '<li><span class="hub hub-sports"></span></li>' +
                  '<li><span class="hub hub-lifestyle"></span></li>' +
                  // '<li><a href="#"><span class="hub hub-sports"></span></a></li>' +
                  // '<li><a href="#"><span class="hub hub-lifestyle"></span></a></li>' +
                  // '<li><a href="#"><span class="hub hub-sky"></span></a></li>' +
                  // '<li><a href="#"><span class="hub hub-sky-on-demand"></span></a></li>' +
                  // '<li><a href="#"><span class="hub hub-tfc"></span></a></li>' +
                '</ul>' +
                '<p class="text-blue">We will now bring you to kapamilyaaccounts.abs-cbn.com</p>' +
                '<p class="btn btn-proceed"><a href="' + x.ssoPageURL + '">Proceed</a></p>' +
                '<p class="btn btn-cancel"><a href="#" class="btn-cancel">Cancel</a></p>' +
              '</div>'+
            '</div>' +
          '</section>';
        x.body.append(template);
        // $('.lightbox').css({"position":"absolute", "height": $('body').height() + "px"});
      }

      function closeTemporaryPopUp() {
        $(document).on('click', '.btn-cancel', function() {
          $('.lightbox').hide();
        });
      }
      
      function logoutDashBoard() {
        $(document).on('click', '.dashboard-logout', function() {
          gigya.accounts.logout();
        });
      }

      if (x.type === "default") {
        x.navbar.attr('aria-expanded', 'false');
        x.navbarCollapse.attr('aria-expanded', 'false');
        createUniNav();
        mobileAccordion();
        headerNavResize();
        pageScroll();
        Toggler();
        checkMobile();
        searchFocus();
        toggleMobile();
        searchClose();
        // markActive();
        getSourceURL();
        createSignOut();
        temporaryPopUp();
        closeTemporaryPopUp();
        logoutDashBoard();
      } else {
        // console.log('custom');
      }
    }

    var getURL = {
      getWebsiteName: function() {
        var webSiteName = "";
        if (getURL.getURLSites("news") >= 0) {
          webSiteName = "NEWS";
        } else if (getURL.getURLSites("entertainment") >= 0) {
          webSiteName = "ENTERTAINMENT";
        } else if (getURL.getURLSites("mmk") >= 0) {
          webSiteName = "MMK";
        } else if (getURL.getURLSites("bmpm") >= 0) {
          webSiteName = "BMPM";
        } else if (getURL.getURLSites("onemusic") >= 0) {
          webSiteName = "ONE MUSIC";
        } else if (getURL.getURLSites("starcinema") >= 0) {
          webSiteName = "STARCINEMA";
        } else if (getURL.getURLSites("sinehub") >= 0) {
          webSiteName = "SINEHUB";
        } else if (getURL.getURLSites("iwantv") >= 0) {
          webSiteName = "I WANT TV";
        } else if (getURL.getURLSites("sports") >= 0) {
          webSiteName = "SPORTS";
        } else if (getURL.getURLSites("lifestyle") >= 0) {
          webSiteName = "LIFESTYLE";
        } else if (getURL.getURLSites("sky") >= 0) {
          webSiteName = "SKY.";
        } else if (getURL.getURLSites("skyondemand") >= 0) {
          webSiteName = "SKY ON DEMAND";
        } else if (getURL.getURLSites("KapamilyaThankyou") >= 0) {
          webSiteName = "Kapamilya Thank you";
        } else if (getURL.getURLSites("starcinema2") >= 0) {
          webSiteName = "STARCINEMA 2";
        } else if (getURL.getURLSites("tfc") >= 0) {
          webSiteName = "TFV";
        } else {
          webSiteName = "KAPAMILYA";
        }
        return webSiteName;  
      },

      getIconClassByReferrrer: function() {
        referrer = getURL.getCookie("Referrer").split('?')[0];

        if (getURL.getUrlParameter('referrer')) {
          referrer = getURL.getUrlParameter("referrer");
        }

        if (referrer.indexOf("news") >= 0) {
          iconClass = "news";
        } else if (referrer.indexOf("entertainment") >= 0) {
          iconClass = "entertainment";
        } else if (referrer.indexOf("mmk") >= 0) {
          iconClass = "mmk";
        } else if (referrer.indexOf("bmpm") >= 0) {
          iconClass = "default";
        } else if (referrer.indexOf("onemusic") >= 0) {
          iconClass = "onemusic";
        } else if (referrer.indexOf("starcinema") >= 0) {
          iconClass = "default";
        } else if (referrer.indexOf("starcinema2") >= 0) {
          iconClass = "default";
        } else if (referrer.indexOf("sinehub") >= 0) {
          iconClass = "default";
        } else if (referrer.indexOf("KapamilyaThankyou") >= 0) {
          iconClass = "default";
        } else if (referrer.indexOf("iwantv") >= 0) {
          iconClass = "iwantv";
        } else if (referrer.indexOf("tfc") >= 0) {
          iconClass = "tfctv";
        } else {
          iconClass = "default";
        }
        return iconClass;  
      },

      getURLSites: function(sName) {
        if (getURL.getUrlParameter('referrer')) {
          return getURL.getUrlParameter("referrer").indexOf(sName);
        }

        return window.location.href.split('?')[0].indexOf(sName);
      },

      getApiReferrerKey: function(useForApi, data) {
        var referrer = getURL.getCookie("Referrer");
    
        if (getURL.getUrlParameter('referrer')) {
          referrer = getURL.getUrlParameter("referrer");
        }

        var apiKeys = "";
        if (referrer !== "") {
          referrer = referrer.split("?")[0];

          if (referrer.indexOf("news") >= 0) {
            apiKeys = useForApi ? data.news : "NEWS";
          } else if (referrer.indexOf("kapamilyaaccounts") >= 0) {
            apiKeys = useForApi ? data.entertainment : "ACCOUNTS";
          } else if (referrer.indexOf("bmpm") >= 0) {
            apiKeys = useForApi ? data.bmpm : "BMPM";
          } else if (referrer.indexOf("entertainment") >= 0) {
            apiKeys = useForApi ? data.entertainment : "ENTERTAINMENT";
          } else if (referrer.indexOf("iwantv") >= 0) {
            apiKeys = useForApi ? data.iwantv : "I WANT TV";
          } else if (referrer.indexOf("KapamilyaThankyou") >= 0) {
            apiKeys = useForApi ? data.kapamilyathankyou : "Kapamilya Thank you";
          } else if (referrer.indexOf("thankyou") >= 0) {
            apiKeys = useForApi ? data.kapamilyathankyou : "Kapamilya Thank you";
          } else if (referrer.indexOf("mmk") >= 0) {
            apiKeys = useForApi ? data.mmk : "MMK";
          } else if (referrer.indexOf("sinehub") >= 0) {
            apiKeys = useForApi ? data.sinehub : "SINEHUB";
          } else if (referrer.indexOf("starcinema") >= 0) {
            apiKeys = useForApi ? data.starcinema : "STAR CINEMA";
          } else if (referrer.indexOf("starcinema2") >= 0) {
            apiKeys = useForApi ? data.starcinema : "STAR CINEMA";
          } else if (referrer.indexOf("onemusic") >= 0) {
            apiKeys = useForApi ? data.onemusic : "ONE MUSIC";
          } else if (referrer.indexOf("hbo") >= 0) {
            apiKeys = useForApi ? data.hbogo : "HBO GO";
          } else if (referrer.indexOf("tfc") >= 0) {
            apiKeys = useForApi ? data.tfctv : "TFC";
          } else if (referrer.indexOf("starcreative") >= 0) {
            apiKeys = useForApi ? data.starcreative : "STAR CREATIVE";
          } else if (referrer.indexOf("sports") >= 0) {
            apiKeys = useForApi ? data.sports : "SPORTS";
          } else if (referrer.indexOf("lifestyle") >= 0) {
            apiKeys = useForApi ? data.lifestyle : "LIFESTYLE";
          } else if (referrer.indexOf("kapamilyathankyou") >= 0 || referrer.indexOf("129.144.41") >= 0) {
            apiKeys = useForApi ? data.kapamilyathankyou : "KAPAMILYA THANK YOU";
          } else if (referrer.indexOf("cablechannels") >= 0) {
            apiKeys = useForApi ? data.cablechannels : "CABLE CHANNELS";
          } else if (referrer.indexOf("ott") >= 0) {
            apiKeys = useForApi ? data.ott : "OTT";
          } else if (referrer.indexOf("ktx") >= 0) {
            apiKeys = useForApi ? data.ktx : "KTX";
          } else if (referrer.indexOf("noink") >= 0 || referrer.indexOf("maniladps.demo.devinition.com") >= 0) {
            apiKeys = useForApi ? data.noink : "NOINK";
          } else if (referrer.indexOf("push") >= 0) {
            apiKeys = useForApi ? data.push : "PUSH";
          } else if (referrer.indexOf("dev-experiment") >= 0) {
            apiKeys = useForApi ? data.test : "Dev Experiment";
          } else if (referrer.indexOf("localhost") >= 0) {
            apiKeys = useForApi ? data.entertainment : "Localhost";
          } else if (referrer.indexOf("abscbnmobile") >= 0) {
            apiKeys = useForApi ? data.abscbnmobile : "ABS-CBN MOBILE";
          } else if (referrer.indexOf("kforce") >= 0) {
            apiKeys = useForApi ? data.abscbnmobile : "ABS-CBN MOBILE";
          } else if (referrer.indexOf("mysky") >= 0) {
            apiKeys = useForApi ? data.mysky : "MY SKY";
          } else if (referrer.indexOf("myxph") >= 0) {
            apiKeys = useForApi ? data.myxph : "MYXPH";
          } else if (referrer.indexOf("mor") >= 0) {
            apiKeys = useForApi ? data.mor : "MOR";
          }

        } else {
          apiKeys = useForApi ? data.entertainment : "KAPAMILYA"; // Just For Localhost
        }

        return apiKeys;
      },

      getCookie: function(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i = 0; i < ca.length; i++) {
          var c = ca[i];
          while (c.charAt(0) === ' ') {
            c = c.substring(1);
          }
          if (c.indexOf(name) === 0) {
            return c.substring(name.length,c.length);
          }
        }
        return "";
      },
      getUrlParameter: function(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
          sParameterName = sURLVariables[i].split('=');

          if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
          }
        }
      }
    };

    function setPageLocation(url) {
      // window.location = destination;
      if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)){
        var referLink = document.createElement('a');
        referLink.href = url;
        document.body.appendChild(referLink);
        referLink.click();
      } else {
        location.href = url;
      }
    }

    function uninavSSO() {
      $.getJSON(store.domainKeys, function(data) {
        // body...
      }).done(function(data) {
        switch (window.location.hostname) {
          case '1eb5e7dfe0b6437ca7449539cd5412c6.cloudapp.net':
          case 'devnews.abs-cbn.com':
          case 'testnews.abs-cbn.com':
          case 'stagingnews.abs-cbn.com':
          case 'staging-dev.abs-cbn.com':
            x.ssoApikey = data.news;
            break;
          case 'dev.mmk.abs-cbn.com':
          case 'conceptmachine.net':
          case 'www.conceptmachine.net':
            x.ssoApikey = data.mmk;
            break;
          case 'dev.entertainment.abs-cbn.com':
          case 'test.entertainment.abs-cbn.com':
          case 'dev.entertainment.abs-cbn.com':
          case 'staging.entertainment.abs-cbn.com':
            x.ssoApikey = data.entertainment;
            break;
          case 'staging-dev2.abs-cbn.com':
            x.ssoApikey = data.abscbnmobile;
            break;
          case 'dev.bmpm.abs-cbn.com':
          case 'bmpm-staging.kudosweb.com':
            x.ssoApikey = data.bmpm;
            break;
          case 'test-starcinema.abs-cbn.com':
          case 'starcinema2.abs-cbn.com':
            x.ssoApikey = data.starcinema;
            break;
          case 'hbodev.evergent.com':
            x.ssoApikey = data.hbogo;
            break;
          case '10.0.7.167':
          case '129.144.41.26':
          case '129.144.41.33':
          case '129.144.41.20':
            x.ssoApikey = data.kapamilyathankyou;
            break;
          case 'tfc.tv':
          case 'beta.tfc.tv':
            x.ssoApikey = data.tfctv;
            break;
          case 'starcreatives.abs-cbn.com':
            x.ssoApikey = data.starcreative;
            break;
          case 'test-push.abs-cbn.com':
            x.ssoApikey = data.push;
            break;
          case 'localhost':
          case 'qa-kapamilyaaccounts.abs-cbn.com':
          case 'dev-kapamilyaaccounts.abs-cbn.com':
          case 'staging-kapamilyaaccounts.abs-cbn.com':
            if (window.location.pathname === "/sms-login" || window.location.pathname === "/sms-sign-up" || window.location.pathname === "/sms-login-no-reset") {
              x.ssoApikey = data.otp;
              // remove this after the OTP setup for each dev child site is finished
              if (getURL.getUrlParameter('referrer') && getURL.getUrlParameter('referrer').indexOf('entertainment') >= 0) {
                x.ssoApikey = data.entertainment;
              } else if (getURL.getCookie("Referrer")) {
                if (getURL.getCookie("Referrer").indexOf('lifestyle') >= 0) {
                  x.ssoApikey = data.lifestyle;
                } else if (getURL.getCookie("Referrer").indexOf('sports') >= 0) {
                  x.ssoApikey = data.sports;
                } else if (getURL.getCookie("Referrer").indexOf('bmpm') >= 0) {
                  x.ssoApikey = data.bmpm;
                } else if (getURL.getCookie("Referrer").indexOf('myxph') >= 0) {
                  x.ssoApikey = data.myxph;
                } else if (getURL.getCookie("Referrer").split('?')[0].indexOf('iwantv') >= 0 || (getURL.getUrlParameter('referrer') && getURL.getUrlParameter('referrer').indexOf('iwantv') >= 0)) {
                  x.ssoApikey = data.iwantv;
                } else if (getURL.getCookie("Referrer").split('?')[0].indexOf('dev-experiment') >= 0 || (getURL.getUrlParameter('referrer') && getURL.getUrlParameter('referrer').indexOf('dev-experiment') >= 0)) {
                  x.ssoApikey = data.test;
                }
              }
              // } else if ((getURL.getCookie("Referrer") && getURL.getCookie("Referrer").split('?')[0].indexOf('iwantv')) || (getURL.getUrlParameter('referrer') && getURL.getUrlParameter('referrer').indexOf('iwantv') >= 0)) {
              //   x.ssoApikey = data.iwantv;
              // }
            } else {
              x.ssoApikey = getURL.getApiReferrerKey(true, data);
            }
            
            break;
        }

        console.log("API KEY : ", x.ssoApikey);

        var gigyaConf = "";
        if (window.__gigyaConf === undefined) {
          gigyaConf = '<script type="text/javascript">window.__gigyaConf = {"enableSSOToken": true}</script>';
        }


        var timestamp = new Date().getTime();
        var scriptSource = window.location.protocol === "https:" ? "https://cdns.gigya.com/JS/gigya.js" : "http://cdn.gigya.com/JS/gigya.js";

        gigyaConf += '<script type="text/javascript" src="'+scriptSource+'?apikey=' + x.ssoApikey + '&timestamp='+timestamp+'"></script>';        
        $('head').append(gigyaConf);


        if (window.location.hostname.indexOf("kapamilyaaccounts.abs-cbn.com") === -1 && window.location.hostname !== "localhost") {
          $('head').append(
            '<script type="text/javascript" src="//assets.abs-cbn.com/sso/sso-behavior.js"></script>'
          );        
        }
        ssoChecker();
      });

      function logoutFromComment() {
        if (window.location.hostname.indexOf('kapamilyaaccounts.abs-cbn.com') < 0) {
          gigya.accounts.addEventHandlers({
            onLogout: function() {
              setPageLocation(x.ssoPageURL + '/logout');
            }
          });
        }
      }

      function ssoChecker() {
        $("script[src*='gigya.com/JS/gigya.js?apikey=" + x.ssoApikey + "']").load(function() {
          ssoApiCall();
        });
      }
      
      function ssoApiCall() {
        gigya.accounts.getAccountInfo({
          include: "loginIDs,profile,data",
          callback: function(res) {
            if (res.status === "FAIL") {
              x.ssoTarget.parent().parent().addClass('sso-not-active');
              /*jshint scripturl:true*/
              x.ssoTarget.parent().attr('href', 'javascript:void(0);');
              $('.uninav-icon-login-new ').parent().parent().addClass('sso-not-active-mobile');
              /*jshint scripturl:true*/
              $('.uninav-icon-login-new ').parent().attr('href', 'javascript:void(0);');
              templates.mobileDashBoard("out");
              mobileLogInDashBoard();
              $(".sso-uninav-login").show();
            } else {
              x.ssoTarget.parent().parent().addClass('sso-active');
              /*jshint scripturl:true*/
              x.ssoTarget.parent().attr('href', 'javascript:void(0);');

              ssoDashBoard(res);
              ssoThumbnail(res);
              characterElipsis(res);
              if (x.customDashBoard.length === 0) {
                templates.mobileDashBoard("in");
                mobileLogOutDashBoard();
              } else {
                mobileCustomDashBoard();
              }
              $(".sso-uninav-login").show();
              logoutFromComment();
            }

            // readingList.setAccountInformation(res.status);
          }
        });
        x.body.append(templates.markup);
      }

      var readingList = {
        data: [],
        dataTitle: [],
        page: 0,

        getDateNow: function() {
          var date = new Date();
          var year = date.getFullYear();
          var day = date.getDate();
          var month = date.getMonth();

          switch (month) {
            case 0: month = "January"; break;
            case 1: month = "Febuary"; break;
            case 2: month = "March"; break;
            case 3: month = "April"; break;
            case 4: month = "May"; break;
            case 5: month = "June"; break;
            case 6: month = "July"; break;
            case 7: month = "August"; break;
            case 8: month = "September"; break;
            case 9: month = "October"; break;
            case 10: month = "November"; break;
            case 11: month = "December"; break;
          }

          return month + " " + day + ", " + year;
        },

        createTempBubblePopup: function(str, el, closeTime) {

          var _container = el.parent();
          $('.bookmark-saved').remove();
          var bubblePopup = $('<div class="bookmark-saved">'+str+'</div>').fadeIn("fast").appendTo(_container);
          
          if (!closeTime) {
            closeTime = closeTime;
          } else {
            closeTime = 3000;
          }

          var timeout = setInterval(function() {
            bubblePopup.fadeOut("slow");
            bubblePopup.remove();
            clearInterval(timeout);
          }, closeTime);
        },

        setAccountInformation: function(status) {
          // get all reading list first.
          gigya.accounts.getAccountInfo({ callback: readingList.getAccountInfoResponse });
          
          $(document).on('click', '.bookmarked', function(e) {
            e.preventDefault();
            var _this = $(this);
            var article = _this.parents('article');

            // set variable for unique article id
            var articleID = new Date().getTime();
            var dateSave = readingList.getDateNow();
            var title = article.find('meta[itemprop="headline"]').attr("content"); //$('meta[property="og:title"]').attr("content");
            var shortDescription = article.find('meta[itemprop="description"]').attr("content"); //$('meta[property="og:description"]').attr("content");
            var thumbnail = article.find('meta[itemprop="thumbnailUrl"]').attr("content"); //$('meta[property="og:image"]').attr("content");
            var siteSrc = article.find('meta[itemprop="url"]').attr("content"); //$('meta[property="og:url"]').attr("content");
            var siteName = $('meta[property="og:site_name"]').attr("content");

            // Note: check if user is login before do anything.
            if (status === "FAIL") {
              var message = '<a href="'+x.ssoPageURL+'"><strong>Login</strong> to your Kapamilya Account to save this article and read it later.</a>';
              readingList.createTempBubblePopup(message, _this, 6000);
              return;
            }
            
            // Check if article already saved or bookmarked
            if(!_this.hasClass("article-saved")) {
              // prepare all data needed to set the accounts info
              var temp = {
                articleId: articleID.toString(),
                title: title,
                shortDesc: shortDescription,
                saveDate: dateSave,
                thumbnail: thumbnail,
                urlLink: siteSrc,
                siteSrc: siteName,
                readed: false
              };

              readingList.data.push(temp);

              var params = { 
                data: {
                  readList: readingList.data
                },
                callback: function(response) {
                  if (response.errorCode === 0) { 
                    readingList.data = [];
                    gigya.accounts.getAccountInfo({ callback: readingList.getAccountInfoResponse });
                    _this.addClass("article-saved");
                    // readingList.createTempBubblePopup("Saved.", _this);
                  } else {
                    readingList.createTempBubblePopup("An error has occurred!<br>Error details: " + response.errorMessage + "<br>Error code: " + response.errorCode + "<br>In method: " + response.operation, 5000);
                  }
                }
              };

              gigya.accounts.setAccountInfo(params);
            }
          });

          $(document).on("click", ".clickable-link", function() {
            var id = $(this).data("articleid");
            var link = $(this).data("urllink");

            $.each(readingList.data, function(i, val) {
              if (val.articleId === id.toString()) {
                val.readed = true;
              }
            });
            
            var params = { 
              data: {
                readList: readingList.data
              },
              callback: function(response) {
                if (response.errorCode === 0) {
                  window.location.href = link;
                } else {
                  alert("An error has occurred!" + '\n' +
                        "Error details: " + response.errorMessage + '\n' + 
                        "Error code: " + response.errorCode + '\n' +               
                        "In method: " + response.operation);
                }
              }
            };

            gigya.accounts.setAccountInfo(params);
          });
        },

        articleAlreadyAdd: function(a, b) {
          return a.indexOf(b) >= 0;
        },

        getAccountInfoResponse: function(response) {
          if (response.data && response.data.readList) {
            if ( response.errorCode === 0 ) {           
              var readList = response.data.readList;

              $.each(readList, function(i, val) {
                readingList.data.push(val);
                readingList.dataTitle.push(val.title);
              });
            }
            var article = $('.bookmarked').parents('article');
            // var title = $('meta[property="og:title"]').attr("content");
            $.each(article, function(i, val) {
              var artContainer = $(val);
              var title = artContainer.find('meta[itemprop="headline"]').attr("content");
              var bookmarkedContainer = artContainer.find(".bookmarked-saved-container");
              if (readingList.articleAlreadyAdd(readingList.dataTitle, title)) {
                bookmarkedContainer.html('<button class="bookmarked article-saved"><span>Saved</span></button>');
              } else {
                bookmarkedContainer.html('<button class="bookmarked"><span>Save</span></button>');
              }
            });
          }
        },
      };

      var templates = {
        markup: "",
        mobileDashBoard: function(status) {
          if(status === "out") {
            templates.markup += '<div class="mobile-dashboard">';
              templates.markup += '<ul>';
                templates.markup += '<li>';
                  templates.markup += '<a href="' + x.ssoPageURL + 'login" onclick="gigya.accounts.login();">Login</a>';
                templates.markup += '</li>';
              templates.markup += '<ul>';
            templates.markup += '</div>';  
          } else {
            templates.markup += '<div class="mobile-dashboard">';
              templates.markup += '<ul>';
                templates.markup += '<li>';
                  templates.markup += '<a href="' + x.ssoPageURL + 'profile">My Kapamilya Account</a>';
                  templates.markup += '<a class="dashboard-logout" href="javascript:void(0);">Log out</a>';
                templates.markup += '</li>';
              templates.markup += '<ul>';
            templates.markup += '</div>';
          }
        },
      };

      function mobileLogInDashBoard() {
        var template = {
          mobileDashBoard:
            '<div class="mobile-dashboard">' +
                '<div>' +
                  '<ul>' +
                    '<li>' +
                      '<a href="' + x.ssoPageURL + 'login" onclick="gigya.accounts.login();">' +
                        'Login' +
                      '</a>' +
                    '</li>' +
                  '<ul>' +
                '</div>' +
            '</div>'
        };

        x.body.append(template.mobileDashBoard);
      }

      function mobileLogOutDashBoard() {
        var template = {
          mobileDashBoard:
            '<div class="mobile-dashboard">' +
                '<div>' +
                  '<ul>' +
                    '<li>' +
                      '<a href="' + x.ssoPageURL + 'profile" >' +
                        'My Kapamilya Account' +
                      '</a>' +
                    '</li>' +
                    '<li>' +
                      '<a class="dashboard-logout" href="javascript:void(0);">' +
                        'Log out' +
                      '</a>' +
                    '</li>' +
                  '<ul>' +
                '</div>' +
            '</div>',
          mobileDashBoard1:
            '<div class="mobile-dashboard">' +
                '<div>' +
                  '<ul>' +
                    '<li>' +
                      '<a class="dashboard-logout" href="' + x.ssoPageURL + 'logout">' +
                        'Log out' +
                      '</a>' +
                    '</li>' +
                  '<ul>' +
                '</div>' +
            '</div>',
        };
        if (window.location.hostname === "staging-kapamilyaaccounts.abs-cbn.com") {
          x.body.append(template.mobileDashBoard1);
        } else {
          x.body.append(template.mobileDashBoard);
        }
      }

      function mobileCustomDashBoard() {
        mobileLogOutDashBoard();
        x.body.append(templates.markup);
        $('.mobile-dashboard ul ul ').remove();
        $('.mobile-dashboard ul li').remove();
        var template = "";

        $.each(x.customDashBoard, function(i, obj) {
          template += '<li>';
            template += '<a target="_blank" href="' + x.customDashBoard[i].url + '">';
              template += x.customDashBoard[i].title;
            template += '</a>';
          template += '</li>';
        });

        $('.mobile-dashboard ul').append(template);
        $('.mobile-dashboard ul').append('<li><a href="' + x.ssoPageURL + 'profile" > My Kapamilya Account </a></li> <li><a class="dashboard-logout" href="' + x.ssoPageURL + 'logout" > Log out </a></li>');
      }

      function ssoTemplate(res) {
        x.kapamilyaImage = res.profile.thumbnailURL;
        x.kapamilyaImagemobile = res.profile.thumbnailURL;
        x.kapamilyaImageDashboard = res.profile.photoURL;
        
        if (res.profile.thumbnailURL === undefined) {
          x.kapamilyaImage = '//assets.abs-cbn.com/universalnav/img/header-icons.png';
          x.kapamilyaImagemobile = "//assets.abs-cbn.com/universalnav/img/sprite-logo.png";
        }
        
        if (res.profile.photoURL === undefined) {
          x.kapamilyaImageDashboard = '//assets.abs-cbn.com/universalnav/img/no-image-logo.png';
        }

        var xName = res.loginIDs.username === undefined ? "" : res.loginIDs.username;
        var firstName = res.profile.firstName === undefined ? "" : res.profile.firstName;
        var lastName = res.profile.lastName === undefined ? "" : res.profile.lastName;

        var template = {
          image: x.kapamilyaImage,
          imagemobile: x.kapamilyaImagemobile,
          kapamilyaName: res.loginIDs.username,
          userName: 
            '<div class="sso-uninav-username">' +
              '<p>' +
                xName +
              '</p>' +
            '</div>',
          defaultDashBoard: 
            '<div class="gigya-expanded-profile">' +
              '<div class="gigya-expanded-profile-account">' +
                '<img class="gigya-expanded-photo" src="' + x.kapamilyaImageDashboard + '"/ >' +
                '<div class="kapamilya-info">' +
                  '<p class="kapamilya-name" title="' + xName + '" >' +
                    xName +
                  '</p>' +
                  '<p class="full-name">' +
                    firstName + ' ' + lastName +
                  '</p>' +
                '</div>' +
                '<div>' +
                  '<ul>' +
                    '<li>' +
                      '<a href="' + x.ssoPageURL + 'profile"> My Kapamilya Account </a>' +
                    '</li>' +
                    '<li>' +
                      '<a href="javascript:void(0);" class="dashboard-logout"> Log out </a>' +
                    '</li>' +
                  '</ul>' +
                '</div>' +
                '<div>' +
            '</div>'
        };

        return template;
      }

      function ssoThumbnail(res) {
        $('.uninav-icon-login-new').addClass('uninav-mobile-sso');
        $('.uninav-icon-login-new').parent().addClass('uninav-mobile-parent');
        $('.uninav-icon-login-new').children('p').html(res.loginIDs.username);

        //For Desktop
        if (ssoTemplate(res).image === "//assets.abs-cbn.com/universalnav/img/header-icons.png") {
          x.ssoTarget.css({
            "background-image": "url(" + ssoTemplate(res).image + ")",
            "background-position": "-115px -2px"
          });
        } else {
          x.ssoTarget.css({
            "background-image": "url(" + ssoTemplate(res).image + ")",
            "background-position": "0 0"
          });
        }

        x.ssoTarget.addClass("active");
        x.ssoTarget.parent().append(ssoTemplate(res).userName);
        
        if (ssoTemplate(res).imagemobile === "//assets.abs-cbn.com/universalnav/img/sprite-logo.png") {
          $('.uninav-icon-login-new').addClass('no-image');
        } else {
          $('.uninav-icon-login-new').css({
            "background-image": "url(" + ssoTemplate(res).image + ")"
          });
        }
      }

      function ssoDashBoard(res) {
        $('.sso-active a').on('click', function() {
          if (!$(this).is('.active')) {
            x.header.append(ssoTemplate(res).defaultDashBoard);
            dashBoardPosition();
            if (x.customDashBoard.length === 0) {
              // use default
            } else {
              customSsoLinks();
            }
          } else {
            $('.gigya-expanded-profile').remove();
          }
        });
      }

      function dashBoardPosition() {
        var top = $('.sso-active').position().top;
        var left = $('.sso-active').position().left;

        $('.gigya-expanded-profile').css({
          'top': $('.header-bar-inner').height(),
          'left': left - $('.gigya-expanded-profile').width() + $('.uninav-toggle').width() - 10
        });
      }

      function customSsoLinks() {
        $('.gigya-expanded-profile ul li').remove();
        var template = "";

        $.each(x.customDashBoard, function(i, obj) {
          template += '<li>';
            template += '<a target="_blank" href="' + x.customDashBoard[i].url + '">';
              template += x.customDashBoard[i].title;
            template += '</a>';
          template += '</li>';
        });

        $('.gigya-expanded-profile ul').append(template);
        $('.gigya-expanded-profile ul').append('<li><a href="' + x.ssoPageURL + 'profile" > My Kapamilya Account </a></li> <li><a class="dashboard-logout" href="' + x.ssoPageURL + 'logout" > Log out </a></li>');
      
      }

      function characterElipsis(res) {
        if ($('.sso-uninav-username').length !== 0) {
          var nameLength = $('.sso-uninav-username').children().html().length,
          maxLength = 10,
          newname = '';
          
          if (nameLength > maxLength) {
            newname = $('.sso-uninav-username').children().html().substr(0, maxLength) + '...';
            $('.sso-uninav-username').children().html(newname);
            $('.sso-uninav-username').children().attr('title', res.loginIDs.username);
          }
        } else {
          // do nothing 
        }
      }
    }

    function initialize() {
      if (x.sso) {uninavSSO();}
      createHeader();
    }
    initialize();
  };
}(jQuery));
