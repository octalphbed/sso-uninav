var gulp = require('gulp'),
  plumber = require('gulp-plumber'),
  rename = require("gulp-rename"),
  autoprefixer = require('gulp-autoprefixer'),
  jshint = require('gulp-jshint'),
  uglify = require('gulp-uglify'),
  sass = require('gulp-sass'),
  browserSync = require('browser-sync'),
  concat = require('gulp-concat'),
  gutil = require('gulp-util');

var versions = [
  {id: 'v1',srcCSS: 'src/v1/**/*.scss',srcJS: 'src/v1/uninav-src.js',distCSS: 'dist/uninav.css',distJS: 'dist/uninav.js'},
  {id: 'v2',srcCSS: 'src/v2/**/*.scss',srcJS: 'src/v2/uninav-src.js',distCSS: 'dist/uninav-v2.css',distJS: 'dist/uninav-v2.js'},
  {id: 'v3',srcCSS: 'src/v3/**/*.scss',srcJS: 'src/v3/uninav-v3.js',distCSS: 'dist/uninav-v3.css',distJS: 'dist/uninav-v3.js'},
  {id: 'v4',srcCSS: 'src/v4/**/*.scss',srcJS: 'src/v4/uninav-v4.js',distCSS: '≈≈≈',distJS: 'dist/staging-uninav-v4.2.0.js'}
];

// Tasks
gulp.task('browser-sync', function() {
  browserSync({
    server: './',
    notify: false,
    ui: false,
    port: 44381
  });
});

gulp.task('bs-reload', function () {
  browserSync.reload();
});

for (var i = 0; i < versions.length; i++) {
  (function(i) {
    gulp.task('styles' + versions[i].id, function() {
      gulp.src(versions[i].srcCSS)
        .pipe(plumber({
          errorHandler: function (error) {
            console.log(error.message);
            this.emit('end');
          }
        }))
        .pipe(sass({
          outputStyle: 'compressed'
        }))
        .pipe(autoprefixer('last 4 versions'))
        // .pipe(rename(versions[i].distCSS))
        .pipe(gulp.dest('./dist/'))
        .pipe(browserSync.stream());
    });

    gulp.task('scripts' + versions[i].id, function() {
      gulp.src([versions[i].srcJS, 'src/uninav-config.js', 'src/site-takeover.js','src/addons.js','src/adblock-config.js', 'src/cookie-config.js', 'src/privacy-config.js'])
        .pipe(plumber({
          errorHandler: function (error) {
            console.log(error.message);
            this.emit('end');
          }
        }))
        .pipe(jshint())
        .pipe(jshint.reporter('default'))
        .pipe(uglify({
          preserveComments: 'some'
        }))
        .pipe(concat(versions[i].distJS))
        .pipe(rename(versions[i].distJS))
        .pipe(gulp.dest('./'))
        .pipe(browserSync.stream());
    });
  })(i);
}

var defaultTasks = (function() {
  var ret = [];
  for (var i = 0; i < versions.length; i++) {
    ret.push('styles' + versions[i].id);
    ret.push('scripts' + versions[i].id);
  }
  return ret;
});

gulp.task('build', defaultTasks());

gulp.task('default', ['build', 'browser-sync'], function() {
  for (var i = 0; i < versions.length; i++) {
    gulp.watch(versions[i].srcCSS, ['styles' + versions[i].id]);
    gulp.watch([versions[i].srcJS, 'src/uninav-config.js', 'src/site-takeover.js', 'src/adblock-config.js', 'src/cookie-config.js', 'src/addons.js', 'src/privacy-config.js'], ['scripts' + versions[i].id]);
  }
  gulp.watch(['**/*.html', '!node_modules'], ['bs-reload']);
});

