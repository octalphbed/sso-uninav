var cookieLaw = {};
// Initialize
cookieLaw.CookieLawInit = function (uCookie) {
    if (localStorage.getItem("uninavIsAgreeCookieLaw") === null && (cookieLaw.getCookie('uninavIsAgreeCookieLaw') === false || cookieLaw.getCookie('uninavIsAgreeCookieLaw') === null || cookieLaw.getCookie('uninavIsAgreeCookieLaw') === '')) {
        cookieLaw.createCookieLaw(uCookie);
    }else{
        if(localStorage.getItem("uninavIsAgreeCookieLaw") === 'true'){
            var dateToday = new Date(new Date().setFullYear(new Date().getFullYear()));
            var cookieLawExpiration = localStorage.getItem("uninavCookieLawExpiration");
            var parsedCLE = new Date(cookieLawExpiration);
            if(dateToday >= parsedCLE){
                localStorage.removeItem("uninavCookieLawExpiration");
                localStorage.removeItem("uninavIsAgreeCookieLaw");
                cookieLaw.createCookieLaw(uCookie);
            }
        }
    }
};

// Get Cookie Name
cookieLaw.getCookie = function (name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1, c.length);
        }
        if (c.indexOf(nameEQ) === 0) {
            return c.substring(nameEQ.length, c.length);
        }
    }
    return null;
};

// MarkUp
cookieLaw.createCookieLaw = function (uCookie) {

    var headerImage = uCookie.headerImage; //? uCookie.headerImage : '//assets.abs-cbn.com/universalnav/img/cookie-law-desktop.png';
    var title = uCookie.title;  //? uCookie.title : 'Welcome, Kapamilya!'; 
    var logo = uCookie.logo; // ? uCookie.logo : '//assets.abs-cbn.com/universalnav/img/logo-abs-cbn-black.png';
    var contentCopy = uCookie.contentCopy ? uCookie.contentCopy : 'We use cookies to improve your browsing experience.<br>Continuing to use this site means you agree to our use of cookies.';

    var markup = '';
    if (headerImage !== 'none') {
        var uheaderImage = uCookie.headerImage ? uCookie.headerImage : '//assets.abs-cbn.com/universalnav/img/cookie-law-desktop.png';
        markup += '<div class="uninav-cookie-law-container" style="background-image : url(' + uheaderImage + ')">';
    } else {
        markup += '<div class="uninav-cookie-law-container" style="background-image : url()">';
    }

    markup += '<div class="uc-content">';
    markup += '<div class="uc-sub-content">';

    if (title !== 'none') {
        var utitle = uCookie.title ? uCookie.title : 'Welcome, Kapamilya!';
        markup += '<p class="uc-main-title">' + utitle + '</p>';
    }

    markup += '<p class="uc-sub-title">' + contentCopy + '</p>';
    markup += '</div>';
    markup += '<div class="uc-sub-btn">';
    markup += '<a href="http://corporate.abs-cbn.com/privacy" class="uc-tell-me-more" target="_blank">Tell me more!</a>';
    markup += '<button class="uc-btn-agree-cookie">I AGREE!</button>';
    markup += '</div>';
    markup += '</div>';
    markup += '</div>';
    $('body').append(markup);

    setTimeout(function () {
        $('.uninav-cookie-law-container').addClass('uninav-cookie-law-container-ready');
    }, 500);
};

$(document).on('click', '.uc-btn-agree-cookie', function () {
    function done(func) {
        $('.uninav-cookie-law-container').addClass('uninav-cookie-law-container-done');
        setTimeout(function () {
            func();
        }, 5000);
    }
    if (typeof localStorage === 'object') {
        try {
            localStorage.setItem("uninavIsAgreeCookieLaw", "true");
            localStorage.setItem("uninavCookieLawExpiration",new Date(new Date().setFullYear(new Date().getFullYear() + 1)));
            done(function () {
                $('.uninav-cookie-law-container').remove();
            });
            console.log('saved via localstorage');
         
        } catch (e) {
            Storage.prototype._setItem = Storage.prototype.setItem;
            Storage.prototype.setItem = function () { };
            // document.cookie = "uninavIsAgreeCookieLaw=true; expires=Fri, 31 Dec 9999 23:59:59 GMT";
            var cookieLawExp = new Date(new Date().setFullYear(new Date().getFullYear() + 1));
            document.cookie = "uninavIsAgreeCookieLaw=true; expires='"+cookieLawExp+"'";
            done(function () {
                $('.uninav-cookie-law-container').remove();
            });
            console.log('saved via cookie');
        }
    }
});
